Lubo
Support Agent
Lubo 15:49
Hi there, thanks for contacting us. I'll be happy to help.
Siva Shankar 15:49
Hi
Lubo 15:52
Hi Siva!
How are you today?
Siva Shankar 15:50
Fine
Lubo 15:53
Great, how can I help you?
Siva Shankar 15:50
I have a doubt in charge..
Lubo 15:53
What kind of doubt?
Siva Shankar 15:51
Is Stripe user account handles multiple currency?
Like USD as well as GBP
Lubo 15:54
Ok
What is the question?
Siva Shankar 15:58
If I charge 100 GBP through card then it will go to Stripe Account with commission 96 GBP. Hence Stripe account hold GBP currency. Right?
Lubo 16:02
If you have a bank accounts set for that currency than we will hold it for you in GBP yes.
It all depends which currency you have set as default and if you have any other currencies supported on your Stripe account.
Siva Shankar 16:02
I don't have added bank account. just using Test data. But the requirement is to set GBP as default..
The stripe will have only GBP currency
Lubo 16:06
If your Stripe account is created in UK than your default currency will be GBP yes
Siva Shankar 16:03
Yes
Lubo 16:07
Ok
Siva Shankar 16:05
But I will charge using different currency LIkE Charge by USD
Lubo 16:08
In that case it depends if you would have a US based bank account where you would eb able to get payouts in USD
If not we will convert USD into GBP for you.
Siva Shankar 16:08
"If not we will convert USD into GBP for you." This is my doubt. Where the conversion doing and what are the charges for conversion? How the Stripe user withdraw the amount in GBP?
Lubo 16:12
That's what we have a documentation about here: https://stripe.com/docs/currencies/conversions
Siva Shankar 16:13
That's not clearing my doubt.
Please tell me how this working "Stripe accumulates separate balances for each currency"
Lubo 16:17
One second please.
What exactly do you have doubt about here?
Siva Shankar 16:15
You said "If not we will convert USD into GBP for you." in above comment..
Lubo 16:19
Yes
As the only currency you would have would be GBP
Lubo 16:19
So if you get paid in USD we will have to convert it into GBP for you
Siva Shankar 16:18
Where you do this conversion?
Lubo 16:21
Do you mean the rates?
The conversion will be done when you receive the payment
Siva Shankar 16:22
Does this conversion have charges?
Lubo 16:26
Let me check
There is a small fee that I'm not sure how much it is but if you are interested I can escalate this to our specialist team for you and they will be able to help you understand it better.
Siva Shankar 16:26
Okay
Shall I wait?
Read
Lubo 16:30
They will contact you by email sivashankar@aryvart.com
Would that be ok with you?
Siva Shankar 16:27
Okay
Siva Shankar 16:27
Thanks