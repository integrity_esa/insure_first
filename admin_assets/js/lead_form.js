
$(function () { 
	$('[data-toggle="tooltip"]').tooltip()
});
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 45 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

/**image priview**/
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#user_image')
                .attr('src', e.target.result)
                .width(150)
                .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
/** DATE AND TIME FILTER**/

$('#anniversary').datetimepicker({
	format: 'DD-MM-YYYY',
    locale: 'de',
    viewMode: 'days',
    useStrict: true,
    focusOnShow: true,
    showClose: true,
  //  minDate : '2019-01-01'
});
$('#renewal').datetimepicker({
	format: 'DD-MM-YYYY',
    locale: 'de',
    viewMode: 'days',
   // useStrict: true,
    focusOnShow: true,
    showClose: true,
   // maxDate: moment()   
});
$('#appointment').datetimepicker({
  format: 'DD-MM-YYYY',
    locale: 'de',
    viewMode: 'days',
   // useStrict: true,
    focusOnShow: true,
    showClose: true,
  //  maxDate: moment()   
});
$('#dob').datetimepicker({
    format: 'DD-MM-YYYY',
    locale: 'de',
    viewMode: 'days',
    //useStrict: true,
    focusOnShow: true,
    showClose: true,
   maxDate: moment()   
});


/**get state value in dropdown**/
$(document).ready(function() {
    getState(); 
    
});
$(document).on("change",'#country', function () {
    getState();
});
/**get city value in dropdown**/
$(document).on("change",'#state', function () {
   getCity();
});

function getState(){
    optVal = $('#country').val(); 
    console.log(optVal); 
    var selectedState=$('#state').attr('data-selected');
        $.ajax({
            url: siteUrl+"/getAllState",
            type: 'post',
            dataType: 'json',
            data: {'country_id' : optVal },
            success: function(res) {
                $('#state').html(res.stateHtml);
                $('#state').val(selectedState);
                getCity();
            },
            error: function(optVal) {
            }
        });
        return false;
}
function getCity(){
    cityId = $('#state').val();
    console.log(cityId); 
    var selectedCity=$('#city').attr('data-selected');
        $.ajax({
            url: siteUrl+"/getAllCity",
            type: 'post',
            dataType: 'json',
            data: {'state_id' : cityId },
            success: function(res) {
                $('#city').html(res.cityHtml);
                $('#city').val(selectedCity);
            },
            error: function(city) {
            }
        });
        return false;
}

    /**disable single or marital Spouse Name**/
$(document).ready(function() { 
    $('#m_status').change(function() {
        //alert('bow');
    if( $(this).val() == 2) {
            $('#spousename').prop( "disabled", false );
    } else {       
      $('#spousename').prop( "disabled", true );
    }
  });
});

/**validation form**/
 $(document).ready(function() {
   //validation from
        $("#adminAddUser").validate({
            rules: {
                    title: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },fname: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },lname: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },aadharnum: {
                        minlength:14,
                        maxlength:16,
                        number: true, 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },address1: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },
                    country: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },state: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },city: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },
                    pincode: { 
                        required: true,
                        minlength:6,
                        maxlength:8,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },phone: { 
                        required: true,
                        minlength:10,
                        maxlength:12,
                        number: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },email: { 
                        required: true,
                        email:true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },gender: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },dob: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },m_status: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },anniversary: {
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },policytype: {
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },leadtype: {
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },leadcategory: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },appointment: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    },renewal: { 
                        required: true,
                        normalizer: function(value) {
                        return $.trim(value);
                        }
                    }
                },
                messages :{
                    "fname" : {
                    required : 'Enter First Name'
                    },"lname": { 
                        required: 'Enter Last Name'
                    },"aadharnum": {
                        required: 'Enter Aadhar Number'
                    },"address1": { 
                        required: 'Enter your Address'
                    },
                    "country": { 
                        required: 'Select country'
                    },"state": { 
                        required: 'Select state'
                    },"city": { 
                        required: 'Select city'
                    },
                    "pincode": { 
                        required: 'Enter Pincode'
                    },"phone": { 
                        required: 'Enter your Phone Number'
                    },"email": { 
                        required: 'Enter your Email'
                    },"gender": { 
                        required: 'Select Gender'
                    },"dob": { 
                        required: 'Select Date of birth',
                    },"m_status": { 
                        required: 'Select Marital Status',
                    },"anniversary": {
                        required:  'Select Anniversary',
                    },"policytype": {
                        required:  'Select Policy Type',
                    },"leadtype": {
                        required: 'Select Lead Type',
                    },"leadcategory": { 
                        required: 'Select Lead Category'
                    },"appointment": { 
                        required: 'Select  Appointment Date & Time'
                    },"renewal": { 
                        required: 'Select Renewal Date'
                    }
                },
            submitHandler: function (form) {
                var formss = $('form#adminAddUser');
                var formData = new FormData(formss[0]);
                $.ajax({
                    url: siteUrl+"/adminAddNew",
                    type: 'post',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(res) {
                        if (res.status=="success") {
                            alertify.alert('Success', 'User information update successfully.', function(){
                                // location.reload();
                                window.location.href=siteUrl+"/load_lead";
                            });
                        } else {
                            alertify.alert('Error', 'Failed to add user information.');
                        }
                    },
                    error: function(html) {
                        JSON.stringify(html);
                        console.log("error" + html);
                    }
                });
            }
        });

});