$( document ).ready(function() {

	// to check all 
$('.checkall').click(function() {
	var check = '.'+$(this).attr('name');
    var checked = $(this).prop('checked');

    $(check).each(function(){
    	$(this).prop('checked', checked);
    	    	//alert(checked);

    });
  });

    $("#role_table").dataTable();

$(".role_submit").click(function(){

		var role_name =  $("#role_name").val();
		var r_permission = [];
		var u_permission = [];
		var l_permission = [];
		var b_permission = [];
		var p_permission = [];
		var settings_permission = [];

		$('.r_permissions').each(function(){
			if($(this).is(":checked")){
				        r_permission.push($(this).val());
			}
		});
		$('.u_permissions').each(function(){
			if($(this).is(":checked")){
				        u_permission.push($(this).val());
			}
		});
		$('.b_permissions').each(function(){
			if($(this).is(":checked")){
				        b_permission.push($(this).val());
			}
		});
		$('.p_permissions').each(function(){
			if($(this).is(":checked")){
				        p_permission.push($(this).val());
			}
		});

		$('.l_permissions').each(function(){
					if($(this).is(":checked")){
						        l_permission.push($(this).val());
					}
				});
		$('.settings_permissions').each(function(){
					if($(this).is(":checked")){
						settings_permission.push($(this).val());
					}
				});

	var permissions = { "user": u_permission, "role": r_permission,"lead":l_permission, "branch":b_permission ,"policy": p_permission ,"settings": settings_permission};
	
 	var url = $(this).data('url')+'admin/add_role';
 	////
		$("#new_role").validate({
        rules: {
        		role_name: {
            	required: true,
        		}
	    },
	     messages: {
	        role_name: {
	            required: "Enter the role name"
	        }
		},
		
		
		  submitHandler: function(form) {
		  	// var role_name = $("#role_name").val();
		  	// alert(role_name); return false;
			if(role_name !=''){
				
				$.ajax({
					
						type: 'POST',
						url: url,			
						data: {role_name:role_name,permissions:permissions},
						
						success:function(response){
							 if (response) {
								alertify.alert('Success', 'Role added successfully.', function(){
									location.reload();
								});
							} else {
								alertify.alert('Error', 'Failed to add role.');
							}
							
						},
						error:function(response){
							console.log("error is there"+response);
						}
					});
			}
		  },
	        error: function (response) {
	            console.log("error is there" + response);
	        }
		});

		

		

	});

	// To delete role
	$('.confirm_box').show();
	// $('.delete_link').click(function(){
	// 	$('.confirm_box').show();
		
	// });

	$('.delete_link').click(function(){
		var id = $(this).data('id');
		alertify.confirm('Confirm', 'Are you want to delete the role?', 
		function(){ 
			//alertify.success('Ok')
		$.ajax({
			url: siteUrl+"/role_delete",
			type: 'post',
			data: {id:id},
			dataType: 'json',
			success: function(res) {
				if (res.status=="success") {
					alertify.alert('Success', 'Role Deleted.', function(){
						location.reload();
					});
				} else {
					alertify.alert('Error', 'Failed to delete the role.');
				}
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});


	 	},function(){ 
	 		//alertify.error('Cancel')
	 	});



});

$('.confirm').click(function(){
		//window.location.href = "https://www.tutorialrepublic.com/";
		$('.confirm_box').hide();
		
	});
$('.cancel').click(function(){
		$('.confirm_box').hide();
		
	});




////////////Script for Add user
/**image priview**/
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#user_image')
                .attr('src', e.target.result)
                .width(150)
                .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$('.add_user_link').click(function(){
window.location.href = $(this).data('url');
});

// $('.add_user').click(function(){
// 	//console.log($('#title').val());
// });
$("#adduser").validate({
                rules: {
                    title: { 
                        required: true
                    },fname: { 
                        required: true,
                    },lname: { 
                        required: true
                    },phone: { 
                        required: true,
                        minlength:10,
						maxlength:10,
						number: true
                    },email: { 
                        required: true,
                        email:true
                    },role:{
                    	required: true,
                    },password : {
                    	required: true,
                   		 minlength : 3
	                },
	                password_confirm : {
	                	required: true,
	                    minlength : 3,
	                    equalTo : "#password"
	                },report_to:{
                    	required: true,
                    },
                    location:{
                    	required: true,
                    	 minlength:6,
						maxlength:6,
                    },
                },
                messages :{
			        "fname" : {
			        required : 'Enter First Name'
			        },"lname": { 
                        required: 'Enter Last Name'
                    },"phone": { 
                        required: 'Enter the Phone Number'
                    },"email": { 
                        required: 'Enter the Email'
                    },"role": { 
                        required: 'Select the Role'
                    },"location": { 
                        required: 'Enter the Location'
                    },"password": { 
                        required: 'Enter the Password'
                    },"password_confirm": { 
                        required: 'Re-enter the Password Correctly'
                    },"report_to": { 
                        required: 'Select the Reporting user '
                    }
			    }
            });

$(document).on('submit','form#adduser',function(){
		var form = $('form#adduser');
        var formData = new FormData(form[0]);
		alert(formData);return false;

		$.ajax({
			url: siteUrl+'/add_user_insert',
			type: 'post',
			dataType: 'json',
			processData: false,
			contentType: false,
			data: formData,
			success: function(res) {
				if (res.status=="1") {
					alertify.alert('Success', 'User information updated successfully.', function(){
								window.location.href=siteUrl+'/users';
					});
				} else {
					alertify.alert('Error', 'Failed to update user information.');
				}
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});
		return false;
	});


$('.user_status').click(function(){
	//var checked = $(this).is(':checked');
	
	var id = $(this).data('id');
	var status = '';
	if($(this).is(':checked') == true){
		status = 1;
	}else{
		status=0;
	}
	// alert(id);
	// alert(status);
	//return false;
	// alert(id);
	// alert(status);
	// return false;
	alertify.confirm('Confirm', 'Are you want to change the status?', 
		function(){ 
			//alertify.success('Ok')
		$.ajax({
			url: siteUrl+"/update_user_status",
			type: 'post',
			data: {id:id,status:status},
			dataType: 'json',
			success: function(res) {
				console.log(res.status);
				if (res.status=="success") {
					alertify.alert('Success', 'User status changned successfully.', function(){
						location.reload();
					});
				} else {
					alertify.alert('Error', 'Failed to change the status.');
				}
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});


	 	},function(){ 
	 		alertify.error('Cancel')
	 		location.reload();
	 	});

});

/// user delete
$('.user_delete').click(function(){
		var id = $(this).data('id');

		alertify.confirm('Confirm', 'Are you want to delete the user?', 
		function(){ 
			//alertify.success('Ok')
		$.ajax({
			url: siteUrl+"/delete_user",
			type: 'post',
			data: {id:id},
			dataType: 'json',
			success: function(res) {
				console.log(res);
				console.log(res.status);
				if (res.status=="success") {
					alertify.alert('Success', 'User Deleted.', function(){
						location.reload();
					});
				} else {
					alertify.alert('Error', 'Failed to delete the user.');
				}
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});


	 	},function(){ 
	 		alertify.error('Cancel')
	 	});



});

$('.user_edit').click(function(){
	var id = $(this).data('id');
	var url = siteUrl+'/edit_user?user_id='+id;
	window.location.href = url;
	return false;
});

$("#edituser").validate({
                rules: {
                    title: { 
                        required: true
                    },fname: { 
                        required: true,
                    },lname: { 
                        required: true
                    },phone: { 
                        required: true,
                        minlength:10,
						maxlength:10,
						number: true
                    },email: { 
                        required: true,
                        email:true
                    },role:{
                    	required: true,
                    },password : {
                    	required: true,
                   		 minlength : 3
	                },
	                password_confirm : {
	                	required: true,
	                    minlength : 3,
	                    equalTo : "#password"
	                },report_to:{
                    	required: true,
                    },
                    location:{
                    	required: true,
                    	 minlength:6,
						maxlength:6,
                    },
                },
                messages :{
			        "fname" : {
			        required : 'Enter First Name'
			        },"lname": { 
                        required: 'Enter Last Name'
                    },"phone": { 
                        required: 'Enter the Phone Number'
                    },"email": { 
                        required: 'Enter the Email'
                    },"role": { 
                        required: 'Select the Role'
                    },"location": { 
                        required: 'Enter the Location'
                    },"password": { 
                        required: 'Enter the Password'
                    },"password_confirm": { 
                        required: 'Re-enter the Password Correctly'
                    },"report_to": { 
                        required: 'Select the Reporting user '
                    }
			    }
            });

$(document).on('submit','form#edituser',function(){
/*
	alert(siteUrl+'/update_user');
		var form = $('form#edituser');		
		alert(formData);
		$.ajax({
			url: siteUrl+'/update_user',
			type: 'post',
			dataType: 'json',
			processData: false,
			contentType: false,
			data: formData,
			success: function(res) {
				if (res.status=="1") {
					alertify.alert('Success', 'User information updated successfully.', function(){
						//location.reload();
					});
				} else {
					alertify.alert('Error', 'Failed to update user information.');
				}
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});
		return false;*/
	});


////////////////
$("#new_branch").validate({
                rules: {
                    branch_name: { 
                        required: true,
                    },
                    location:{
                    	required: true,
                    },
                },
                messages :{
			        "branch_name" : {
			        required : 'Enter the Branch Name'
			        },"location": { 
                        required: 'Enter the Location'
                    }
			    }
            });

$(document).on('submit','form#new_branch',function(){
		var form = $('form#new_branch');		

		var siteUrl = form.data('url');
		$.ajax({
			url: siteUrl,
			type: 'post',
			dataType: 'json',
			processData: false,
			contentType: false,
			data: formData,
			success: function(res) {
				if (res.status=="success") {
					alertify.alert('Success', 'User information updated successfully.', function(){
						location.reload();
					});
				} else {
					alertify.alert('Error', 'Failed to update user information.');
				}
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});
		return false;
	});

	$('.fso').change(function(){
		var user_id = $(this).val();
		var url = $('#select_fso').data('url');
		$.ajax({
			url: url+"admin/lead_manage",
			type: 'post',
			data: {user_id:user_id},
			success: function(res) {
				console.log(res);
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});


	 	
	});

});
//For Add and Edit Settings
$(document).on('click','.add_settings',function(){ 
	
	var formss = $('form#addsettings');
	var formData = new FormData(formss[0]);
	$.ajax({
		url: baseUrl+'settings',
		type: 'post',
		dataType: 'json',
		processData: false,
		contentType: false,
		data: formData,
		success: function(res) {
			if (res.status=="1") {
				alertify.alert('Success', 'Settings information updated successfully.', function(){
					location.reload();
				});
			} else {
				alertify.alert('Error', 'Failed to update settings information.');
			}
		},
		error: function(html) {
			JSON.stringify(html);
			console.log("error" + html);
		}
	});
	return false;
});

/// for existing email and password check
function checkuserEmail() {
        var email = $("#email").val();
        var id = $(".urldata").data('user');
        var url = $(".urldata").data('url');
        if (email != "") {
            $.ajax({
                url: url+"checkUseremail",
                data: {userEmail:email,id:id },
				type: "post",
				dataType:'json',
                success: function(result ) {
					//alert(result.query); return false;
                    $("#email").parent().next(".validation").remove(); 
                    if (result.query == 1) {
						//alert("sss");
                        $("#email").focus();
                        $("#email").css('border-color', 'red');
                        $("#email").closest('.form-group').find('.mandatory').html('Email address already exists');
                        $("#email").val('');
                       return false;
                    }else if(result.query === 0){
						// $("#email").css('border-color', '');
						// $("#email").closest('.form-group').find('.mandatory').html('');
						var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (reg.test($("#email").val()) == false) {
                            $("#email").focus();
                            $("#email").css('border-color', 'red');
                            $("#email").closest('.form-group').find('.mandatory').html('');
							$("#email").val('');
							return false;
                        } else {
                            $("#email").css('border-color', '');
                            $("#email").closest('.form-group').find('.mandatory').html('');        return false;               
                        }
					}
                }
            });
            return false;

        }
}

 function checkuserPhone() {
        var phone = $("#phone").val();
        var id = $(".urldata").data('user');
        var url = $(".urldata").data('url');
        if (phone != "") {
            $.ajax({
                url: url+"checkUserPhone",
                data: {phone : phone,id:id},
				type: "post",
				dataType:'json',
                success: function (result) {
                    $("#phone").parent().next(".validation").remove(); 
                    if (result.query == 1) {
                        $("#phone").focus();
                        $("#phone").css('border-color', 'red');
                        $("#phone").closest('.form-group').find('.mandatory').html('Phone number already exists');
                        $("#phone").val('');
                        return false;
                    } else if(result.query == 0){
						/* $("#phone").css('border-color', '');
                        $("#phone").closest('.form-group').find('.mandatory').html(''); */
						$("#phone").css('border-color', '');
						$("#phone").closest('.form-group').find('.mandatory').html(''); 
                        return false;   
                    }
                }
            });
            return false;
        }
}