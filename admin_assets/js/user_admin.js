/**check mail already exit(24-12-2018)***/
function checkEmail() {
    var email = $("#email").val();

    if (email != "") {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/checkEmail",
            data: "userEmail=" + $("#email").val(),
            type: "post",
            success: function (result) {
                $("#email").parent().next(".validation").remove(); 
                if (result == 1) {
                    $("#email").focus();
                    $("#email").css('border-color', 'red');
                    $("#email").closest('.form-group').find('.mandatory').html('Email address already exists');
                    $("#email").val('');
                    return false;
                } else {
                    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    if (reg.test($("#email").val()) == false) {
                        $("#email").focus();
                        $("#email").css('border-color', 'red');
                        $("#email").closest('.form-group').find('.mandatory').html('');
                        $("#email").val('');
                    } else {
                        $("#email").cssselect('border-color', '');
                        $("#email").closest('.form-group').find('.mandatory').html('');                            
                    }
                }
            }
        });
    }
}