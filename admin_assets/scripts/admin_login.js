$(document).ready(function(){ 

 $(".forgot_pass").click(function(){
    var email =  $(".email").val();  
    var url = $(this).data('url') ;
                $.ajax({
                type: "POST",
                url: url + "admin/admin_forgot_password/",
                dataType: "json",
                data: {email:email},
                success: function (json) {
                    console.log(json);

                        if (json.status == 0) {
                            $('.alert-success').hide();
                            $('.alert-danger').show();
                            $('.alert-danger').html("Invalid Email");

                        } else if (json.status == 1) {
                            $('.alert-danger').hide();
                            $('.alert-success').show();
                            $('.alert-success').show();
                            $('#alert_succ_message').html("Mail send successfully");                
                            //window.location = url+'admin/forgot_password';
                            return false;
                        }
                    }
                });
 });


// changne password
$("#admin_change").validate({
                rules: {
                    new_password : {
                        required: true,
                         minlength : 3
                    },
                    confirm_password : {
                        required: true,
                        minlength : 3,
                        equalTo : "#new_password"
                    },
                },
                messages :{
                   
                    "new_password": { 
                        required: 'Enter the Password'
                    },"confirm_password": { 
                        required: 'Re-enter the Password Correctly'
                    }
                }
            });


 $('.change_pass').click(function(){
    var new_pass = $('.new_password').val();
    var con_pass = $('.confirm_password').val();
    var email = $(this).data('email');
    var url = $(this).data('url');
   $.ajax({
                type: "POST",
                url: url + "admin/change_new_password",
                dataType: "json",
                data: {password:new_pass,email:email},
                success: function (json) {
                    console.log(json);

                        if (json.status == 0) {
                            $('.alert-success').hide();
                            $('.alert-danger').show();
                            $('.alert-danger').html("Failed to change password.Please try again.");

                        } else if (json.status == 1) {
                            $('.alert-danger').hide();
                            $('.alert-success').show();
                            $('.alert-success').show();
                            $('.fields').hide();
                            $('.login_link').removeClass('hidden');
                            $('#alert_succ_message').html("Password changed successfully");                
                            //window.location = url+'admin/forgot_password';
                            return false;
                        }
                    }
                });
   return false;

 });
                

                
                
});

   