// DATATABLE SCRIPT CODE BEGIN 
$(document).ready(function () {
  generateOrderTable();
$(document).on('change', '#leadStatus', function () {
                generateOrderTable();
            });
$(document).on('change', '#leadCategory', function () {
                generateOrderTable();
            }); 
$(document).on('change','#branch_manager',function(){
window.user_id = $(this).val();
generateOrderTable();
// alert(user_id);
});
$(document).on('change','#fso_filter',function(){
 // alert();
window.fso_user_id = $(this).val();
// alert(window.fso_user_id);
generateOrderTable();
 // alert(lead_id);
});
  function generateOrderTable() {
                 var element = {
                    leadStatus: $('#leadStatus').val(),
                    leadCategory: $('#leadCategory').val(),
                    user_id:window.user_id,
                    fso_user_id:window.fso_user_id
                }; 
        //alert(JSON.stringify(element));return false;
                $.ajax({
          url: base_url+"lead/filterleads",
          data:{
            lead_status :element.leadStatus,
            lead_category :element.leadCategory,
            user_id :element.user_id,
            fso_user_id :element.fso_user_id
          },
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function () {
                        $('#render-list-of-order').html('<div class="text-center mrgA padA"><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i></div>');
                    },
                    success: function (html) {
          //  console.log(html); return false;
                        var dataTable = '<table id="order-datatable" class="table table-striped" cellspacing="0" width="100%"></table>';
                        $('#render-list-of-order').html(dataTable);
                        var table = $('#order-datatable').DataTable({
                            aoColumnDefs: [
                                {
                                    'bSortable': false,
                                    'aTargets': [-1]
                                }
                            ],
                            bJQueryUI : true,
                            bDestroy : true,
                            aaSorting : [[0, 'asc']],

                            data: html.data,
                            "bPaginate": true,
                            // "pageLength": 3,
                            "bLengthChange": true,
                            "bFilter": true,
                            "bInfo": true,
                            "bAutoWidth": true,
                            columns: [
                                {title: "S.NO"},
                                {title: "FIRST NAME"},
                                {title: "STATUS NAME"},
                                {title: "CATEGORY NAME"},
                                {title: "LEAD TITLE"},
                                {title: "APPOINTMENT DATE"},
                                {title: "RENEWAL DATE"},
                                {title: "ACTION"},
                            ],
                        });
                    },
                    error: function (html) {
                        JSON.stringify(html);
                        console.log("error" + html);
                    }
                });
            }
    });

 // Assign FSO LEADS related script code 
$('body').on('click', '#submit_form_data', function(){
      var form = $('form#lead_data');
      var formData = new FormData(form[0]);
    // alert(JSON.stringify(formData));return false;
      $.ajax({
         url: base_url+"UserAssign_fsolead/check_fso_lead_id_update",
         type: 'post',
         dataType: 'json',
         processData: false,
         contentType: false,
         data: formData,
         success: function(res) {
            if (res == 1) {
               alertify.alert('Success', 'Lead assigned successfully.', function(){
                        window.location.href = base_url+'UserAssign_fsolead/assignFsoLead';
               });
      } else if(res==2) {
               alertify.alert('Error', 'Please select atleast one lead to submit');
            }else {
               alertify.alert('Error', 'Failed to assign lead.');
            }
         },
         error: function(html) {
            JSON.stringify(html);
            console.log("error" + html);
         }
      });
      return false;

   });
$('#select_fso').change(function(){
    var user_id = $(this).val();
    /* console.log(user_id); 
    return false; */
    var kms = $("#kilometer").val();
    var limit = $("#limit").val();
      if (user_id != "" && user_id !=0) {
              $.ajax({
                  url: base_url+"UserAssign_fsolead/assignFsoLead",
                  data: {user_id:user_id,limit:limit,kms:kms},
                  type: "post",
                  dataType:"JSON",
                  success: function (result) {
                      //console.log(result);return false;
                     if(result.leads.length>0){
                        var lead_id="";
                        var check_bx_fso_lead ="";
                        var leadHtml='<ol class="list-unstyled">';
                        $.each(result.leads, function(index, element) {
                            leadHtml+='<li><label class="fso-check"><input type="checkbox"  name="check_fso_lead[]" value="'+element.lead_id+'"/><span class="checkmark"></span> '+element.lead_title.toUpperCase()+' '+element.first_name.toUpperCase()+' '+element.last_name.toUpperCase()+' <br/>'+element.address_line1.toUpperCase()+'<br/> '+element.address_line2.toUpperCase()+' <br/> '+element.email+' <br/> '+element.pincode+'<br/> '+element.phone+'</label></li>';

                           lead_id +='<input type="hidden" name="lead_id[]" value="'+element.lead_id+'"/>';                          
                         });
                         //user id getting
                         // console.log(result.user_id);
                         leadHtml+='</ol>';
                        var hidden_user_id ='<input type="hidden" name="user_id" value="'+result.user_id+'"/>';
                        var submit_form ='<button type="button" name="submit" id="submit_form_data" class="btn btn-primary">submit</button>';
                         //console.log(leadHtml);
                        $('.lead_list_view').html(leadHtml);
                        $('.check_box_fso_lead').html(check_bx_fso_lead);
                        $('.lead_id').html(lead_id);

                        $('#hidden_user_id').html(hidden_user_id);
                        $('#submit_data').html(submit_form);
                     } else{
                        alertify.alert('Message', 'No Leads Available.');
                        $('.lead_list_view').html('');
                        $('.lead_id').html('');
                        $('.check_box_fso_lead').html('');
                        $('#hidden_user_id').html('');
                        $('#submit_data').html('');
                     }
                  }
              });
             return false;
          }
          else{
            $('.lead_list_view').html('');
                        $('.lead_id').html('');
                        $('.check_box_fso_lead').html('');
                        $('#hidden_user_id').html('');
                        $('#submit_data').html('');
          }
   });


//import excel file
$('#importform_data').on('submit', function(event){
  //var url = base_url+"UserAssign_lead/import";
  var branch_manager_user_id = $('#select_branchManager').val();
  //alert(branch_manager_user_id);return false;
  //alert(url); return false;
  var form_data = new FormData(this);
  form_data.append('branch_manager_user_id',branch_manager_user_id);
    event.preventDefault();
    $.ajax({
        url:base_url+"UserAssign_lead/import",
        method:"POST",
        data:form_data,
        contentType:false,
        cache:false,
        processData:false,
        dataType:'JSON',
        success:function(data){
            // alert(data.existMails);
            if (data.existMails!="") {
                alertify.alert('Error', data.existMails+' Emails already exists.', function(){
                    location.reload();
                });
            } else {
                // alertify.alert('Error', 'Failed to upload.');
                alertify.alert('success', 'successfully upload.', 
                    function(){
                    location.reload();
                });
             }
        }
    })
});

