// ajax login in users
$('#submit_data').on('click', function (e) {
    e.preventDefault();
    $('#spnError').html('Checking...');
    var url = base_url + 'login/login_user';
    var user = $('#logForm').serialize();
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: user,
        success: function (response) {
            if (response === '0') {
                $('#spnError').removeClass('alert alert-success');
                $('#spnError').addClass('alert alert-danger');
                $("#spnError").html("Check your email and password!!!");
            } else {
                $('#spnError').removeClass('alert alert-danger');
                $('#spnError').addClass('alert alert-success');
                $("#spnError").html("You've been successfully logged in!");
                setTimeout(function () {
                    window.location.href = base_url + 'dashboard';
                }, 1000);
            }
        },
        error: function (response) {
            console.log("error is there" + response);
        }
    });
});
// forgot password

//validation from
$("#validate_form").validate({
    rules: {
        mobile_number: {
            required: true,
        }
    },
    messages: {
        mobile_number: {
            required: "Enter the mobile number"
        }
    },
    submitHandler: function (form) {
         var mobile_number = $('#mobile_number').val();
	    $('#show_text').html('Please wait...');
	    $.ajax({
	        type: 'POST',
	        url: base_url + 'login/forgot_password_check',
	        dataType: 'json',
	        data: {mobile_number: mobile_number},
	        success: function (response) {
	            if (response.message == "OTP_mail_send_successful") {
	                $('#show_text').removeClass('alert alert-danger');
	                $('#show_text').addClass('alert alert-success');
	                $("#show_text").html("You've been successfully send mail!");
	                setTimeout(function () {
	                    window.location.href = base_url + 'login/otp_verification/' + response.mobile;
	                }, 1500);
	            } else if (response.message == "Unregistered_mobile_number") {
	                $('#show_text').removeClass('alert alert-success');
	                $('#show_text').addClass('alert alert-danger');
	                $("#show_text").html("Unregistered mobile number!!!");
	            } else if (response.message == "Failed_to_send_mail") {
	                $('#show_text').removeClass('alert alert-success');
	                $('#show_text').addClass('alert alert-danger');
	                $("#show_text").html("Failed to send mail !!!");
	            } else if (response.message == "Failed_to_submit") {
	                $('#show_text').removeClass('alert alert-success');
	                $('#show_text').addClass('alert alert-danger');
	                $("#show_text").html("Failed to submit !!!");
	            }
	        },
	        error: function (response) {
	            console.log("error is there" + response);
	        }
	    })
    }
});
//verification four digit otp
//validation form
$("#validate_form_otp1").validate({
    rules: {
        first_digit: {
            required: true,
        },
        second_digit: {
            required: true,
        },
        third_digit: {
            required: true,
        },
        forth_digit: {
            required: true,
        }
    },
    messages: {
        first_digit: {
            required: "Enter otp"
        },
        second_digit: {
            required: "Enter otp"
        },
        third_digit: {
            required: "Enter otp"
        },
        forth_digit: {
            required: "Enter otp"
        }
    },
    submitHandler: function (form) {
    var first_digit = $('#first_digit').val();
    var second_digit = $('#second_digit').val();
    var third_digit = $('#third_digit').val();
    var forth_digit = $('#forth_digit').val();
    //$('#show_text').html('Please wait...');
    $.ajax({
        type: 'POST',
        url: base_url + 'login/check_email_otp_verification',
        dataType: 'json',
        data: {first_digit: first_digit, second_digit: second_digit, third_digit: third_digit, forth_digit: forth_digit},
        success: function (response) {
            if (response.message == "valid_otp") {
                $('#show_text_verify').removeClass('alert alert-danger');
                $('#show_text_verify').addClass('alert alert-success');
                $("#show_text_verify").html("You've been successfully varify otp!");
                setTimeout(function () {
                    window.location.href = base_url + 'login/change_password/' + response.phone;
                }, 1500);
            } else if (response.message == "invalid_otp") {
                $('#show_text_verify').removeClass('alert alert-success');
                $('#show_text_verify').addClass('alert alert-danger');
                $("#show_text_verify").html("Invalid otp!");
            }
        },
        error: function (response) {
            console.log("error is there" + response);
        }
    })	
    }
});
//update forget password
$("#update_validate_form").validate({
    rules: {
        password_1: {
            required: true,
        },
        password_2: {
            required: true,
            equalTo: "#password_1"
        }
    },
    messages: {
        password_1: {
            required: "Enter your password"
        },
        password_2: {
            required: "Enter your password"
        }
    },
    submitHandler: function (form) {
    var password_1 = $('#password_1').val();
    var password_2 = $('#password_2').val();
    var mobile_number = $('#mobile_number').val();
    $('#update_text').html('Please wait...');
    $.ajax({
        type: 'POST',
        url: base_url + 'login/change_password_update',
        dataType: 'json',
        data: {password_1:password_1,password_2:password_2,mobile_number: mobile_number},
        success: function (response) {
            if (response.message == "updated_success") {
                $('#update_text').removeClass('alert alert-danger');
                $('#update_text').addClass('alert alert-success');
                $("#update_text").html("Password updated successfully!");
                setTimeout(function () {
                    window.location.href = base_url + 'login';
                }, 1500);
            }
        },
        error: function (response) {
            console.log("error is there" + response);
        }
    })   
    }
});
