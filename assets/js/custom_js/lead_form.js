
$(function () { 
	$('[data-toggle="tooltip"]').tooltip()
});
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 45 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

/**image priview**/
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#user_image')
                .attr('src', e.target.result)
                .width(150)
                .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
/** DATE AND TIME FILTER**/

$('#anniversary').datetimepicker({
	format: 'DD-MM-YYYY',
    locale: 'de',
    viewMode: 'days',
    useStrict: true,
    focusOnShow: true,
    showClose: true,
    //minDate: moment()	
});
$('#renewal').datetimepicker({
	format: 'DD-MM-YYYY',
    locale: 'de',
    viewMode: 'days',
    useStrict: true,
    focusOnShow: true,
    showClose: true,
    minDate: moment()
});
$('#appointment').datetimepicker({
   format:'DD-MM-YYYY HH:mm',
   locale: 'de',
    viewMode: 'days',
    useStrict: true,
    focusOnShow: true,
    showClose: true,
    minDate: moment()
});
$('#dob').datetimepicker({
    format: 'DD-MM-YYYY',
    locale: 'de',
    viewMode: 'days',
    useStrict: true,
    focusOnShow: true,
    showClose: true,
    maxDate: moment()   
});
    
/**disable single or marital Spouse Name**/
/*$(document).ready(function() { 
    $('#m_status').change(function() {
    if( $(this).val() == 2) {
            $('#spousename').prop( "disabled", false );
    } else {       
      $('#spousename').prop( "disabled", true );
    }
  });
});*/

// to upload campaign lead management start
// $('#import_form').on('submit', function(event){
//    // event.preventDefault();
//    var url = "<?php echo base_url(); ?>Lead_list/import";
//    alert(url);return false;
//    var formss = $('form#import_form');
//    var formData = new FormData(formss[0]);
//     $.ajax({
//         url:"<?php echo base_url(); ?>Lead_list/import",
//         method:"POST",
//         data:formData,
//         contentType:false,
//         cache:false,
//         processData:false,
//         dataType:'JSON',
//         success:function(data){
//             // alert(data.existMails);
//             if (data.existMails!="") {
//                 alertify.alert('Error', data.existMails+' Emails already exists.', function(){
//                     location.reload();
//                 });
//             } else {
//                 // alertify.alert('Error', 'Failed to upload.');
//                 alertify.alert('success', 'successfully upload.', 
//                     function(){
//                     location.reload();
//                 });
//              }
//         }
//     })
// });
// to upload campaign lead management start

/**get state value in dropdown**/
$(document).on("change",'#country', function () {

	optVal = $(this).val(); 
	$.ajax({
		url: "lead/getAllState",
		type: 'post',
		dataType: 'json',
		data: {'country_id' : optVal },
		success: function(res) {
			$('#state').html(res.stateHtml)
		},
		error: function(optVal) {
		}
		});
	return false;
});

/**get city value in dropdown**/
$(document).on("change",'#state', function () {

	city = $(this).val(); 
	$.ajax({
		url: "lead/getAllCity",
		type: 'post',
		dataType: 'json',
		data: {'state_id' : city },
		success: function(res) {
			$('#city').html(res.cityHtml)
		},
		error: function(city) {
		}
		});
	return false;
});
/**validation form**/
 $(document).ready(function() {

            $("#edituser").validate({
                rules: {
                    title: { 
                        required: true
                    },fname: { 
                        required: true,
                    },lname: { 
                        required: true
                    },aadharnum: {
                    	minlength:14,
						maxlength:16,
						number: true, 
                        required: true
                    },address1: { 
                        required: true
                    },country: { 
                        required: true
                    },state: { 
                        required: true
                    },city: { 
                        required: true
                    },pincode: { 
                        required: true,
                         minlength:06,
						maxlength:06,
                    },phone: { 
                        required: true,
                        minlength:10,
						maxlength:10,
						number: true
                    },email: { 
                        required: true,
                        email:true
                    },gender: { 
                        required: true
                    },dob: { 
                        required: true,
                    },m_status: { 
                        required: true,
                    },policytype: {
                        required: true
                    },leadtype: {
                        required: true
                    },leadcategory: { 
                        required: true
                    },appointment: { 
                        required: true
                    },renewal: { 
                        required: true
                    }
                },
                messages :{
			        "fname" : {
			        required : 'Enter First Name'
			        },"lname": { 
                        required: 'Enter Last Name'
                    },"aadharnum": {
                        required: 'Enter Aadhar Number'
                    },"address1": { 
                        required: 'Enter your Address'
                    },"country": { 
                        required: 'Select country'
                    },"state": { 
                        required: 'Select state'
                    },"city": { 
                        required: 'Select city'
                    },"pincode": { 
                        required: 'Enter Pincode'
                    },"phone": { 
                        required: 'Enter your Phone Number'
                    },"email": { 
                        required: 'Enter your Email'
                    },"gender": { 
                        required: 'Select Gender'
                    },"dob": { 
                        required: 'Select Date of birth',
                    },"m_status": { 
                        required: 'Select Marital Status',
                    },"policytype": {
                        required:  'Select Policy Type',
                    },"leadtype": {
                        required: 'Select Lead Type',
                    },"leadcategory": { 
                        required: 'Select Lead Category'
                    },"appointment": { 
                        required: 'Select  Appointment Date & Time'
                    },"renewal": { 
                        required: 'Select Renewal Date'
                    }
			    }
            });
    });


/** form submit for new user (18-12-18) **/
	$(document).on('submit','form#edituser',function(){
		var formss = $('form#edituser');
		var formData = new FormData(formss[0]);
		$.ajax({
			url: siteUrl+"lead/addNew",
			type: 'post',
			dataType: 'json',
			processData: false,
			contentType: false,
			data: formData,
			success: function(res) {
				if (res.status=="success") {
					alertify.alert('Success', 'User information updated successfully.', function(){
						location.reload();
					});
				} else {
					alertify.alert('Error', 'Failed to update user information.');
				}
			},
			error: function(html) {
				JSON.stringify(html);
				console.log("error" + html);
			}
		});
		return false;
	});

/** hide and display divbased on married r single**/
    $(document).ready(function() {
        $('#lol').hide();
    $('#m_status').change(function() {
        if($(this).val() == 2)
            $('#lol').show();
        else
            $('#lol').hide();
    });
    });
//to change password
    $(document).ready(function() {

 $("#userpass").validate({
        rules: {
        oldpass :{
            required: true,
        },
        newpass: { 
            required: true, 
            minlength: 3
        }, 
        conpass: { 
            required: true, 
            equalTo: "#newpass", 
            minlength: 3
        }
        },
        messages : {
        "oldpass" :{
            required : 'Enter old password'
            },
        "newpass":{ 
                required: 'Enter new password'
            },
        "conpass":{
                required: 'Enter new password'
            }
        },
        submitHandler: function (form) {
        var formss = $('form#userpass');
        var formData = new FormData(formss[0]);
        var url = siteUrl+"userprofile/userProfileLead";
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: formData,
            success: function(res) {
                console.log(res);
             if (res =="success") {
                    alertify.alert('Success', 'User Password updated successfully.', function(){
                            //location.reload();
                            });
                    } else {
                        alertify.alert('Error', 'Invalid old password.');
                    }
            },
            error: function(html) {
                JSON.stringify(html);
                console.log("error" + html);
            }
        });
        }
        });
 });

// $(document).ready(function() {
//       $("#userpass").validate({
//         rules: {
//             oldpass :{
//                 required: true,
//             },
//           newpass: { 
//                 required: true, 
//                 minlength: 3
//           }, 
//           conpass: { 
//                 required: true, 
//                 equalTo: "#newpass", 
//                 minlength: 3
//           }
//       }
//     });
// });

// /** form submit for user information (02-01-19) **/
//     $(document).on('submit','form#userpass',function(){
//         var formss = $('form#userpass');
//         var formData = new FormData(formss[0]);
//         var url = siteUrl+"userProfile/userProfileLead";
//         $.ajax({
//             url: url,
//             type: 'post',
//             dataType: 'json',
//             processData: false,
//             contentType: false,
//             data: formData,
//             success: function(res) {
//              if (res =="success") {
//                     alertify.alert('Success', 'User Password updated successfully.', function(){
//                             //location.reload();
//                             });
//                     } else {
//                         alertify.alert('Error', 'Invalid old password.');
//                     }
//             },
//             error: function(html) {
//                 JSON.stringify(html);
//                 console.log("error" + html);
//             }
//         });
//         return false;
//     });

// });