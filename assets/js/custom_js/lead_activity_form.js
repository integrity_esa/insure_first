$(document).ready(function(){
	$('#datetimepicker1').datetimepicker({
		format: "DD-MM-YYYY hh:mm A",
		defaultDate: new Date(),
		minDate: new Date(),
	});
	
	$("#lead_status_form").validate({
		rules: {
			lead_status: {
				required: true
			},
			doc: {
				extension: "xls|csv"
			}
		},
		messages: {
			lead_status: {
				required: "Please select the lead status"
			}
		},
		submitHandler: function (form) {
			var form = $("#lead_status_form");
			var formData = new FormData(form[0]);
			$.ajax({
				url: $(form).attr('action'),
				type: 'POST',
				data: formData,
				dataType: 'JSON',
	            processData: false,
	            contentType: false,
				success: function(res) {
					if (res.status) {
						alertify.alert('Success', 'Lead status updated successfully.', function(){
							location.reload();
						});
	                } else {
	                    alertify.alert('Error', 'Lead status failed to update.');
					}
                },
				error: function(x) {
					console.log(x);
				}
			});
		}
	});

	$("#recorder_history_audio").validate({
		ignore: [],
		rules:{
			'audiup[]': {
				required: true,
			}
		},
		messages: {
			'audiup[]': {
				required: "Please select the audio files!"
			}
		},
		errorPlacement: function(error, elem) {
			//return error.insertAfter(elem);
			return false;
		},
		submitHandler: function (form) {
			var formData = new FormData($(form)[0]);
			$.ajax({
				url: $(form).attr('action'),
				type: 'POST',
				data: formData,
				dataType: 'JSON',
	            processData: false,
	            contentType: false,
				success: function(res) {
					if (res.status) {
						alertify.alert('Success', 'Lead recorder history audio added successfully.', function(){
							location.reload();
						});
	                } else {
	                    alertify.alert('Error', 'Lead recorder history audio failed to add.');
					}
                },
				error: function(x) {
					console.log(x);
				}
			});
		}
	});
	$("#recorder_history_vedio").validate({
		ignore: [],
		rules:{
			'vedi[]': {
				required: true,
			}
		},
		messages: {
			'vedi[]': {
				required: "Please select the vedio files!",
			}
		},
		errorPlacement: function(error, elem) {
			//return error.insertAfter(elem);
			return false;
		},
		submitHandler: function (form) {
			var formData = new FormData($(form)[0]);
			$.ajax({
				url: $(form).attr('action'),
				type: 'POST',
				data: formData,
				dataType: 'JSON',
	            processData: false,
	            contentType: false,
				success: function(res) {
					if (res.status) {
						alertify.alert('Success', 'Lead recorder history vedio added successfully.', function(){
							location.reload();
						});
	                } else {
	                    alertify.alert('Error', 'Lead recorder history vedio failed to add.');
					}
                },
				error: function(x) {
					console.log(x);
				}
			});
		}
	});
});

function fileMultiPreview(t, type, tar) {
	//console.log(t);
	//console.log(t.files);

	if (t.files) {
		var filesAmount = t.files.length;
		$(tar).html('');
		for (i = 0; i < filesAmount; i++) {
			var reader  = new FileReader();
			window.auF = t.files[i];
			reader.onload = function(event) {
				var htm = '';
				if (type == 'aud' && tar) {
					htm = $("<div>", { 
						class: "col-md-6"
					}).append($("<audio>", {
						'controls': '',
						'src': event.target.result
					}));
					$(tar).append(htm);
				} else if (type == 'ved') {
					htm = $("<div>", { 
						class: "col-md-6"
					}).append($("<video>", {
						'controls': '',
						'src': event.target.result
					}));
					$(tar).append(htm);
				}
			};
			reader.readAsDataURL(t.files[i]);
		}
	}
}