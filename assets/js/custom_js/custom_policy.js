$(document).ready(function(){
	console.log('cuspolicy');

		$('#purchase-date').datetimepicker({
			format: "DD-MM-YYYY hh:mm A",
			defaultDate: new Date(),
			minDate: new Date(),
		})

		$('#renewal-date').datetimepicker({
			format: "DD-MM-YYYY hh:mm A",
			defaultDate: new Date(),
			minDate: new Date(),
		});

		$('#purchase-date').on('dp.change', function (e) {
			$('#renewal-date').data("DateTimePicker").minDate(e.date);
		});

		$("#customer-policy-add").validate({
		rules: {
			lead_id: {
				required: true
			},
			policy_type_id: {
				required: true
			},
			payment_method: {
				required: true
			},
			contact_no: {
				required: true
			},
			purchase_date: {
				required: true
			},
			renewal_date: {
				required: true
			},
		},
		messages: {
			lead_id: {
				required: "Please select the lead"
			},
			policy_type_id: {
				required: "Please select the policy type"
			},
			payment_method: {
				required: "Please select the payment method"
			},
			contact_no: {
				required: "Please enter the contact number"
			},
			purchase_date: {
				required: "Please select the date of purchase"
			},
			renewal_date: {
				required: "Please select the renewal date"
			},
		},
		submitHandler: function (form) {
			var form = $("#customer-policy-add");
			var formData = new FormData(form[0]);

			$.ajax({
				url: $(form).attr('data-validate'),
				type: 'POST',
				data: {
					lead_id: $('#lead_identity').val(),
					policy_type_id: $('#policy_type_identity').val() 
				},
				dataType: 'JSON',
				success: function($data) {
					if ($data.status) {
						$.ajax({
							url: $(form).attr('data'),
							type: 'POST',
							data: formData,
							dataType: 'JSON',
				            processData: false,
				            contentType: false,
							success: function(res) {
								if (res.status) {
									alertify.alert('Success', 'Customer policy details added successfully.', function(){
										location.reload();
									});
				                } else {
				                    alertify.alert('Error', 'Customer policy details failed to add.');
								}
			                },
							error: function(x) {
								console.log(x);
							}
						});
					} else {
						alertify.alert('Error', 'Lead already has this vehicle type.');
					}
				},
				error: function() {

				}
			});
		}
	});
});