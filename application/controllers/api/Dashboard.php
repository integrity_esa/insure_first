<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Dashboard extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('dashboard_model'));

        $this->load->library('Ajax_pagination');
        $this->perPage = 2;

    }
    public function index_get(){
        $userId = $this->input->get('user_id');
        $data = array();
        $data['get_all_leads'] = $this->dashboard_model->get_all_leads($userId);
        $data['get_today_leads'] = $this->dashboard_model->get_today_leads($userId);
        if ($data) {
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response($data);
        }

    }
    // karthik :  get today leads in Home page
    public function all_assign_lead_post(){
        $userId = $this->input->post('user_id');
        //$data = array();
        $data = $this->dashboard_model->get_today_leads_assign($userId);
        if ($data) {
            $message = ['status' => '1','get_assign_leads'=>$data, 'message' => 'ok'];
           
        } else {
            $message = ['status' => '0','get_assign_leads'=>array(), 'message' => 'No Leads are available'];
        }

         $this->response($message, REST_Controller::HTTP_OK);

    }

     //get the events
    public function get_events_post(){
        $params['user_id'] = $this->input->post("user_id");
        $params['month'] = $this->input->post("month");
        $params['year'] = $this->input->post("year");

        $events = $this->dashboard_model->get_all_day_events_on_month_year_wise($params);

        if (isset($events) && !empty($events)) {
            $message = ['status' => '1', 'events' => $events, 'message' => 'success'];
        } else {
            $message = ['status' => '0', 'events' => array(), 'message' => 'No events are available'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
        die();
    }
    // change password profile
    public function user_profile_post(){
        $userId   =  $this->input->post('user_id');
        $old_pass =  $this->input->post('oldpass');
        $new_pass =  $this->input->post('newpass');
        $data  = $this->dashboard_model->checkPasswordExist($old_pass,$userId);
        // echo "<pre>";print_r($data);exit();
        if (!empty($data)) {
            $saveuser = $this->dashboard_model->profileInfo($new_pass,$userId);
            if ($saveuser) {
            $message = ['status' => '1','message' => 'Password change successfully'];           
             } else {
            $message = ['status' => '0','message' => 'Check ur password'];
            } 
        }else{
            $message = ['status' => '0','message' => 'Old password is not matched'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }
}

?>