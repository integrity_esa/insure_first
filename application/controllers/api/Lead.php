<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Lead extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('lead_model'));
        $this->load->helper(array('integrate'));
        $this->load->library(array('Ajax_pagination', 'File_handling'));
        $this->perPage = 3;
        }

 // public function addNew_post(){
 //        $userId = $this->input->post('userId');
 //        $request = $this->input->post('request');
 //        $files = $this->input->post('files');
 //        //echo"";print_r($request);exit;
 //        $saveuser=$this->lead_model->insert_user($request,$userId,$files);
 //        if ($saveuser) {
 //            $this->response($saveuser, REST_Controller::HTTP_OK);
 //        } else {
 //            $this->response($saveuser);
 //        }

 //    } 
    public function addNew_post(){
        $data  = json_decode($_POST['personal_lead_details'],true);
        //echo "<pre>";print_r($data);exit();
        $user_id = $data['userid'];
        $files = isset($_FILES) ? $_FILES:'';
        $saveuser = $this->lead_model->insert_user($data,$user_id,$files);

       if ($saveuser==true) {
            $message = ['status' => '1','data'=>$saveuser,'message'=>'success'];
        } else {
            $message = ['status' => '0','data'=>false,'message'=>'Error'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }

   // karthik :  get today leads in lead page
    public function day_leads_post(){
        $filter = $this->input->post('filter');
        $userId = $this->input->post('user_id');
        //$data = array();
        $data = $this->lead_model->get_day_leads($userId, $filter);
        $data_all = $this->lead_model->get_all_leads($userId, $filter);
        if ($data || $data_all) {
            $message = ['status' => '1','day_leads'=>$data,'get_all_leads_page'=>$data_all, 'message' => 'ok'];
        } else {
            $message = ['status' => '0','day_leads'=>array(),'get_all_leads_page'=>array(), 'message' => 'No leads are available'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }
    // get the country
    public function country_post(){
        $data_country = $this->lead_model->get_country();
        if ($data_country) {
            $message = ['status' => '1','getCountry'=>$data_country,'message' => 'ok'];
        } else {
            $message = ['status' => '0','getCountry'=>array(),'message' => 'No countries are avialable'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }    
    // get the state
    public function state_post(){
        $country_id = $this->input->post('country_id');
        $data_state = $this->lead_model->get_state($country_id);
        if ($data_state) {
            $message = ['status' => '1','getState'=>$data_state,'message' => 'ok'];
        } else {
            $message = ['status' => '0','getState'=>array(),'message' => 'No states are avialable'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }
    // get the city
    public function city_post(){
        $state_id = $this->input->post('state_id');
        $data_city = $this->lead_model->get_city($state_id);
        if ($data_city) {
            $message = ['status' => '1','getCity'=>$data_city,'message' => 'ok'];
        } else {
            $message = ['status' => '0','getCity'=>array(),'message' => 'No cities are avialable'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }
    // get all lead_type,lead_category,policy_type,lead_status
    public function get_all_lead_list_post(){
        $lead_type      = $this->lead_model->lead_type();
        $lead_category  = $this->lead_model->lead_category();
        $lead_status    = $this->lead_model->lead_status();
        $policy_type    = $this->lead_model->policy_type();
        $campaign    = $this->lead_model->campaign();
        if (!empty($lead_type || $lead_category || $lead_status || $policy_type || $campaign)) {
            $message = ['status' => '1','lead_type'=>$lead_type,'lead_category'=>$lead_category,'lead_status'=>$lead_status,'policy_type'=>$policy_type,'campaign'=>$campaign,'message' => 'ok'];
        } else {
            $message = ['status' => '0','lead_type'=>array(),'lead_category'=>array(),'lead_status'=>array(),'policy_type'=>array(),'campaign'=>array(),'message' => 'No leads are avialable'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }

    /**
     * API lead details
     * Created by siva shankar
     * @param lead ID
     */
    public function details_post() {
        $lead_id    = $this->input->post('id');
        $result     = array();
        $lead_type  = $this->lead_model->lead_type();
        $detailsObj = $this->lead_model->get_lead_display_details($lead_id, array('select' => array(
            'L.lead_id',
            'L.first_name',
            'L.last_name',
            'L.dob',
            'L.phone',
            'L.lead_img',
            'AL.lead_status AS current_status',
            'LT.leadname AS lead_source'
        )));
        if (!empty($detailsObj) && $detailsObj->num_rows() > 0) {
            $details              = new stdClass(); 
            $details->status      = 'succces';
            $details->message     = 'Lead details retrived successfully';
            
            $result               = $detailsObj->row();
            $result->lead_history = $this->lead_model->get_lead_display_activity($lead_id, array(
                'select' => array(
                    'lead_activity_id',
                    'status_id',
                    'act_status_date AS history_date',
                    'status_name'
                )
            ))->result();
            $result->all_status  = $this->lead_model->lead_status();
            $result->policy_type = array(
                'id' => '',
                'policyname' => '',
                'image' => ''
            );

            /* Validation */
            if (!empty($result->lead_img)) {
                $result->lead_img = base_url("assets/images/user_images/{$details->lead_img}");
            }

            $details->data = $result;
        } else {
            $details = new stdClass();
            $details->status  = 'failed';
            $details->message = 'Error: There is no such a lead available!';
            $details->data    = array();
        }
        $this->response($details, REST_Controller::HTTP_OK);
    }

    /**
     * API lead details
     * Created by siva shankar
     * @param lead ID
     */
    public function activity_post() {
        $request = $this->input->post();
        $files = $_FILES;
        $message  = new stdClass(); 
        if (!empty($request) && !empty($request['lead_id'])) {
            $activity_data = array(
                'act_lead_id' => $request['lead_id'],
                'act_lead_status_id' => $request['lead_status'],
                'act_status_date' => datetime_saveformater($request['datetime']),
                'act_comment' => $request['comment'],
                'act_doc_path' => '',
                'act_created_by' => $request['user_id']
            );

            $upName = '';
            // if (!empty($files['doc']['name']) && empty($files['doc']['error'])) {
                
            //     //exit();
            //     $upName = $this->file_handling->file_upload(
            //         'uploads/lead_doc', 
            //         'doc', 
            //         $files['doc'], 
            //         'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF|pdf|PDF|doc|docx|DOC|DOCX|xls|XLS|xlsx|XLSX'
            //     );
            //     $activity_data['act_doc_path'] = $upName;
            // }
            if (!empty($files['doc_upload']['name']) && empty($files['doc_upload']['error'])) {
               // $message = $files['doc_upload']['name'];
                //exit();
                $upName = $this->file_handling->file_upload(
                    'uploads/lead_doc', 
                    'doc_upload', 
                    $files['doc_upload'], 
                    '*'
                    // jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF|pdf|PDF|doc|docx|DOC|DOCX|xls|XLS|xlsx|XLSX
                );
                $activity_data['act_doc_path'] = $upName;
            }
            //print_r($activity_data); die();
            $rtn_status = $this->lead_model->lead_activity_status($activity_data);

            if (!empty($rtn_status)) {
                $message->status  = 'success';
                $message->message = 'The status has been updated successfully';
                $message->data    = array('activity_id' => $rtn_status);
            } else {
                $message->status  = 'failed';
                $message->message = 'Error: The status has been failed to update';
                $message->data    = array();    
            }
        } else {
            $message->status  = 'failed';
            $message->message = 'Error: The status has been failed to update';
            $message->data    = array();
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }

    /**
     * Saving recorded history for audio
     * Created by Siva Shankar
     * @param lead ID, Files
     */
    function recorder_history_audio_post() {
        $rtn_status = array(
            'status' => 'false',
            'message' => 'Audio upload failed',
        );
        $lead_id = $this->input->post('lead_id');
        $user_id = $this->input->post('user_id');
        $files = $_FILES;

        //print_r($files); die();

        if (!empty($user_id) && !empty($lead_id) && !empty($files['audiup'])) {
            //echo 123; die();
            $folder_location = "uploads/recorded_history/audio/{$lead_id}";
            if (!is_dir($folder_location)) {
                mkdir("./$folder_location", 0777, TRUE);
            }

            if (!empty($files['audiup']['name'])) {
                $filesCount = count($files['audiup']['name']);

                for ($i=0; $i < $filesCount; $i++) { 
                    $upName = '';
                    if (!empty($files['audiup']['name'][$i]) && empty($files['audiup']['error'][$i])) {
                        $pass = array(
                            'name' => $files['audiup']['name'][$i],
                            'type' => $files['audiup']['type'][$i],
                            'tmp_name' => $files['audiup']['tmp_name'][$i],
                            'error' => $files['audiup']['error'][$i],
                            'size' => $files['audiup']['size'][$i]
                        );
                    //print_r($pass); die();
                        $upName = $this->file_handling->file_upload(
                            "$folder_location", 
                            $files['audiup']['name'][$i], 
                            $pass, 
                            '*'
                        );
                        if (!empty($upName)) {
                            $recorded_history = array();
                            $recorded_history['lead_id'] = $lead_id;
                            $recorded_history['file_type'] = 'audio';
                            $recorded_history['file_path'] = "$folder_location/$upName";
                            $recorded_history['mime_type'] = $files['audiup']['type'][$i];
                            $recorded_history['created_by'] = $user_id;

                            $this->lead_model->recorded_history($recorded_history);
                        }
                    }
                }
            }

            $rtn_status['status'] = 'true';
            $rtn_status['message'] = 'Audio upload has been successfully uploaded!';
            
            $this->response($rtn_status, REST_Controller::HTTP_OK);            
        } else {
            $this->response($rtn_status, REST_Controller::HTTP_OK);
        }
    }    

    /**
     * Saving recorded history for vedio
     * Created by Siva Shankar
     * @param lead ID, Files
     */
    function recorder_history_vedio_post() {
        $rtn_status = array(
            'status' => 'false',
            'message' => 'Video upload failed',
        );
        $lead_id = $this->input->post('lead_id');
        $user_id = $this->input->post('user_id');
        $files = $_FILES;

        //print_r($files); die();

        if (!empty($user_id) && !empty($lead_id) && !empty($files['vedi'])) {

            $folder_location = "uploads/recorded_history/vedio/{$lead_id}";
            if (!is_dir($folder_location)) {
                mkdir("./$folder_location", 0777, TRUE);
            }

            if (!empty($files['vedi']['name'])) {
                $filesCount = count($files['vedi']['name']);

                for ($i=0; $i < $filesCount; $i++) { 
                    $upName = '';
                    if (!empty($files['vedi']['name'][$i]) && empty($files['vedi']['error'][$i])) {
                        $pass = array(
                            'name' => $files['vedi']['name'][$i],
                            'type' => $files['vedi']['type'][$i],
                            'tmp_name' => $files['vedi']['tmp_name'][$i],
                            'error' => $files['vedi']['error'][$i],
                            'size' => $files['vedi']['size'][$i]
                        );
                    //print_r($pass); die();
                        $upName = $this->file_handling->file_upload(
                            "$folder_location", 
                            $files['vedi']['name'][$i], 
                            $pass, 
                            '*'
                        );
                        if (!empty($upName)) {
                            $recorded_history = array();
                            $recorded_history['lead_id'] = $lead_id;
                            $recorded_history['file_type'] = 'vedio';
                            $recorded_history['file_path'] = "$folder_location/$upName";
                            $recorded_history['mime_type'] = $files['vedi']['type'][$i];
                            $recorded_history['created_by'] = $user_id;

                            $this->lead_model->recorded_history($recorded_history);
                        }
                    }
                }
            }

            $rtn_status['status'] = 'true';
            $rtn_status['message'] = 'Video upload has been successfully uploaded!';
            
            $this->response($rtn_status, REST_Controller::HTTP_OK);            
        } else {
            $this->response($rtn_status, REST_Controller::HTTP_OK);
        }
    }    

    /**
     * Retriving recorded history for vedio
     * Created by Siva Shankar
     * @param lead ID
     */
    // function get_recorder_history_post() {
    //     $lead_id = $this->input->post('lead_id');

    //     $data = $this->lead_model->get_recorded_history($lead_id);
    //  echo "<pre>";print_r($data);
    //    //  $file_type = $data->result_array();
    //    // if($file_type=='audio'){
    //    //  echo "audio";
    //    // }else{
    //    //  echo "video";
    //    // }
    //    // exit();
    //     // // file_type => audio,video file_type
    //     if (!empty($data) && $data->num_rows() > 0) {
    //         $rtn_status = array(
    //             'status' => 'success',
    //             'message' => "Retrived successfully",
    //             'data' => $data->result()
    //         );
    //     } else {
    //         $rtn_status = array(
    //             'status' => 'error',
    //             'message' => "Failed to retrived history",
    //             'data' => $data->result()
    //         );
    //     }
    //     $this->response($rtn_status, REST_Controller::HTTP_OK);
    // } 
    function get_recorder_history_post() {
        $lead_id = $this->input->post('lead_id');

        // $data = $this->lead_model->get_recorded_history($lead_id);
        $data['audio'] = $this->lead_model->get_recorded_audio_history($lead_id);
        $data['vedio'] = $this->lead_model->get_recorded_vedio_history($lead_id);
        if (!empty($data)) {
            $rtn_status = array(
                'status' => 'success',
                'message' => "Retrived successfully",
                'audio' => $data['audio'],
                'video' => $data['vedio']
            );
        } else {
            $rtn_status = array(
                'status' => 'error',
                'message' => "Failed to retrived history",
                'audio' => array(),
                'video' => array()
            );
        }
        $this->response($rtn_status, REST_Controller::HTTP_OK);
    }
}

?>