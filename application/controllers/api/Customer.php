<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Customer extends REST_Controller {
	
    function __construct() {
        // Construct the parent class
        parent::__construct();
    }
    public function test_post() {
		$message = ['designer_status' => 'yes', 'message' => 'New customer added successfully'];
		$this->set_response($message, REST_Controller::HTTP_OK);
	}	
}
?>