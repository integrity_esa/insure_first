<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Login extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('user_login_model', 'email_model'));
    }

    /* SENTHAMIZH SELVI - Login Mobile api 14-12-2018 */

    public function user_login_post() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $userData = $this->user_login_model->login($email, $password);

        if (!$userData) {
            $data = (object) array();
            $message = ['status' => '0', 'user_info' => $data, 'message' => 'Invalid Login'];
        } else {
            $data = array(
                'user_name' => $userData['first_name'],
                'last_name' => $userData['last_name'],
                'user_id' => $userData['user_id'],
                'email' => $userData['email'],
                'mobile_number' => $userData['phone'],
                'user_image' => base_url() . 'uploads/' . $userData['profile_picture'],
                'role_id' => $userData['role_id']
            );
            $message = ['status' => '1', 'user_info' => $data, 'message' => 'Login Successfull'];
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    /* SENTHAMIZH SELVI - Login Mobile api 14-12-2018 */

    public function forgot_password_post() {
        $mobile = $this->input->post('mobile_number');
        $result = $this->user_login_model->get_phone($mobile);
        $rand = rand(999, 10000);
        if (!$result) {
            $message = ['status' => '0', 'message' => 'Unregistered mobile number'];
        } else {
            $data = array('otp_number' => $rand);
            $res = $this->user_login_model->update($data, 'phone', $mobile, 'users');
            if ($res) {
                $result = $this->email_model->forgot_password_mail($mobile, $rand);
                if ($result) {
                    $message = ['status' => '1', 'message' => 'OTP mail send successful'];
                } else {
                    $message = ['status' => '0', 'message' => 'Failed to send mail'];
                }
            } else {
                $message = ['status' => '0', 'message' => 'Failed to submit'];
            }
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    /* SENTHAMIZH SELVI - Login Mobile api 14-12-2018 */

    public function otp_verification_post() {

        $data['mobile'] = $this->input->post('mobile_number');
        $data['otp'] = $this->input->post('otp_number');

        $result = $this->user_login_model->get_phone($data['mobile']);

        if (!$result) {
            $message = ['status' => '0', 'message' => 'Unregistered mobile number'];
        } else {
            $res = $this->user_login_model->otp_validation($data);
            if ($res) {
                $message = ['status' => '1', 'message' => 'success'];
            } else {
                $message = ['status' => '0', 'message' => 'OTP Verification code is incorrect'];
            }
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    /* SENTHAMIZH SELVI - Change Password api 14-12-2018 */
    public function change_password_post() {
        
        $mobile = $this->input->post('mobile_number');
        $password = $this->input->post('new_password');

        $data = array('password' => sha1($password),
            'real_password' => $password);
        
        $result = $this->user_login_model->get_phone($mobile);
        
        if (!empty($result)){
            
            $res = $this->user_login_model->update($data, 'phone', $mobile, 'users');
            $message = ['status' => '1', 'message' => 'success'];
        } else {
            $message = ['status' => '0', 'message' => 'The Oassword that you have entered incorrect'];
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

}

?>