<?php
defined('BASEPATH') OR exit('No direct script access allowed');
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Policy_sold extends REST_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('location_model'));
    }
    
    public function index_post() {
        $userId = $this->input->post('user_id');
        $policy_sold_details = $this->location_model->policy_sold($userId);
       // echo "<pre>";print_r($policy_sold);exit();
       // $this->template->load('frontTemplate', 'policy_sold', $data);
       if (!empty($policy_sold_details)) {
        $message = ['status' => '1','policy_sold_details'=>$policy_sold_details,'message' => 'ok'];
    } else {
        $message = ['status' => '0','policy_sold_details'=>array(),'message' => 'No Policy sold details are available'];
    }
    $this->response($message, REST_Controller::HTTP_OK);
    }
    public function policy_sold_view_post(){
        $policy_sold_id = $this->input->post('policy_sold_id');
        $policy_sold_view = $this->location_model->policy_sold_view($policy_sold_id);
        if ($policy_sold_view) {
            $message = ['status' => '1','policy_sold_view'=>$policy_sold_view,'message' => 'ok'];
        } else {
            $message = ['status' => '0','policy_sold_view'=>array(),'message' => 'Policy sold id is not available'];
        }
        $this->response($message, REST_Controller::HTTP_OK);
    }

    
}
