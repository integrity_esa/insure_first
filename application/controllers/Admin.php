<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends Admin_Controller {
	
    public function __construct(){
		
    	parent::__construct(); 
        $this->load->model(array('admin_model','location_model','email_model','lead_model'));
        $this->load->library('excel');
    }

    /* SENTHAMIZHSELVI - Display login page 11-12-2018 */
	public function index() {

       if(!empty($this->session->userdata('admin'))){
            $this->load->view('admin/dashboard');

        }
        else{
            $data['page_title'] = "Login";
            $this->load->view('admin/login/login',$data);
        }
	}

    /* SENTHAMIZHSELVI - Display Ajax login 11-12-2018 */
    function ajax_login($ajax = false) {

        $getAdmin = $this->admin_model->checkAdmin($this->input->post());
        $redirect ='admin/dashboard';

        if($getAdmin) {
            $adminArr = array(
                'admin_name'    => $getAdmin['admin_name'],
                'admin_id'      => $getAdmin['admin_id'],
                'admin_email'   => $getAdmin['email_id'],
                'admin_role'    => $getAdmin['role'],
                'admin_logged_in'=> true
            );
            $this->session->set_userdata(['adminEmail' => $getAdmin['email_id']]);
            $this->session->set_userdata('admin', $adminArr);
            $admin              = $this->session->userdata('admin');

            echo json_encode(array('result'=>true,'redirect'=>$redirect));
            die();
        } 
        else {
            echo json_encode(array('result'=>false));
            die();
        }
    }

    /* SENTHAMIZHSELVI - Display Dashboard page 11-12-2018 */
	public function dashboard() {
        $data['page_title']     = "Dashboard";
		$this->load->view('admin/dashboard',$data);
	}

    /* SENTHAMIZHSELVI - Display logout page 12-12-2018 */
	function logout()
    {
        $this->session->unset_userdata('admin');
        $this->session->set_flashdata('message', 'Logout Successfully');
        redirect(base_url().'admin');
    }

     /* SENTHAMIZHSELVI - Forgot password page 12-12-2018 */
    function forgot_password() {
            $data['page_title'] = "Forgot Password";
            $this->load->view('admin/login/forgot_password',$data);
    }

/* Velmurugan -  email_forget_password 21-12-2018 */

    public function admin_forgot_password($ajax = false) {
       
        $email = $this->input->post('email');
        $result = $this->admin_model->forgot_password($email);
        if (!$result) {
            $message = ['status' => '0', 'message' => 'Unregistered_email'];
        } else {
            
                $result = $this->email_model->admin_forgot_password_mail($email);
               
                if ($result) {
                    $message = ['status' => '1', 'message' => 'Mail_send_successfully', 'email_id' => $email];


                } else {
                    $message = ['status' => '0', 'message' => 'Failed_to_send_mail'];
                }
            } 
       
        echo json_encode($message);
        die();
    }

    /* Change password*/
    public function change_password(){
        $data['page_title']= 'Change Password';
        $this->load->view('admin/login/change_password',$data);

    }
    public function change_new_password(){

        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        $password = sha1($pass);

        $result = $this->admin_model->admin_change_password($password,$pass,$email);
        if (!$result) {
                $message = ['status' => '0', 'message' => 'Failed_to_change_password'];
            } else {
                               
                   
                    if ($result) {
                        $message = ['status' => '1', 'message' => 'Password_changed_successfully', 'password' => $password];


                    } else {
                        $message = ['status' => '0', 'message' => 'Failed_to_change_password'];
                    }
                } 
           
            echo json_encode($message);
            die();
    }
    /**listing leads(24-12-2018) **/
    public function load_lead() {
        $data['page_title']     = "Lead Listing";
        $data['leadLists']      = $this->listing_lead();
        $this->load->view('admin/lead/lead_form',$data);
    }
    /**Delete function in lead**/
     public function delete_Lead() {
        $admin                  = $this->session->userdata('admin');
        $adminId                = $admin['admin_id'];
        $request                = $this->input->post('valDel');
        $data['deleteListing']  = $this->admin_model->deleteLead($request,$adminId);
         if (!empty($data)) {
             $status            ="success";
        } else {
           $status              ="Error";
        }
       echo json_encode($status);
    }

    public function listing_lead() {
        $data['page_title']     = "Lead Listing";
        $admin                  = $this->session->userdata('admin');
        $admin_id               = $admin['admin_id'];
        $data                   = $this->admin_model->getlead($admin_id);
        return $data;   
    }
    /**addnew leads in admin side**/
    public function adminAddNew() {
        $data['page_title']     = "Admin Lead Form";
        $data['leadLists']      = $this->admin_listing_lead();        
    }
    
    public function admin_listing_lead() {
        $request                = $this->input->post();
        $files                  = $_FILES;
        $data['page_title']     = "Admin Lead Form";
        $admin                  = $this->session->userdata('admin');
        $adminId                = $admin['admin_id'];
        $id                     = $this->input->get('id', TRUE);
        $data['editListing']    = array();
        if(!empty($id)){
            $data['editListing']    = $this->admin_model->editLead($id);
        }
        //echo "<pre>";print_r($data['editListing']);exit();
        if (!empty($this->input->post())) {
            $saveuser           = $this->admin_model->admininsert_user($request,$adminId,$files);
            if (!empty($saveuser)) {
                $status         ="success";
                $this->session->set_flashdata('message', 'create Successfully !!!');
            } else {
                $status         ="Error";
                $this->session->set_flashdata('error', 'Error Occurred in Insert !!!');
            }
            die(json_encode(array('status'=>$status)));
        }
        $data['getCountry']     = $this->admin_model->get_all_order('country', '', '', 'country_name', 'ASC');
        /*$data['getState']     = $this->admin_model->get_all_order('state', '', '', 'name', 'ASC');
        $data['getCity']        = $this->admin_model->get_all_order('city', '', '', 'city_name', 'ASC');*/
        $data['leadtypeVal']    = $this->admin_model->get_all_order('lead_type', '', '', 'leadname', 'ASC');
        $data['leadcategoryVal']= $this->admin_model->get_all_order('lead_category', '', '', 'category_name', 'ASC');
        $data['leadpolicyVal']  = $this->admin_model->get_all_order('policy_type', '', '', 'policy_name', 'ASC');
        $data['leadstatusVal']  = $this->admin_model->get_all_order('lead_status', '', '', 'status_name', 'ASC');


        $this->load->view('admin/lead/admin_lead_form',$data); 
    }

    /**listing lead type(28-12-2018) **/
    public function lead_type() {
        $data['page_title']     = "Lead Listing";
        $data['leadListing']    = $this->admin_model->leadTypeList();
        $this->load->view('admin/lead/lead_type',$data);
    }

    /**add lead type(28-12-2018)**/
    public function leadTypes() {
        $data['page_title']     = "Admin Lead Type";
        $id                     = $this->input->get('id', TRUE);
        $data['edit_lead_type']    = $this->admin_model->edit_lead_type($id);
       $this->load->view('admin/lead/leadTypes',$data); 
    }
    /**Delete function in lead**/
    public function delete_lead_type() {
        $request                = $this->input->post('id');
        $data['deleteListing']  = $this->admin_model->delete_lead_type($request);
         if (!empty($data)) {
             $status            ="success";
        } else {
           $status              ="Error";
        }
       echo json_encode($status);
    }

    /**insert lead type(28-12-2018)**/
    public function adminLeadType() {
        $data['page_title']     = "Lead Type";
        $request                = $this->input->post();
        //$data                 = $this->admin_model->insertLeadType($request);
        $check_category         = $this->admin_model->check_category('lead_id','lead_type',$request,'leadname');
        if($check_category->num_rows() > 0 )
        { 
            $status = 'category_exist';
        }
        else
        { 
            $data  = $this->admin_model->insertLeadType($request);
            if (!empty($data)) {
               $status            ="success";
            } else {
               $status              ="Error";
            }
        }
       echo json_encode($status);
    } 

    /**listing lead Category(28-12-2018) **/
    public function Lead_Category() {
        $data['page_title']     = "Lead Category";
        $data['categoryVal']    = $this->admin_model->leadCategoryList();
        $this->load->view('admin/lead/Lead_Category',$data);
    }

    /**add lead Category(28-12-2018)**/
    public function leadCategory() {
        $data['page_title']     = "Lead Category Type";
        $id                     = $this->input->get('id', TRUE);
        $data['edit_category']    = $this->admin_model->edit_category($id);
        $this->load->view('admin/lead/addCategory',$data); 
    }

    /**insert lead type**/
    public function adminCategory() {
        $data['page_title']     = "Category Type";
        $request                = $this->input->post();
        $check_category  = $this->admin_model->check_category('category_id','lead_category',$request,'category_name');
        if($check_category->num_rows() > 0 )
        { 
            $status = 'category_exist';
        }
        else
        { 
            $data  = $this->admin_model->insertCategoryType($request);
            if (!empty($data)) {
               $status            ="success";
            } else {
               $status              ="Error";
            }
        }
       echo json_encode($status);
    } 
    /**delete lead category**/
    public function delete_lead_category() {
        $request                = $this->input->post('id');
        $data['delete_lead_category']  = $this->admin_model->delete_lead_category($request);
        if (!empty($data)) {
             $status            ="success";
        } else {
           $status              ="Error";
        }
       echo json_encode($status);
    }
    /**listing policy type(02-01-2019)**/
    public function policy_type() {
        $data['page_title']     = "Policy Listing";
        $data['policyVal']      = $this->admin_model->leadpolicyList();
        $this->load->view('admin/lead/policy_type',$data);
    }

    /**add policy type(02-01-2019)**/
    public function policyType() {
    $data['page_title']     = "Admin police Type";
    $id                     = $this->input->get('id', TRUE);
    $data['edit_policy_type']  = $this->admin_model->edit_policy_type($id);
    $this->load->view('admin/lead/policyType',$data); 
    }
     /**delete policy type**/
    public function delete_policy_type() {
        $request                = $this->input->post('id');
        $data['delete_policy_type']  = $this->admin_model->delete_policy_type($request);
        if (!empty($data)) {
             $status            ="success";
        } else {
           $status              ="Error";
        }
       echo json_encode($status);
    }
    /**insert lead type(02-01-2019)**/
    public function adminpolicyType() {
        $data['page_title']     = "Lead Type";
        $request                = $this->input->post();
        $check_category         = $this->admin_model->check_category('policy_id','policy_type',$request,'policy_name');
       // echo "<pre>";print_r($check_category);exit();
        if($check_category->num_rows() > 0 )
        { 
            $status = 'category_exist';
        }
        else
        { 
            $data  = $this->admin_model->insertpolicyType($request);
            if (!empty($data)) {
               $status            ="success";
            } else {
               $status            ="Error";
            }
        }
       echo json_encode($status);
    } 

     /**listing lead status(02-01-2019) **/
    public function lead_status() {
        $data['page_title']     = "Lead Status Listing";
        $data['leadstatus']    = $this->admin_model->leadstatusList();
        $this->load->view('admin/lead/lead_status',$data);
    }

    /**add lead type(02-01-2019)**/
    public function leadStatus() {
        $data['page_title']     = "Admin Lead Status";
        $id                     = $this->input->get('id', TRUE);
        $data['edit_lead_status']    = $this->admin_model->edit_lead_status($id);
       $this->load->view('admin/lead/leadStatus',$data); 
    }

    /**insert lead type(02-01-2019)**/
    public function adminLeadStatus() {
        $data['page_title']     = "Lead Status";
        $request                = $this->input->post();
        $check_category         = $this->admin_model->check_category('status_id','lead_status',$request,'status_name');
        if($check_category->num_rows() > 0 )
        { 
            $status = 'category_exist';
        }
        else
        { 
            $data  = $this->admin_model->insertLeadStatus($request);
            if (!empty($data)) {
               $status            ="success";
            } else {
               $status            ="Error";
            }
        }
       echo json_encode($status);
    } 
    /**delete lead status**/
    public function delete_lead_status() {
        $request                = $this->input->post('id');
        $data['delete_lead_status']  = $this->admin_model->delete_lead_status($request);
         if (!empty($data)) {
             $status            ="success";
        } else {
           $status              ="Error";
        }
       echo json_encode($status);
    }

   /**get state value based on contury**/  
     public function getAllState() {
        $request                = $this->input->post();
        $states                 = $this->admin_model->statevalue($request);
        $stateHtml              = '<option value="">-- Select State --</option>';

        if(!empty($states)){
            foreach($states as $state){
                $stateHtml.='<option value="'.$state['state_id'].'">'.$state['name'].'</option>';
            }
        }
        die(json_encode(array('stateHtml'=>$stateHtml)));
    }

    /**get state value based on city**/  
    public function getAllCity() {
        $request                = $this->input->post();
        $citys                  = $this->admin_model->cityvalue($request);
        $cityHtml               ='<option value="">-- Select City --</option>';
        if(!empty($citys)){
            foreach($citys as $city){
                $cityHtml.='<option value="'.$city['city_id'].'">'.$city['city_name'].'</option>';
            }
        }
        die(json_encode(array('cityHtml'=>$cityHtml)));
    } 
    
    /**email exit or not check**/
    public function checkEmail() {
        $email                  = $this->input->post('userEmail');
        $emailCheck             = $this->admin_model->get('email', $email, 'lead');
        if (!$emailCheck) {
            echo 0;
        } else {
            echo 1;
        }
    }
    
    /* Velmurugan - Display User page 21-12-2018 */
    public function users() {

        $data['page_title']     = "Users";
        $data['users']          = $this->admin_model->user_list();
        //echo "<pre>";print_r($data['users']);exit();
        $this->load->view('admin/users/index',$data);
    }
     /**email exit or not check**/
    public function checkUseremail() {
        $email                  = $this->input->post('userEmail');
       // echo "<pre>";print_r($email);exit();
        $id                     = $this->input->post('id');
        //echo $id;exit();
        if(!empty($id)){
        $emailCheck = $this->admin_model->checkEmail($email,$id);
        }else{
        $emailCheck = $this->admin_model->checkEmail($email,$id);
        }
        //$emailCheck             = $this->admin_model->get('email', $email, 'users',$id);
        //echo "<pre>";print_r($emailCheck);exit();
        if(!empty($emailCheck)){
            echo json_encode(array('query'=>1));
            //echo 1;
        } else {
            echo json_encode(array('query'=>0));
            //echo 0;
        }
    }
    /**phone exit or not check**/
    public function checkUserphone() {
        $phone                  = $this->input->post('phone');
        $id                     = $this->input->post('id');
        //$phoneCheck             = $this->admin_model->get('phone', $phone, 'users',$id);
        if(!empty($id)){
            $phoneCheck = $this->admin_model->checkPhone($phone,$id);
            }else{
            $phoneCheck = $this->admin_model->checkPhone($phone,$id);
            }
           
            if(!empty($phoneCheck)){
                echo json_encode(array('query'=>1));
                //echo 1;
            } else {
                echo json_encode(array('query'=>0));
                //echo 0;
            }
    }
    /**addnew user**/
    public function add_user() {
        $data['page_title']     = "New User";
        $data['page_title2']    = "Users";
        $data['roles']          = $this->admin_model->role_list();
        $this->load->view('admin/users/add',$data);      
    }
    /* Velmurugan - Display User page 22-12-2018 */
    public function add_user_insert() {
        $data['page_title']    = "Users";
        $values                = $this->input->post();
        $files                 = $_FILES;
        //echo "<pre>";print_r($values);print_r($files);exit();
        if(!empty($values)){
            $data['user'] = $this->admin_model->new_user($values,$files);
            if ($data['user']) {
            $message = ['status' => '1', 'message' => 'User_added_successfully'];
            } else {
                $message = ['status' => '0', 'message' => 'Failed_to_add_user'];
            }
            echo json_encode($message);    
        }
    }


    public function edit_user(){
        $data['page_title'] = "Edit";
        $data['page_title2'] = "users";
        $id = $this->input->get('user_id');
        // echo "<pre>";print_r($id);exit();
        $data['roles']          = $this->admin_model->role_list();        
        $data['values'] = $this->admin_model->get_user($id);
        // echo "<pre>";print_r($data['values']);exit();
        $this->load->view('admin/users/edit',$data);
    }

    public function update_user() {

        //$data['page_title'] = "Edit";
        $id = $this->input->post('user_id');
        $values = $this->input->post('formData');
        $files                  = $_FILES; 
       
       $data['update'] = $this->admin_model->user_update($values,$id,$files );
        //$this->load->view('admin/users/edit',$data);
    }

    public function delete_user() {
        $id = $this->input->post('id') ;
        $delete = $this->admin_model->user_delete($id);
        if (!empty($delete)) {
             $status         ="success";
        } else {
           $status         ="Error";
        }
      die(json_encode(array('status'=>$status))); 

    }
    ////////// update status
     public function update_user_status() {
        $id = $this->input->post('id') ;
        $status = $this->input->post('status');
        $update_status = $this->admin_model->user_status($id,$status);
        if (!empty($update_status)) {
             $status         ="success";
        } else {
           $status         ="Error";
        }
      die(json_encode(array('status'=>$status)));
    }

////////////////////////
    public function role() {

        $data['page_title']     = "Roles";
         $data['roles']         = $this->admin_model->role_list();
        $this->load->view('admin/roles/index',$data);
    }

   public function add_role() {
        $data = array();
        $data['page_title']     = "New Role";
        $val['role_name']       = $this->input->post('role_name');
        $val['permissions']     = json_encode($this->input->post('permissions'));
        $val['status']          = 1;
        $val['created_at']      = date('Y-m-d H:i:s');
        $val['updated_at']      = date('Y-m-d H:i:s');
              // echo "<pre>"; print_r($val);exit; 
        if(!empty($val['role_name'])){
             $val['values']     =  $this->admin_model->add_new_role($val);
        }
      // echo '<pre>';print_R($data);echo '</pre>';exit();
        $this->load->view('admin/roles/add_role',$data);
    }

    public function edit_role(){
        $data['page_title']     = "Edit Role";
         $this->load->view('admin/roles/edit_role',$data);
    }
    public function role_delete(){
   
        $id = $this->input->post('id') ;
            $delete = $this->admin_model->delete_role($id);
        if (!empty($delete)) {
             $status         ="success";
        } else {
           $status         ="Error";
        }
      die(json_encode(array('status'=>$status))); 

        
    }   
    public function branch() {

        $data['page_title']     = "Branches";
         $data['branches']      = $this->admin_model->branch_list();
        $this->load->view('admin/branch/index',$data);
    }

   public function add_branch() {
        $data = array();
        $data['page_title']    = "New branch";
        $val['branch_name']     = $this->input->post('branch_name');
        $val['location']        = $this->input->post('location');
        $val['status']          = 1;
        $val['created_at']    = date('Y-m-d H:i:s');
        $val['updated_at']    = date('Y-m-d H:i:s');
        
        if(!empty($val['branch_name'])){
             $val['values']     =  $this->admin_model->add_new_branch($val);
        }
    // echo '<pre>';print_R($data);echo '</pre>';exit();
        $this->load->view('admin/branch/add_branch',$data);
    }

      ////////////////// Lead Mangement

    public function lead_manage(){
        $data['page_title']= 'Lead Management';
        $data['distance'] = $this->location_model->lead_dist();
        $data['fsos'] = $this->location_model->fso_list();
        $id = $this->input->post('user_id');
        $kms = $this->input->post('kms');
        $lmt = $this->input->post('limit');
        if(!empty($id)){
            $data['leads'] = $this->location_model->lead_list($id,$kms,$lmt);
            $data['user_id'] =$id;
          // echo "<pre>";print_r($data['leads']);exit();           
            //die(json_encode(array('leads'=> $data['leads'] ?? array(),'user_id'=>$data['user_id'])));
            die(json_encode(array('leads'=> $data['leads'] ? $data['leads']: array(),'user_id'=>$data['user_id'])));
            
        }
       // echo '<pre>';
       //print_r($data);
       //exit;
        $this->load->view('admin/assign_lead/index',$data);
    }
    public function check_lead_id_update() {
        $post = $this->input->post();
        $flag = 0; // Failure
        /* if(!empty($post['lead_id'])){
            $this->db->set('reference_id', $post['user_id']);
            $this->db->where_in('lead_id', $post['lead_id']);
            $this->db->update('assign_lead');
            $flag = 1; // Success
        } */
        if(!empty($post['check_fso_lead'])){
             $this->db->set('reference_id', $post['user_id']);
             $this->db->where_in('lead_id', $post['check_fso_lead']);
             $this->db->update('assign_lead');
             $flag = 1; // Success
         } else{
            $flag = 2;
         }
        die(json_encode($flag));



    }
////////////////////GENERAL SETTINGS//////////////////////
    public function settings(){
        $data['page_title']= 'Settings';
        $data['fsos'] = $this->location_model->fso_list();
        $id = $this->input->post('user_id');
        $data['leads'] = $this->location_model->lead_list($id);
      // print_r($data);
      // exit;
        $this->load->view('admin/settings/index',$data);
    }



 /**listing campaign type(02-01-2019)**/
    public function campaign() {
        $data['page_title']     = "Campaign Listing";
        $data['campaignVal']      = $this->admin_model->campaignList();
        $this->load->view('admin/lead/campaign_index',$data);
    }

    /**add policy type(02-01-2019)**/
    public function campaignType() {
    $data['page_title']     = "Admin Campaign";
    $id                     = $this->input->get('id', TRUE);
    $data['edit_campaign']  = $this->admin_model->edit_campaign($id);
    $this->load->view('admin/lead/campaign',$data); 
    }
     /**delete policy type**/
    public function delete_campaign() {
        $request                = $this->input->post('id');
        $data['delete_campaign']  = $this->admin_model->delete_campaign($request);
        if (!empty($data)) {
             $status            ="success";
        } else {
           $status              ="Error";
        }
       echo json_encode($status);
    }
    /**insert lead type(02-01-2019)**/
    public function adminCampaign() {
        $data['page_title']     = "Lead Type";
        $request                = $this->input->post();
       // echo "<pre>";print_r($request);exit;
        $check_category         = $this->admin_model->check_category('campaign_id','campaign_type',$request,'campaign_name');
        //echo "<pre>";print_r($check_category->num_rows());exit();
        if($check_category->num_rows() > 0 )
        { 
            $status = 'category_exist';
        }
        else
        { 
            $data  = $this->admin_model->insertCampaign($request);
            if (!empty($data)) {
               $status            ="success";
            } else {
               $status            ="Error";
            }
        }
       echo json_encode($status);
    } 
    public function assign_branch_view(){
        $data['page_title']= 'Assign Branch';
        $data['branchHeads'] = $this->location_model->branchHead_list();
        $this->load->view('admin/assign_branch/index',$data);
    }
    public function assign_branch_fso_view(){
        $data['page_title']= 'Assign Branch Fso';
        $data['branchHeads'] = $this->location_model->branchHead_list();
        $this->load->view('admin/assign_branch/branch_fso_assign',$data);
    }

    public function assign_branch(){
        $data['page_title']= 'Assign Branch';
        $data['distance'] = $this->location_model->lead_dist();
        $id = $this->input->post('user_id');
        /* $kms = $this->input->post('kms');
        $lmt = $this->input->post('limit'); */
        $branchrole_id=1;
        $kms = $data['distance']['kilometer'];
        $lmt = $data['distance']['item_number'];
       // $data['branchHeads'] = $this->location_model->branchHead_list();
        $data['branchManagers'] = $this->location_model->branchManagerlist($kms,$id, $branchrole_id);
        echo json_encode(array('branchManagers'=>$data['branchManagers']));

    }
    public function assign_branch_fso(){
        $data['page_title']= 'Assign Branch Fso';
        $data['distance'] = $this->location_model->lead_dist();
       
        $user_id = $this->input->post('user_id');
        /* $kms = $this->input->post('kms');
        $lmt = $this->input->post('limit'); */
        $role_id=2;
        $kms = $data['distance']['kilometer'];
        $lmt = $data['distance']['item_number'];
       // $data['branchHeads'] = $this->location_model->branchHead_list();
        // $data['branchManagers'] = $this->location_model->branchManagerlist($kms,$id, $branchrole_id);
        $data['branch_manager_fso'] = $this->location_model->branch_manager_fso_list($kms,$user_id,$role_id);
        echo json_encode(array('branch_manager_fso'=>$data['branch_manager_fso']));

    }
    //excel import
    public function assign_branch_lead() {
         if(isset($_FILES["file"]["name"]))
        {
            // echo "<pre>";
            // print_r($this->input->post('branch_manager_user_id'));
            //print_r($this->input->post('branch_head_user_id'));
            // exit();
            $branch_manager_id = $this->input->post('branch_manager_user_id');
            $branch_head_user_id = $this->input->post('branch_head_user_id');
            $branch_manager_fso__user_id = $this->input->post('branch_manager_fso__user_id');
            
            // echo "<pre>";print_r($branch_manager_fso__user_id);exit();
            $today = date("Y-m-d H:i:s");
            $user_data   = $this->session->userdata('admin');
            //echo "<pre>";print_r($user_data);exit();
            $admin_id = $user_data['admin_id'];
            $admin_role = $user_data['admin_role'];
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                
                for($row=2; $row<=$highestRow; $row++)
                {

                    //echo "<pre>";print_r($data['branch_manager_fso']);exit;
                    
                    
                $title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                $fname = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $lname = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $aadharnum = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                $address1 = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                $address2 = $worksheet->getCellByColumnAndRow(5, $row)->getValue();

                $country_name = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                $country_where = " REPLACE(`country_name`, ' ', '') LIKE '$country_name%' OR `country_name` LIKE '$country_name%' ";
                $country = $this->admin_model->getDataFromTable('country_id', 'country', $country_where)['country_id'];
                //echo $this->db->last_query();exit();
                //$country = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                //$state = $worksheet->getCellByColumnAndRow(7, $row)->getValue();

                $state_name = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                $state_where = " REPLACE(`name`, ' ', '') LIKE '%$state_name%' OR `name` LIKE '%$state_name%' ";
                $state = $this->admin_model->getDataFromTable('state_id', 'state', $state_where)['state_id'];
                //echo $this->db->last_query();exit();
                $city_name = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $city_where = " REPLACE(`city_name`, ' ', '') LIKE '%$city_name%' OR `city_name` LIKE '%$city_name%' ";
                $city = $this->admin_model->getDataFromTable('city_id', 'city', $city_where)['city_id'];
                
                //$city = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $pincode = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                $phone = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                $email = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                $gender_id = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                if(strtolower($gender_id) == "male"){
                    $gender =1;
                }else{
                    $gender =2;
                }

                $dob = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                $dob = PHPExcel_Shared_Date::ExcelToPHP($dob);
                $company = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                $m_status_id = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                if(strtolower($m_status_id) == "single"){
                    $m_status =1;
                }else{
                    $m_status =2;
                }
                //echo "<pre>";print_r($m_status);exit();
                $spousename = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                $anniversary = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                $anniversary = PHPExcel_Shared_Date::ExcelToPHP($anniversary);
                // $policytype = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                //$policytype_name = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                $policytype_name = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                $policytype_where = " REPLACE(`policy_name`, ' ', '') LIKE '%$policytype_name%' OR `policy_name` LIKE '%$policytype_name%' ";
                $policytype = $this->admin_model->getDataFromTable('policy_id', 'policy_type', $policytype_where)['policy_id'];
                //echo $this->db->last_query();exit();

                $gname = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                $campaign = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                $leadtype = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

                $lead_name = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                $lead_where = " REPLACE(`leadname`, ' ', '') LIKE '%$lead_name%' OR `leadname` LIKE '%$lead_name%' ";
                $leadtype = $this->admin_model->getDataFromTable('lead_id', 'lead_type', $lead_where)['lead_id'];
              //  echo $this->db->last_query();exit();

                //$leadcategory = $worksheet->getCellByColumnAndRow(22, $row)->getValue();

                $leadcategory_name = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                $leadcategory_where = " REPLACE(`category_name`, ' ', '') LIKE '%$leadcategory_name%' OR `category_name` LIKE '%$leadcategory_name%' ";
                $leadcategory = $this->admin_model->getDataFromTable('category_id', 'lead_category', $leadcategory_where)['category_id'];
                //echo $this->db->last_query();exit();

                //$leadstatus = $worksheet->getCellByColumnAndRow(23, $row)->getValue();

                $leadstatus_name = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                $leadstatus_name_where = " REPLACE(`status_name`, ' ', '') LIKE '%$leadstatus_name%' OR `status_name` LIKE '%$leadstatus_name%' ";
                $leadstatus = $this->admin_model->getDataFromTable('status_id', 'lead_status', $leadstatus_name_where)['status_id'];
                //echo $this->db->last_query();exit();

                $renewal = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                $renewal = PHPExcel_Shared_Date::ExcelToPHP($renewal);
                $appointment = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                $campaign_name = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                $campaign_name_where = " REPLACE(`campaign_name`, ' ', '') LIKE '%$campaign_name%' OR `campaign_name` LIKE '%$campaign_name%' ";
                $campaign_type = $this->admin_model->getDataFromTable('campaign_id', 'campaign_type', $campaign_name_where)['campaign_id'];
               // echo $this->db->last_query();exit();

                $appointment = PHPExcel_Shared_Date::ExcelToPHP($appointment);
                // campaign 3 feilds start
                $campaign_owner = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                $campaign_location = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
                // campaign 3 feilds end
                $checkEmail = $this->admin_model->checkEmailExists($email,$phone);
                //print_r($checkEmail);
                if(!empty($checkEmail)){
                // check mail id and phone exists or not

                    $notInsertMails[]=$email;
                    $error[]="error";
                } else{
                // if mail and phone number is not there it will be inserted
                $lead = array(
                    'lead_title'        =>  $title,
                    'first_name'        =>  $fname,
                    'last_name'         =>  $lname,
                    'address_line1'     =>  $address1,
                    'address_line2'     =>  $address2,
                    'city_id'           =>  $city,
                    'state_id'          =>  $state,
                    'country_id'        =>  $country,
                    'pincode'           =>  $pincode,
                    'email'             =>  $email,
                    'phone'             =>  $phone,
                    'dob'               =>  date('Y-m-d', $dob), 
                    'gender'            =>  $gender,
                    'company'           =>  $company,  
                    'aadhar_id'         =>  $aadharnum, 
                    'marital_status'    =>  $m_status, 
                    'spouse_name'       =>  !empty($spousename)?$spousename:'', 
                    'anniversary'       =>  date('Y-m-d', $anniversary),
                    'modified_by'       => $admin_id, 
                    'status'            => 1,  
                    'created_at'        => $today,
                    'created_by'        => $admin_id,
                    'updated_at'        => $today
                    // 'lead_img'           => $imagename
                );
               // echo "<pre>";print_r($lead);exit();
                $insert_id = $this->db->insert('lead',$lead);
                $lead_id = $this->db->insert_id();
                $assign_lead =array(
                    'lead_id'         => $lead_id,
                    'garage_name'     => $gname ? $gname:'',
                    'campaign_name'   => $campaign_type ? $campaign_type: '',
                    'campaign_owner'  => $campaign_owner ? $campaign_owner: '',
                    'campaign_location'   => !empty($campaign_location)?$campaign_location:'',
                    'lead_type'       => $leadtype,
                    'lead_status'     => $leadstatus,
                    'lead_cat'        => $leadcategory,
                    'policy_type'     => $policytype,
                    'reference_id'    => $branch_manager_fso__user_id?$branch_manager_fso__user_id:'0',
                    'role_id'         => $admin_role,
                    'branch_head'     => $branch_head_user_id?$branch_head_user_id:'',
                    'branch_manager'  => $branch_manager_id?$branch_manager_id:'',
                    'created_by'      => $admin_id,
                    'created_at' =>$today,
                    'renewal_date'    => date('Y-m-d', $renewal), 
                    'appointment_date'=> date('Y-m-d', $appointment)
                ); 
                $this->db->insert('assign_lead',$assign_lead);
                //echo "test";exit;
                $this->assigned_branch_leads_data();
                //$success[] ="success";
                }
                }
            }
            // print_r($notInsertMails);
            // print_r($success);
             die(json_encode(array('existMails'=>!empty($notInsertMails)?implode(' , ',$notInsertMails):'')));
        }
    }

    public function assigned_branch_leads_data(){

        $data['get_all_assign_lead_data'] = $this->lead_model->get_all_assign_lead_data();
        $data['distance'] = $this->location_model->lead_dist();
        $kms = $data['distance']['kilometer'];
        $branch_manager_id = $this->input->post('branch_manager_user_id');
        $branch_head_user_id = $this->input->post('branch_head_user_id');
        $branch_manager_fso = $this->location_model->branch_manager_fso_list($kms,$branch_manager_id,2);
       // echo "<pre>";print_r($branch_manager_fso);exit;
        if(!empty($data['get_all_assign_lead_data'])){
            foreach ($data['get_all_assign_lead_data'] as $all_leads_data) {
               /*  echo "<pre>";
                print_r($all_leads_data);exit(); */
                 foreach ($branch_manager_fso as $i=>$fso) {
                     
                     if(!empty($fso)){
                        $check_assign= $this->lead_model->assign_lead_data($fso['user_id']);
                        //echo"<pre>";print_r($check_assign);exit;
                        //echo $this->db->last_query();
                        //echo "<pre>";echo count($check_assign);exit;
                            if(count($check_assign)<6){
                            $this->db->set('reference_id', $fso['user_id']);
                            $this->db->where('lead_id',$all_leads_data['lead_id']);
                            $this->db->update('assign_lead');

                            }
                         
                    } 
            
                }
            }
        }
    }

}
