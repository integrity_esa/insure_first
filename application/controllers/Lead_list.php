<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lead_list extends Admin_Controller {
    
    public function __construct(){
        
        parent::__construct(); 
        $this->load->model(array('lead_model','admin_model','location_model'));
        $this->load->library('excel');
    }
    public function import()
	{
		if(isset($_FILES["file"]["name"]))
		{
			$today = date("Y-m-d H:i:s");
        	$admin   = $this->session->userdata('admin');
        	$adminId = $admin['admin_id'];
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++)
				{
				$title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
				$fname = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
				$lname = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
				$aadharnum = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
				$address1 = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
				$address2 = $worksheet->getCellByColumnAndRow(5, $row)->getValue();

			    $country_name = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
			    $country_where = " REPLACE(`country_name`, ' ', '') LIKE '$country_name%' OR `country_name` LIKE '$country_name%' ";
			    $country = $this->admin_model->getDataFromTable('country_id', 'country', $country_where)['country_id'];
			    //echo $this->db->last_query();exit();
				//$country = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
				//$state = $worksheet->getCellByColumnAndRow(7, $row)->getValue();

				$state_name = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
				$state_where = " REPLACE(`name`, ' ', '') LIKE '%$state_name%' OR `name` LIKE '%$state_name%' ";
				$state = $this->admin_model->getDataFromTable('state_id', 'state', $state_where)['state_id'];
				//echo $this->db->last_query();exit();
				$city_name = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
				$city_where = " REPLACE(`city_name`, ' ', '') LIKE '%$city_name%' OR `city_name` LIKE '%$city_name%' ";
				$city = $this->admin_model->getDataFromTable('city_id', 'city', $city_where)['city_id'];
				
				//$city = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
				$pincode = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
				$phone = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
				$email = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
				$gender_id = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
				if(strtolower($gender_id) == "male"){
					$gender =1;
				}else{
					$gender =2;
				}

				$dob = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
				$dob = PHPExcel_Shared_Date::ExcelToPHP($dob);
				$company = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
				$m_status_id = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
				if(strtolower($m_status_id) == "single"){
					$m_status =1;
				}else{
					$m_status =2;
				}
				//echo "<pre>";print_r($m_status);exit();
				$spousename = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
				$anniversary = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
				$anniversary = PHPExcel_Shared_Date::ExcelToPHP($anniversary);
				// $policytype = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
				//$policytype_name = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
				$policytype_name = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
				$policytype_where = " REPLACE(`policy_name`, ' ', '') LIKE '%$policytype_name%' OR `policy_name` LIKE '%$policytype_name%' ";
				$policytype = $this->admin_model->getDataFromTable('policy_id', 'policy_type', $policytype_where)['policy_id'];
			    //echo $this->db->last_query();exit();

				$gname = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
				$campaign = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
				$leadtype = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

				$lead_name = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
				$lead_where = " REPLACE(`leadname`, ' ', '') LIKE '%$lead_name%' OR `leadname` LIKE '%$lead_name%' ";
				$leadtype = $this->admin_model->getDataFromTable('lead_id', 'lead_type', $lead_where)['lead_id'];
			  //  echo $this->db->last_query();exit();

				//$leadcategory = $worksheet->getCellByColumnAndRow(22, $row)->getValue();

				$leadcategory_name = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
				$leadcategory_where = " REPLACE(`category_name`, ' ', '') LIKE '%$leadcategory_name%' OR `category_name` LIKE '%$leadcategory_name%' ";
				$leadcategory = $this->admin_model->getDataFromTable('category_id', 'lead_category', $leadcategory_where)['category_id'];
			    //echo $this->db->last_query();exit();

				//$leadstatus = $worksheet->getCellByColumnAndRow(23, $row)->getValue();

				$leadstatus_name = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
				$leadstatus_name_where = " REPLACE(`status_name`, ' ', '') LIKE '%$leadstatus_name%' OR `status_name` LIKE '%$leadstatus_name%' ";
				$leadstatus = $this->admin_model->getDataFromTable('status_id', 'lead_status', $leadstatus_name_where)['status_id'];
			    //echo $this->db->last_query();exit();

				$renewal = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
				$renewal = PHPExcel_Shared_Date::ExcelToPHP($renewal);
				$appointment = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
				$campaign_name = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
				$campaign_name_where = " REPLACE(`campaign_name`, ' ', '') LIKE '%$campaign_name%' OR `campaign_name` LIKE '%$campaign_name%' ";
				$campaign_type = $this->admin_model->getDataFromTable('campaign_id', 'campaign_type', $campaign_name_where)['campaign_id'];
			   // echo $this->db->last_query();exit();

				$appointment = PHPExcel_Shared_Date::ExcelToPHP($appointment);
				// campaign 3 feilds start
				$campaign_owner = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
				$campaign_location = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
				// campaign 3 feilds end
				$checkEmail = $this->admin_model->checkEmailExists($email,$phone);
				//print_r($checkEmail);
				if(!empty($checkEmail)){
				// check mail id and phone exists or not

					$notInsertMails[]=$email;
					$error[]="error";
				} else{
				// if mail and phone number is not there it will be inserted
				$data = array(
					'lead_title'		=>	$title,
					'first_name'		=>	$fname,
					'last_name'			=>	$lname,
					'address_line1'		=>	$address1,
					'address_line2'		=>	$address2,
					'city_id'			=>	!empty($city)?$city:'',
					'state_id'			=>	$state,
					'country_id'		=>	$country,
					'pincode'			=>	$pincode,
					'email'				=>	$email,
					'phone'				=>	$phone,
					'dob'           	=>  date('Y-m-d', $dob), 
		            'gender'          	=>  $gender,
		            'company'           =>  $company,  
		            'aadhar_id'         =>  $aadharnum, 
		            'marital_status'  	=>  $m_status, 
		            'spouse_name'     	=>  !empty($spousename)?$spousename:'', 
		            'anniversary'     	=>  date('Y-m-d', $anniversary),
		            'modified_by'     	=> $today, 
		            'status'          	=> $today,  
		            'created_at'      	=> $today,
		            'created_by'      	=> $adminId,
		            'updated_at'      	=> $today
		            // 'lead_img'        	=> $imagename
				);
				$insert_id = $this->db->insert('lead',$data);
				$lead_id = $this->db->insert_id();
				$data1 =array(
		            'lead_id'         => $lead_id,
		            'garage_name'     => $gname ? $gname:'',
		            'campaign_name'   => !empty($campaign_type)?$campaign_type:'',
		            'campaign_owner'  => !empty($campaign_owner)?$campaign_owner:'',
		            'campaign_location'   => !empty($campaign_location)?$campaign_location:'',
		            'lead_type'   	  => $leadtype,
		            'lead_status'     => $leadstatus,
		            'lead_cat'        => $leadcategory,
		            'policy_type'     => $policytype,
		            'reference_id'    => 0,
		            'created_by'      => $adminId,
		            'created_at' =>$today,
		            'renewal_date'    => date('Y-m-d', $renewal), 
		            'appointment_date'=> date('Y-m-d', $appointment)
                ); 
				$this->db->insert('assign_lead',$data1);
				//$success[] ="success";
				$this->assigned_leads_data($pincode);
				}
				}
			}
			// print_r($notInsertMails);
			// print_r($success);
			 die(json_encode(array('existMails'=>!empty($notInsertMails)?implode(' , ',$notInsertMails):'')));
		}	
	}


	//Get all leads data from admin
	public function index(){
		$data['page_title'] = "Assign Lead Details";

        $data['lead_status']    = $this->lead_model->lead_status();
        $data['lead_category']  = $this->lead_model->lead_category();
        //branch head filter for branch manager 
       $data['distance'] = $this->location_model->lead_dist();
         $kms = $data['distance']['kilometer'];
         $lmt = $data['distance']['item_number'];
		$data['branchManagers'] = $this->location_model->branchm_list();
		//$data['fso_list'] = $this->location_model->fso_list();
        //echo "<pre>";print_r($data['branchManagers']);exit();
        //$this->template->load('frontTemplate', 'assignLead', $data); 
        //
		$this->load->view('admin/adminAssignLead/filterleadsData',$data);
	}
	
	public function getFsoList(){
		$data['page_title'] = "Assign Lead Details";
		$data['distance'] = $this->location_model->lead_dist();
         $kms = $data['distance']['kilometer'];
         $lmt = $data['distance']['item_number'];
		 $request = $this->input->post();
		 //echo "<pre>";print_r($request);exit();
		 $user_id=$request['user_id'];
		 $role_id=2;
        $data['fso_list'] = $this->location_model->assign_fso_list($kms,$user_id,$role_id);
		echo json_encode($data['fso_list']);
	}
	//Get all leads filter in Admin

    public function filterleads(){
        $draw    = intval($this->input->get("draw"));
        $start   = intval($this->input->get("start"));
        $length  = intval($this->input->get("length"));
        //branch manager user_id
        $b_m_user_id = $this->input->post('b_m_user_id');
        if(!empty($b_m_user_id)){
            $this->lead_model->branch_manager_lead($b_m_user_id);
        }
        // lead status filter
        $lead_status = $this->input->post('lead_status');
        if(!empty($lead_status)){
            $this->lead_model->lead_status_name($lead_status);
        }
        // lead category filter
        $lead_category = $this->input->post('lead_category');
        if(!empty($lead_category)){
            $this->lead_model->lead_category_name($lead_category);
        }
        
        $b_m_fso_id = $this->input->post('b_m_fso_id');
        
        if(!empty($b_m_fso_id)){
        	//echo "<pre>";print_r($lead_id);exit();
            $this->lead_model->branch_manger_fso_filter($b_m_fso_id);
        }

		$all_leads_filter= $this->lead_model->get_all_leads_filter_admin();
		$all_fso_filter="";

		if(!empty($b_m_user_id)){
		 $data['distance'] = $this->location_model->lead_dist();
         $kms = $data['distance']['kilometer'];
		 $all_fso_filter = $this->location_model->branch_manager_fso_list($kms,$b_m_user_id,2);
		//$all_fso_filter= $this->lead_model->get_all_leads_filter();
		}
		//echo "<pre>";print_r($all_leads_filter);exit();
		$no = $this->input->post('start');
        $data = [];
        if(!empty($all_leads_filter)){
                    foreach ($all_leads_filter as $leads_filter) {
            $data[] = array(
                // $leads_filter['id'],
                ++$no,
                ucwords($leads_filter['first_name']),
                ucwords($leads_filter['status_name']),
                $leads_filter['category_name'],
                $leads_filter['lead_title'],
                $leads_filter['appointment_date'],
                $leads_filter['renewal_date'],
                '<a style="cursor:pointer;"  class="btn but-blue" href="lead_list/lead_view/' . $leads_filter['id'] . '"><i class="fa fa-eye" aria-hidden="true"></i>
                </a>'
                // $action_button
            );
        }
  
        }
        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($all_leads_filter),
            "recordsFiltered" => count($all_leads_filter),
            "data" => $data
        );
		//echo json_encode($output);
		echo json_encode(array('filter_data'=>$output,'fso_lead'=>$all_fso_filter));
        exit();
       // $this->template->load('frontTemplate', 'lead/filterleads');
	}
	
	
	//Showing all lead details view in Admin
    public function lead_view(){
    	$data['page_title'] = "Assign Lead Details View";
        $id = $this->uri->segment(3);
        // $data['user_profile']=
        $data['assign_lead_profile'] = $this->lead_model->assign_lead_profile($id);
        //echo "<pre>";print_r($data['assign_lead_profile']);exit();		
		$this->load->view('admin/adminAssignLead/fso_profileview',$data);

	}
	

	public function assigned_leads_data($pincode){
		// here to get that row assign_lead
        $data['get_all_assign_lead_data'] = $this->lead_model->get_all_assign_lead_data();
        // to find a nearest location in settings table
        $data['distance'] = $this->location_model->lead_dist();
        $kms = $data['distance']['kilometer'];
        $role_id=3;
        // to find nearest location based on lead
        $branch_fso = $this->location_model->branch_fso_list_excel($kms,$role_id,$pincode);
        if(!empty($branch_fso)){
        if(!empty($data['get_all_assign_lead_data'])){
            foreach ($data['get_all_assign_lead_data'] as $all_leads_data) {
       
                 foreach ($branch_fso as $i=>$fso) {
                     if(!empty($fso)){
                     	// to get the number of leads
                        $check_assign= $this->lead_model->assign_lead_data($fso['user_id']);
                        // to get the count
                            if(count($check_assign)<6){
                            $this->db->set('reference_id', $fso['user_id']);
                            $this->db->set('assigned_status', 1);
                            $this->db->where('lead_id',$all_leads_data['lead_id']);
                            $this->db->update('assign_lead');
                            }
                    }
            
                }
            }
        }
        }else{
        // unmatched data update assigned_status is 2
    	 foreach($data['get_all_assign_lead_data'] as $unmatched_data){
    	 	 $this->db->set('assigned_status', 2);
             $this->db->where('lead_id',$unmatched_data['lead_id']);
             $this->db->update('assign_lead');
    	 }
        }
    }
}