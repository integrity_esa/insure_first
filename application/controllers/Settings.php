<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends Admin_Controller {
	
    public function __construct(){
		
    	parent::__construct(); 
        $this->load->model(array('admin_model','location_model','email_model'));
    }
	public function index() {


        $post = $this->input->post();

       
        if(!empty($post)){
            $files  = $_FILES;      
            $res          = $this->admin_model->new_settings($post,$files);
           
            $status=!empty($res)?'1':'0';
            die(json_encode(array('status'=>$status)));            
        }

        $data=array();
        $data['page_title']     = "Settings";
        $data['page_title2']    = "Settings";
        $data['settings']=$this->admin_model->getSettings();
        $this->load->view('admin/settings/add',$data);
    }

    }
    ?>