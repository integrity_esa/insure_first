<?php 

/**
 * 
 */
class File_handling
{
    protected $_ci;

    function __construct()
    {
        $this->_ci =& get_instance();
    }

    function file_upload($path, $name, $file, $allowed_extensions, $key = '') {
        $config['upload_path'] = $path;
        if (!is_numeric($key)) {
            $_FILES['images']['name']       = $file_name = $file['name'];
            $_FILES['images']['type']       = $file['type'];
            $_FILES['images']['tmp_name']   = $file['tmp_name'];
            $_FILES['images']['error']      = $file['error'];
            $_FILES['images']['size']       = $file['size'];
        } else{
            $file_name=$name;
        }

        $config['allowed_types'] = $allowed_extensions;

        $this->_ci->load->library('upload', $config);
        $this->_ci->upload->initialize($config);

        if ($this->_ci->upload->do_upload('images')) {
            $file_data = $this->_ci->upload->data();
            $filename = $file_data['file_name'];
            return $filename;
        } else{
            $filename = '';
            return $filename;
        }
    }
}