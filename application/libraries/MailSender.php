<?php
class MailSender
{
    public function __construct()
    {
        
    }

    public function load()
    {
        require_once(APPPATH."third_party/phpmailer/PHPMailerAutoload.php");
        $mail = new PHPMailer;
		return $mail;
    }
}