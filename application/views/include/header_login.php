<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Insure First:: <?php echo $page_title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> 
  <!--fontawesome-->
  <?php echo theme_css('font-awesome.min.css', true); ?>
  <!-- Bootstrap CSS CDN -->
  <?php echo theme_css('bootstrap.min.css', true); ?>
  <!--custom-style-->
  <?php echo theme_css('style.css', true); ?>
  <!--responsive-style-->
  <?php echo theme_css('responsive.css', true); ?>
<body>
