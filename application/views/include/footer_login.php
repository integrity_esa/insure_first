</div>
<!-- jQuery CDN -->
<?php echo theme_js('jquery.min.js', true); ?>
<!-- Bootstrap Js CDN -->
<?php echo theme_js('bootstrap.min.js', true); ?>
<!-- custom js -->
<script>var base_url = '<?php echo base_url() ?>';</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<?php echo custom_js('login.js', true); ?>
<?php echo custom_js('custom_script.js', true); ?>
<script >
$(document).ready(function () {
 $('#sidebarCollapse').on('click', function () {
     $('#sidebar').toggleClass('active');
 });

//number validation only
$(".number_only").keypress(function (e) {
var number = $.trim($('.number_only').val());
//if the letter is not digit then display error and don't type anything
if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
return false;
}
});


//auto move tab otp
$(".inputs_tab").keyup(function () {
    if (this.value.length == this.maxLength) {
    	//alert("hh");
      $(this).next('.inputs_tab').focus();
    }
});
});


</script>
</body>
</html>
