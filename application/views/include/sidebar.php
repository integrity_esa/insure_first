<?php
    /**highlight active menu**/
    $ctrl_name      = $this->uri->segment(1);
    $method_name    = !empty($this->uri->segment(2))?$this->uri->segment(2):'';
    /**username**/
    $userData       = $this->session->userdata('user');
    $username       = $userData['first_name'] .' '. $userData['last_name'];

?>
<!-- Sidebar Holder -->	
<nav id="sidebar">		
    <ul class="list-unstyled components">
        <li class="profile-li" >
            <a href="<?php echo base_url() ?>userprofile"><img href="<?php echo base_url() ?>userprofile" src="<?php echo base_url() ?>assets/images/profile-img.png" class="img-fluid" alt="profile"/><?php echo strtoupper($username); ?></a>
        </li>    
        <li class="dashboard-mnu <?php echo $ctrl_name == 'dashboard' && empty($method_name)?'active':''; ?>">
        	<a href="<?php echo base_url() ?>dashboard"><img src="<?php echo base_url() ?>assets/images/home.png" class="img-fluid white" alt="home"/><img src="<?php echo base_url() ?>assets/images/home-y.png" class="img-fluid yellow" alt="home"/>HOME</a>
        </li>
        <li class="leads-mnu <?php echo $ctrl_name=="lead"?'active':''; ?>">
        	<a href="<?php echo base_url() ?>lead/leads"><img src="<?php echo base_url() ?>assets/images/leads.png" class="img-fluid white" alt="leads"/><img src="<?php echo base_url() ?>assets/images/leads-y.png" class="img-fluid yellow" alt="leads"/>LEADS</a>
        </li>
        <li>
        	<a href="<?php echo base_url('quotation'); ?>">
                <img src="<?php echo base_url() ?>assets/images/quotation.png" class="img-fluid white" alt="quotation"/>QUOTATION
            </a>
        </li>
        <li class="leads-mnu <?php echo $ctrl_name=="policy_sold"?'active':''; ?>">
        	<a href="<?php echo base_url() ?>policy_sold"><img src="<?php echo base_url() ?>assets/images/policy.png" class="img-fluid white" alt="policy"/>POLICY SOLD</a>
        </li>
        <li>
        	<a href="#"><img src="<?php echo base_url() ?>assets/images/performance.png" class="img-fluid white" alt="performance"/>PERFORMANCE & CONTEST</a>
        </li>

         <?php if($userData['role_id']==1){ ?>
        <li>
        	<a href="<?php echo base_url() ?>UserAssign_lead/assignLead"><img src="<?php echo base_url() ?>assets/images/performance.png" class="img-fluid white" alt="assignLeads"/>Assign Leads</a>
        </li>
        <?php } ?>
        <?php if($userData['role_id']==2){ ?>
        <li>
        	<a href="<?php echo base_url() ?>UserAssign_fsolead/assignFsoLead"><img src="<?php echo base_url() ?>assets/images/performance.png" class="img-fluid white" alt="assignLeads"/>Assign FSO Leads</a>
        </li>	
        <?php } ?>
        <li>
            <a href="<?php echo base_url() ?>lead"><img src="<?php echo base_url() ?>assets/images/performance.png" class="img-fluid white" alt="assignLeads"/>View Status</a>
        </li>   
    </ul>	
</nav>
<!-- Page Content Holder -->
