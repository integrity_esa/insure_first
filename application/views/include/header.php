<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>::.Insure First.::</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">  
   <!--fontawesome-->
  <?php echo theme_css('font-awesome.min.css', true); ?>

  <!-- Bootstrap CSS CDN -->
  <?php echo theme_css('bootstrap.min.css', true); ?>

  <!--custom-style-->
  <?php echo theme_css('style.css', true); ?>

  <!--responsive-style-->
  <?php echo theme_css('responsive.css', true); ?>

  <!-- datepicker -->
  <?php echo theme_css('jquery-ui.css', true); ?>

  <!-- datetimepicker -->
  <?php echo theme_css('bootstrap-datetimepicker.min.css', true); ?>

  <!-- Alertify - CSS -->
  <?php echo theme_css('alertify.min.css', true); ?>
  <?php echo theme_css('default.min.css', true); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<?php echo admin_css('global/plugins/select2/select2.css', true); ?>
<?php echo admin_css('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', true); ?>
<!-- END PAGE LEVEL STYLES -->
<script type="text/javascript">
  var siteUrl='<?php echo base_url(); ?>';
</script>

<body>
<div class="header">
	<div class="row">
		<div class="col-md-3 text-center">
			<a href="#"><img src="<?php echo base_url() ?>assets/images/logo.png" class="img-fluid" /></a>
		</div>
		<div class="col-md-9 top-icon">
			<ul class="list-inline">
				<li class="list-inline-item"><a href="#" class="search"><img src="<?php echo base_url(); ?>assets/images/search.png" class="img-fluid" alt="search" /></a></li>
				<li class="list-inline-item"><a href="#"><img src="<?php echo base_url(); ?>assets/images/speaker-icon.png" class="img-fluid" alt="speaker-icon" /></a></li>
				<li class="list-inline-item"><a href="#"><img src="<?php echo base_url(); ?>assets/images/notification-icon.png" class="img-fluid" alt="notification-icon" /><span class="badge badge-success">7</span></a></li>
				<li class="list-inline-item"><a href="<?php echo base_url('login/logout'); ?>" class="log-out">Log out<img src="<?php echo base_url(); ?>assets/images/logout-icon.png" class="img-fluid" alt="logout-icon" /></a></li>

			</ul>
			
		</div>
	</div>
</div>
<div class="wrapper">