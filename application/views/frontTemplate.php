<?php 
/*** Header ***/
$this->load->view('include/header');
/*** View ***/
?>

<?php 
/*** Sidebar ***/
$this->load->view('include/sidebar');
/*** View ***/
?>
<!-- Page Content Holder -->
<?=$contents;?>

<?php
/*** Footer ***/
$this->load->view('include/footer'); 
?>