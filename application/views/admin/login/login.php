<?php include_header_login(); ?>

<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" method="post">
        <h3 class="form-title">Sign In</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span id="alert_danger_message"></span>
        </div>
        <div class="alert alert-success display-hide">
            <button class="close" data-close="alert"></button>
            <span id="alert_success_message"></span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Email Id</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email id" name="email" id="email" />
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password" />
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase">Login</button>
            <label class="rememberme check">
            <input type="hidden" value="submitted" name="submitted"/>
            <!-- <input type="checkbox" name="remember" value="1"/>Remember  -->
            </label>
            <a href="<?php echo base_url('admin/forgot_password'); ?>" id="back-btn" class="btn btn-default forget-password">Forgot Password?</a>
        </div>
    </form>
    <!-- END LOGIN FORM -->
</div>

<?php include_footer_login(); ?>
