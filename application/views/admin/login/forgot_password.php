<?php include_header_login(); ?>

<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form">
        <h3>Forget Password ?</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span id="alert_dng_message"></span>
        </div>
        <div class="alert alert-success display-hide">
            <button class="close" data-close="alert"></button>
            <span id="alert_succ_message"></span>
        </div>
        <p>
             Enter your e-mail address below to reset your password.
        </p>
        <div class="form-group">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
        </div>
        <div class="form-actions">
            <a href="<?php echo base_url('admin'); ?>" id="back-btn" class="btn btn-default">Back</a>
            <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>

<?php include_footer_login(); ?>
