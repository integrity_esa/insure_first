<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Insure First | <?php echo $page_title;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

<?php echo admin_css('global/plugins/font-awesome/css/font-awesome.min.css', true); ?>
<?php echo admin_css('global/plugins/simple-line-icons/simple-line-icons.min.css', true); ?>
<?php echo admin_css('global/plugins/bootstrap/css/bootstrap.min.css', true); ?>
<?php echo admin_css('global/plugins/uniform/css/uniform.default.css', true); ?>
<?php echo admin_css('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css', true); ?>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<?php echo admin_css('global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', true); ?>
<?php echo admin_css('global/plugins/fullcalendar/fullcalendar.min.css', true); ?>
<?php echo admin_css('global/plugins/jqvmap/jqvmap/jqvmap.css', true); ?>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<?php echo admin_css('css/tasks.css', true); ?>
<!-- END PAGE STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<?php echo admin_css('global/plugins/select2/select2.css', true); ?>
<?php echo admin_css('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', true); ?>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<?php echo admin_css('global/css/components-rounded.css', true); ?>
<?php echo admin_css('global/css/plugins.css', true); ?>

<?php echo admin_css('global/css/components-md.css', true); ?>
<?php echo admin_css('global/css/plugins-md.css', true); ?>

<?php echo admin_css('css/layout.css', true); ?>
<?php echo admin_css('css/light.css', true); ?>
<?php echo admin_css('css/custom.css', true); ?>


  <!-- Alertify - CSS -->
  <?php echo admin_css('css/alertify.min.css', true); ?>
  <?php echo admin_css('css/default.min.css', true); ?>

<!-- datetime picker css (24-12-2018)-->
<?php echo admin_css('css/bootstrap-datetimepicker.min.css', true); ?>

<script type="text/javascript">
  var siteUrl='<?php echo base_url('admin'); ?>';
  var baseUrl='<?php echo base_url(); ?>';
</script>

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<?php $admin = $this->session->userdata('admin'); ?>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo base_url('admin/dashboard'); ?>">
                <img src="<?php echo base_url().'admin_assets/image/logo.png'; ?>" alt="logo" class="logo-default" style="width: 185px;"/>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide">
                    </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username username-hide-on-mobile">
                        <?php echo isset($admin['admin_name'])?$admin['admin_name']:'';?> </span>
                        <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                        <img alt="" class="img-circle" src="<?php echo base_url().'admin_assets/image/avatar9.jpg'; ?>"/>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="extra_profile.html">
                                <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/logout'; ?>">
                                <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>