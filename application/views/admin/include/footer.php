<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <?php echo date('Y'); ?> © Insure First. Admin Dashboard.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->

<?php echo admin_js('global/plugins/jquery.min.js', true); ?>
<?php echo admin_js('global/plugins/jquery-migrate.min.js', true); ?>

<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<?php echo admin_js('global/plugins/jquery-ui/jquery-ui.min.js', true); ?>
<?php echo admin_js('global/plugins/bootstrap/js/bootstrap.min.js', true); ?>
<?php echo admin_js('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js', true); ?>
<?php echo admin_js('global/plugins/jquery.blockui.min.js', true); ?>
<?php echo admin_js('global/plugins/jquery.cokie.min.js', true); ?>
<?php echo admin_js('global/plugins/uniform/jquery.uniform.min.js', true); ?>
<?php echo admin_js('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js', true); ?>
<?php echo admin_js('global/plugins/jqvmap/jqvmap/jquery.vmap.js', true); ?>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php echo admin_js('global/scripts/metronic.js', true); ?>
<?php echo admin_js('js/layout.js', true); ?>
<?php echo admin_js('js/demo.js', true); ?>
<?php echo admin_js('scripts/tasks.js', true); ?>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php echo admin_js('global/plugins/select2/select2.min.js', true); ?>
<?php echo admin_js('global/plugins/datatables/media/js/jquery.dataTables.min.js', true); ?>
<?php echo admin_js('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js', true); ?>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php echo admin_js('scripts/table-managed.js', true); ?>
<!-- END PAGE LEVEL SCRIPTS -->
<!--validate(radhakrishnan 24-12-2018)-->
<?php echo admin_js('js/jquery.validate.min.js', true); ?>
<!--Datepicker-->
<?php echo admin_js('js/jquery-ui.js', true); ?>

<!--alertify popup-->
<?php echo admin_js('js/alertify.min.js', true); ?>

<!--Datetimepicker-->
<?php echo admin_js('js/moment.min.js', true); ?>
<?php echo admin_js('js/bootstrap-datetimepicker.min.js', true); ?>

<?php echo admin_js('js/lead_form.js', true); ?>
<?php echo admin_js('js/custom_js/admin_custom.js', true); ?>

<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        Demo.init(); // init demo features 
        TableManaged.init();
        Tasks.initDashboardWidget(); // init tash dashboard widget  
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>