<div class="copyright">
     <?php echo date('Y'); ?> © Insure First. Admin Dashboard.
</div>

<?php echo admin_js('global/plugins/jquery.min.js', true); ?>
<?php echo admin_js('global/plugins/jquery-migrate.min.js', true); ?>
<?php echo admin_js('global/plugins/bootstrap/js/bootstrap.min.js', true); ?>
<?php echo admin_js('global/plugins/jquery.blockui.min.js', true); ?>
<?php echo admin_js('global/plugins/uniform/jquery.uniform.min.js', true); ?>
<?php echo admin_js('global/plugins/jquery.cokie.min.js', true); ?>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php echo admin_js('global/plugins/jquery-validation/js/jquery.validate.min.js', true); ?>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php echo admin_js('global/scripts/metronic.js', true); ?>
<?php echo admin_js('js/layout.js', true); ?>
<?php echo admin_js('js/demo.js', true); ?>
<?php echo admin_js('scripts/login.js', true); ?>
<?php echo admin_js('scripts/admin_login.js', true); ?>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Login.init();
Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>