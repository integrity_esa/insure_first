<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Insure First | <?php echo $page_title;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<?php echo admin_css('global/plugins/font-awesome/css/font-awesome.min.css', true); ?>
<?php echo admin_css('global/plugins/simple-line-icons/simple-line-icons.min.css', true); ?>
<?php echo admin_css('global/plugins/bootstrap/css/bootstrap.min.css', true); ?>
<?php echo admin_css('global/plugins/uniform/css/uniform.default.css', true); ?>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<?php echo admin_css('css/login.css', true); ?>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<?php echo admin_css('global/css/components-rounded.css', true); ?>
<?php echo admin_css('global/css/plugins.css', true); ?>
<?php echo admin_css('css/layout.css', true); ?>
<?php echo admin_css('css/default.css', true); ?>
<?php echo admin_css('css/custom.css', true); ?>
<!-- END THEME STYLES -->
<script type="text/javascript">
    var baseurl = "<?php print base_url(); ?>";
</script>
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="<?php echo base_url('admin'); ?>">
        <?php echo admin_img('logo.png', true); ?>
    </a>
</div>
<!-- END LOGO -->