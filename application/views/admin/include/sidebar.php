<?php
    /**highlight active menu**/
    $ctrl_name = $this->uri->segment(1);
    $method_name = !empty($this->uri->segment(2))?$this->uri->segment(2):'';
    //echo"";print_r($method_name);die;
?>

    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="start-mnu <?php echo $ctrl_name == 'admin' && empty($method_name)?'active':''; ?>">
                    <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/users'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Users</span>
                    </a>
                </li>
                <li>
                    <a >
                    <i class="icon-bar-chart"></i>
                    <span class="title">Lead</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url('admin/load_lead'); ?>">
                            <i class="icon-home"></i>
                            Add Lead</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/lead_type'); ?>">
                            <i class="icon-home"></i>
                            Lead Type</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/Lead_Category'); ?>">
                            <i class="icon-basket"></i>
                            Lead Category</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/lead_status'); ?>">
                            <i class="icon-basket"></i>
                            Lead Status</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/policy_type'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Policy Type</span>
                    <span class="selected"></span>
                    </a>                        
                </li>
                <li>
                    <a href="<?php echo base_url('admin/role'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Roles Management</span>
                    <span class="selected"></span>
                    </a>                        
                </li>
                <li>
                    <a href="<?php echo base_url('admin/branch'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Branch Management</span>
                    <span class="selected"></span>
                    </a>                    
                </li>
                <li>
                    <a href="<?php echo base_url('settings'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Settings</span>
                    <span class="selected"></span>
                    </a>                    
                </li>
                 <li>
                    <a href="<?php echo base_url('admin/campaign'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Campaign</span>
                    <span class="selected"></span>
                    </a>                        
                </li>
                <li>
                    <a href="<?php echo base_url('admin/assign_branch_view'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Assign Branch</span>
                    <span class="selected"></span>
                    </a>                        
                </li>
                 <li>
                    <a href="<?php echo base_url('admin/lead_manage'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Lead Management</span>
                    <span class="selected"></span>
                    </a>                    
                </li>
                <li>
                    <a href="<?php echo base_url('admin/assign_branch_fso_view'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">Assign Branch Fso</span>
                    <span class="selected"></span>
                    </a>                        
                </li>
                <li>
                    <a href="<?php echo base_url('lead_list'); ?>">
                    <i class="icon-user"></i>
                    <span class="title">View Status</span>
                    <span class="selected"></span>
                    </a>                        
                </li>
                <!--<li>
                    <a href="javascript:;">
                    <i class="icon-basket"></i>
                    <span class="title">eCommerce</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="ecommerce_index.html">
                            <i class="icon-home"></i>
                            Dashboard</a>
                        </li>
                        <li>
                            <a href="ecommerce_orders.html">
                            <i class="icon-basket"></i>
                            Orders</a>
                        </li>
                        <li>
                            <a href="ecommerce_orders_view.html">
                            <i class="icon-tag"></i>
                            Order View</a>
                        </li>
                        <li>
                            <a href="ecommerce_products.html">
                            <i class="icon-handbag"></i>
                            Products</a>
                        </li>
                        <li>
                            <a href="ecommerce_products_edit.html">
                            <i class="icon-pencil"></i>
                            Product Edit</a>
                        </li>
                    </ul>
                </li>-->

            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->