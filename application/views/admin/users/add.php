<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<style type="text/css">
	.error {
		color: red;
	}
</style>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper urldata" data-user = "0" data-url="<?php echo base_url(); ?>admin/">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?php echo $page_title;?></h1>
				</div>
			</div>
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?php echo $page_title2;?></a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?php echo $page_title;?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i><?php echo $page_title;?>
							</div>
						</div>
						<div class="portlet-body">
							<form method="post" id="adduser" enctype="multipart/form-data" autocomplete="off">
								<div class="inner-content clearfix">
									<div class="row">
										<div class="col-md-3"></div>
										<div class="col-md-6">
											<div class="profile-details">
												<div class="profile-photo">
													<a href="#">
														<img style="width: 150px; height: 150px; border-radius: 50%;"  id="user_image" src="" src="#" height="100" width="100" />
														<img style="position: relative; top: 60px; right: 45px; cursor: pointer;" onclick="$('#user_images').click()" src="<?php echo base_url() ?>assets/images/cam.png" class="img-fluid" alt="cam-img" />
														<input type="file" onchange="readURL(this);" name="user_image" id="user_images" style="display: none;"  accept="image/*" value="">

													</a>						
												</div>
											</div>
										</div>
										<div class="col-md-3"></div>
									</div>
										<div class="row">
											<div class="col-lg-1"></div>
											<!-- <span class="mandatory" style="color:red;"></span> -->
											<div class="col-lg-5">
												<div class="form-group">
													<label>Title</label>
													<select name="title" id="title" class="form-control ">
														<option value="">-- Select --</option>
														<option value="Mr." >Mr.</option>
														<option value="Ms." >Ms.</option>
														<option value="Mrs." >Mrs.</option>
													</select>
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>First Name *</label>
													<input type="text"  name="fname" id="fname" class="form-control" value="" placeholder="">
												</div>
											</div>
											<div class="col-lg-1"></div>
										</div>
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Last Name</label>
													<input type="text" value="" name="lname" id="lname" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Role</label>
														<select name="role" id="role" class="form-control custom-select">
													<option value="">-- Select Role --</option>
													<?php
													foreach ($roles as $role) {
		                                        	
		                                            echo '<option value="' . $role['role_id'] . '" '.$selected.'>' . $role['role_name'] . '</option>';
			                                        }
													?>
												</select>
												</div>
											</div>
											
											<div class="col-lg-1"></div>
										</div>										
						
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Phone Number</label>
											<input type="text" value="" name="phone" id="phone" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control" placeholder="" onfocusout="checkuserPhone()">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>E-Mail</label>
											<input type="text" name="email" value="" id="email" class="form-control" placeholder="" onfocusout="checkuserEmail()">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-1"></div>
								</div>
								

							<div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
									<div class="form-group">
										<label>Password</label>
										<input type="password" name="password" class="password form-control" id="password" value=""/>
										<!--<span class="generate_password">Generate Here</span>-->
										
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Confirm Password</label>
										<input type="password" name="password_confirm" class="password_confirm form-control" id="password_confirm" value=""/>
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<!-- <div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
								<div class="form-group">
										<label>Access Privilege</label>
										<input type="text" name="access" class="access form-control" id="access" value=""/>
									</div>
									
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>ISP (Yes| No)</label><br>
										<input type="radio" name="isp" class="isp form-control" id="isp" value="0" checked/><span >NO</span>
										<input type="radio" name="isp" class="isp form-control" id="isp" value="1"/>
										<span >YES</span><br>
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div> -->
								<div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
									<div class="form-group">
										<label>Location<span>(Zipcode)</span></label>
										<!--<input type="text" name="location" class="location form-control" id="location" value=""/>-->
										<!--<span class="generate_password">Generate Here</span>-->
										<input type="text" maxlength="06" onkeypress="return isNumberKey(event)" value="" class="form-control location" name="location" id="location" placeholder="eg:521101">
										
									</div>
								</div>
								<div class="col-lg-5">
									<!-- <div class="form-group">
										<label>Report To</label>
										<input type="text" name="report_to" class="form-control report_to" id="report_to" value=""/>
										
									</div> -->
								</div>
								<div class="col-lg-1"></div>
							</div>
					
							<div class="row">
								<div class="col-lg-4 mx-auto">
									<button class="btn btn-info photo-btn purchase-but add_user">SAVE</button>
									<button class="btn btn-info photo-btn purchase-but">CANCEL</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>

<?php include_admin_footer(); ?>
