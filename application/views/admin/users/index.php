<?php include_admin_header(); ?>
<style>
	.action_class:hover{
		cursor: pointer;
	}


</style>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?php echo $page_title;?> </h1>
					
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?php echo $page_title;?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i><?php echo $page_title;?>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<button id="sample_editable_1_new" data-url="<?php echo base_url().'admin/add_user';?>" class="btn green add_user_link">
											Add New <i class="fa fa-plus"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="user_table">
							<thead>
							<tr>
								<th class="table-checkbox">
									<input type="checkbox" class="group-checkable" data-set="#user_table .checkboxes"/>
								</th>
								<th>
									 Name
								</th>
								<th>
									 Phone
								</th>									
								<th>
									 Email
								</th>
								<th>
									 Role
								</th>
								
								<th>
									 Status
								</th>
								<th>
									 Action
								</th>
							</tr>
							</thead>
							<tbody>

							<?php 
									$i=1;
									if(!empty($users)){
										foreach($users as $user){ 
											$status = '';
											if($user['status']==1){
												$status = 'checked';
											}else{
												$status = '';
											}
										echo 	"<tr class='odd gradeX'>
												<td>".$i."</td>
												<td>".$user['first_name'].' '.$user['last_name']."</td>
												<td>".$user['phone']."</td>
												<td>".$user['email']."</td>
												<td>".ucwords($user['role_name'])."</td>
												<td><input type='checkbox' class='user_status' name='user_status' data-id='".$user['user_id']."' ".$status."/></td>
												<td>
												<span data-id='".$user['user_id']."' class='fa fa-edit user_edit action_class  btn btn-info'></span>
												<span data-id='".$user['user_id']."' class='fa fa-trash user_delete action_class  btn btn-info'></span>
												</td>
												</tr>";
											$i++;
										 } 
									 }
									 ?>

							
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include_admin_footer(); ?>
<script type="text/javascript">
	
	$(document).ready(function(){
		    $("#user_table").dataTable();

	});
</script>