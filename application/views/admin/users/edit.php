<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<style type="text/css">
	.error {
		color: red;
	}
</style>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper urldata" data-user = "<?php echo $_GET['user_id'];?>" data-url="<?php echo base_url(); ?>admin/">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?php echo $page_title;?></h1>
				</div>
			</div>
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				
				<li>
					<a href="#"><?php echo $page_title2;?></a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?php echo $page_title;?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i><?php echo $page_title;?>
							</div>
						</div>
						<div class="portlet-body">
						<?php 
						if(!empty($values)){
						    ?>

							<form method="post" id="adduser" enctype="multipart/form-data" autocomplete="off" >
							<input type="hidden" name="update_id" value="<?php echo !empty($values)?$values['user_id'] : '' ; ?>">
								<div class="inner-content clearfix">
									<div class="row">
										<div class="col-md-3"></div>
										<div class="col-md-6">
											<div class="profile-details">
												<div class="profile-photo">
													<a href="#">
														<!-- image start -->
														<img style="width: 150px; height: 150px; border-radius: 50%;"  id="user_image" src="<?php echo base_url() .'assets/images/user_images/'.$values['profile_picture'];?>" onerror="this.onerror=null; this.src='<?php echo base_url('assets/images/user.png') ?>';" src="#" height="100" width="100" />
														<img style="position: relative; top: 60px; right: 45px; cursor: pointer;" onclick="$('#user_images').click()" src="<?php echo base_url() ?>assets/images/cam.png" class="img-fluid" alt="cam-img" />
														<input type="file" onchange="readURL(this);" name="user_image" id="user_images" value="<?php echo !empty($values)?$values['profile_picture'] : '' ; ?>" style="display: none;"  accept="image/*">
														<input type="hidden" name="old_user_image" value="<?php echo !empty($values)?$values['profile_picture'] : '' ; ?>">
														<!-- image end -->
													</a>						
												</div>
											</div>
										</div>
										<div class="col-md-3"></div>
									</div>
										<div class="row">
											<div class="col-lg-1"></div>
											<!-- <span class="mandatory" style="color:red;"></span> -->
											<div class="col-lg-5">
												<div class="form-group">
													<label>Title</label>
													<select name="title" id="title" class="form-control ">
														<option value="" <?php echo $values['user_title'] ==""?'selected':''; ?>>-- Select --</option>
														<option value="Mr." <?php echo $values['user_title'] =="Mr."?'selected':''; ?>>Mr.</option>
														<option value="Ms." <?php echo $values['user_title'] =="Ms."?'selected':''; ?>>Ms.</option>
														<option value="Mrs." <?php echo $values['user_title'] =="Mrs."?'selected':''; ?>>Mrs.</option>
													</select>
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>First Name *</label>
													<input type="text"  name="fname" id="fname" class="form-control" value="<?php echo !empty($values['first_name']) ? $values['first_name'] : ' ' ?>" placeholder="">
												</div>
											</div>
											<div class="col-lg-1"></div>
										</div>
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Last Name</label>
													<input type="text" value="<?php echo !empty($values['last_name']) ? $values['last_name'] : ' ' ?>" name="lname" id="lname" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Role</label>
														<select name="role" id="role" class="form-control custom-select">
													<option value="">-- Select Role --</option>
													<?php
													foreach ($roles as $role) {
		                                        	if($values['role_id']==$role['role_id']){
		                                        		$selected = 'selected';
		                                        	}
		                                        	else{
		                                        		$selected='';
		                                        	}
		                                            echo '<option value="' . $role['role_id'] . '" '.$selected.'>' . $role['role_name'] . '</option>';
			                                        }
													?>
												</select>
												</div>
											</div>
											
											<div class="col-lg-1"></div>
										</div>										
						
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Phone Number</label>
											<input type="text" value="<?php echo !empty($values['phone']) ? $values['phone'] : ' ' ?>" name="phone" id="phone" onkeypress="return isNumberKey(event)" maxlength="10" onfocusout="checkuserPhone()" class="form-control" placeholder="">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>E-Mail</label>
											<input type="text" name="email" value="<?php echo !empty($values['email']) ? $values['email'] : ' ' ?>" onfocusout="checkuserEmail()" id="email" class="form-control" placeholder="">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-1"></div>
								</div>
								

							<div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
									<div class="form-group">
										<label>Password</label>
										<input type="password" name="password" class="password form-control" id="password" value="<?php echo !empty($values['real_password']) ? $values['real_password'] : ' ' ?>" />
										<!--<span class="generate_password">Generate Here</span>-->
										
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Confirm Password</label>
										<input type="password" name="password_confirm" class="password_confirm form-control" id="password_confirm" value="<?php echo !empty($values['real_password']) ? $values['real_password'] : ' ' ?>" />
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div>
						<!-- 	<div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
								<div class="form-group">
										<label>Access Privilege</label>
										<input type="text" name="access" class="access form-control" id="access" value=""/>
									</div>
									
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>ISP (Yes| No)</label><br>
										<input type="radio" name="isp" class="isp form-control" id="isp" value="0" <?php if($values['isp'] ==0){echo 'checked';}?>/><span >NO</span>
										<input type="radio" name="isp" class="isp form-control" id="isp" value="1" <?php if($values['isp'] ==1){echo 'checked';}?>/>
										<span >YES</span><br>
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div> -->
								<div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
									<div class="form-group">
										<label>Location<span>(Zipcode)</span></label>
										<!--<input type="text" name="location" class="location form-control" id="location" value=""/>-->
										<!--<span class="generate_password">Generate Here</span>-->
										<input type="text" maxlength="06" onkeypress="return isNumberKey(event)"  value="<?php echo !empty($values['zip_code']) ? $values['zip_code'] : ' ' ?>" class="form-control location" name="location" id="location" placeholder="eg:521101">
										
									</div>
								</div>
								<div class="col-lg-5">
									<!-- <div class="form-group">
										<label>Report To</label>
										<input type="text" name="report_to" class="form-control report_to" id="report_to" value="<?php echo !empty($values['authority_id']) ? $values['authority_id'] : ' ' ?>" />
										
									</div> -->
								</div>
								<div class="col-lg-1"></div>
							</div>
					
							<div class="row">
								<div class="col-lg-4 mx-auto">
									<button class="btn btn-info photo-btn purchase-but save_update" data-id="<?php echo $values['user_id'];?>">SAVE</button>
									<input type="button" class="btn btn-info photo-btn purchase-but" id="cancel_btn" value="CANCEL">
								</div>
							</div>
						</div>
					</form>
					<?php } ?>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>

<?php include_admin_footer(); ?>
<script type="text/javascript">
	
    // redirect cancel_btn
    $('#cancel_btn').on('click',function(){
    	window.location.href= siteUrl+"/users";
    });
</script>