<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">			
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?php echo $page_title;?> </h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?php echo $page_title;?></a>
					<i class="fa fa-circle"></i>
				</li>
				
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Managed Table
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<!--<button id="sample_editable_1_new" class="btn green">
											Add New <i class="fa fa-plus"></i>
											</button>-->
											<a href="<?php echo base_url().'admin/add_branch';?>" class="add_branch_link"> Add Branch</a>
										</div>
									</div>
								
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="branch_table">
							<thead>
							<tr>
								
										<th>
											 #
										</th>
										<th>
											Branch
										</th>
										<th>Location</th>
							</tr>
							</thead>
							<tbody>
					
								<?php
									if(!empty($branches)) {
										$i=1;
										foreach($branches as $branch){ 

											?>

										 	<tr id="<?php echo $branch['branch_id'];?>" class="odd gradeX">
												<td><?php echo $i;?></td>
												<td><?php echo $branch['branch_name'];?></td>
												<td><?php echo $branch['location'];?></td>
												
											</tr>
									<?php
											$i++;
										 } 
									 } ?>
							</tbody>
							</table>
							<!--<div class="confirm_box">
										ARe you Confirm to delete this role?
										<span class="confirm">Yes</span>
										<span class="cancel">Cancel</span>
									</div>-->
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include_admin_footer(); ?>