<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<style type="text/css">
	.error {
		color: red;
	}
</style>
<?php //echo"<pre>";print_r($editListing);exit; ?>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Branch</h1>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>
							</div>
						</div>
						<div class="portlet-body">
							<form method="post" id="new_branch" data-url="<?php echo base_url().'admin/add_branch';?>"class="new_branch" enctype="multipart/form-data" autocomplete="off" >
								<div class="inner-content clearfix">
									
									<div class="detail-form">
										<div class="row">
											<div class="col-lg-1"></div>
											<!-- <span class="mandatory" style="color:red;"></span> -->
											<div class="col-lg-5">
												<div class="form-group">
													<label>Branch Name</label>
													<input type="text" class="form-control" name="branch_name" id="branch_name" required/>
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Branch Location</label>
													<input type="text" class="form-control" name="location" id="location" required/>
													</div>
												</div>
											</div>
											
											<div class="col-lg-1"></div>
										</div>
										
										
									</div>
								</div>
							
							<div class="row">
								<div class="col-lg-4 mx-auto">
									<button class="btn btn-info photo-btn purchase-but branch_submit" id="branch_submit" data-url = "<?php echo base_url();?>">SAVE</button>
									<button class="btn btn-info photo-btn purchase-but">CANCEL</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>

<?php include_admin_footer(); ?>
