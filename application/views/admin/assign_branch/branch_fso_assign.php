<?php include_admin_header(); ?>
<div class="page-container">
   <?php include_admin_side_bar();?>
   <style type="text/css">.error {color: red;}</style>
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
               <h1><?php
                  echo $page_title;
                  ?></h1>
            </div>
         </div>
         <!-- BEGIN PAGE BREADCRUMB -->
         <ul class="page-breadcrumb breadcrumb">
            <li>
               <a href="<?php
                  echo base_url('admin/dashboard');
                  ?>">Home</a>
               <i class="fa fa-circle"></i>
            </li>
            <li>
               <a href="#"><?php echo $page_title;
                  ?></a>
            </li>
         </ul>
         <!-- END PAGE BREADCRUMB -->
         <div class="row">
            <div class="col-md-12">
               <!-- BEGIN EXAMPLE TABLE PORTLET-->
               <div class="portlet box grey-cascade">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-globe"></i><?php echo $page_title;?>
                     </div>
                  </div>
                  <div class="portlet-body">
                    <form method="post" id="import_form" enctype="multipart/form-data">
                        <div class="fso_list">
                           <label>Branch Head:</label>
                           <select name="select_branchhead" id="select_branchhead" class="select_branchhead" data-url ="<?php echo base_url('admin');?>">
                           <option value="0" >Select Branch Head</option>
                           <?php if(!empty($branchHeads)){
                              foreach($branchHeads  as  $branchHead){
                                echo  '<option value="'.$branchHead['user_id'].'">'.$branchHead['user_title'].' '.ucfirst($branchHead['first_name']).'</option>';
                              }
                              
                              } ?>
                            </select>
                        </div>
                        <div class="lead_list_view"></div>
                        <div class="lead_list_view_fso"></div>

                        <div class="" id="excel_import" hidden="">
                        <p><label>Select Excel File</label>
                        <input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
                        <br />
                        <input type="submit" name="import" value="Import" class="btn btn-info" />
                        </div>
                    </form>
                  </div>
               </div>
               <!-- END EXAMPLE TABLE PORTLET-->
               <!-- END PAGE CONTENT-->
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   include_admin_footer();
   ?>
<script type = "text/javascript" >
    //import excel file
    $('#import_form').on('submit', function(event) {
        var branch_head_user_id = $('#select_branchhead').val();
        var branch_manager_user_id = $('#branchManager').val();
        var branch_manager_fso__user_id = $('#branch_manager_fso').val();
        var form_data = new FormData(this);
        form_data.append('branch_head_user_id', branch_head_user_id);
        form_data.append('branch_manager_user_id', branch_manager_user_id);
        form_data.append('branch_manager_fso__user_id', branch_manager_fso__user_id);
        // console.log(JSON.stringify(new FormData(this)));return false;
        event.preventDefault();
        $.ajax({
            url: "<?php echo base_url()?>admin/assign_branch_lead",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'JSON',
            success: function(data) {
                // alert(data.existMails);
                if (data.existMails != "") {
                    alertify.alert('Error', data.existMails + ' Emails already exists.', function() {
                        location.reload();
                    });
                } else {
                    // alertify.alert('Error', 'Failed to upload.');
                    alertify.alert('success', 'successfully upload.',
                        function() {
                            location.reload();
                        });
                }
            }
        })
    });
// $('#excel_import').addClass('hidden');
$('#select_branchhead').change(function() {
    var user_id = $(this).val();
    //alert(user_id); 
    //return false;
    var url = $(this).data('url');
    var kms = $("#kilometer").val();
    if (user_id != "" || user_id != 0) {
        $.ajax({
            url: url + "/assign_branch",
            data: {
                user_id: user_id,
                kms: kms
            },
            type: "post",
            dataType: "JSON",
            success: function(result) {
                //console.log(JSON.stringify(result));
                //return false;  
                if (result.branchManagers.length > 0) {
                    var user_id = "";
                    var leadHtml = '<label>Branch Manager:</label> <select name="branchManager" id="branchManager" required >';
                    leadHtml += '<option value="" >--Select Branch Manager--</option>';
                    $.each(result.branchManagers, function(index, element) {

                        leadHtml += '<option value="' + element.user_id + '"> ' + element.user_title + ' ' + element.first_name + ' ' + element.last_name + '</option>';
                    });
                    //user id getting
                    //console.log(result.user_id);
                    leadHtml += '</select>';
                    $('.lead_list_view').html(leadHtml);
                    //remove attribute
                    //$('#excel_import').removeAttr('hidden');
                } else {
                    $('.lead_list_view').html('');
                    //add attribute
                   // $('#excel_import').attr('hidden', true);
                }
            }
        });
        return false;
    } else {
        $('.lead_list_view').html('');
        //add attribute
       // $('#excel_import').attr('hidden', true);
    }
});
//fso assign
// $('body').on('change','#branchManager',function(){ 
//   var user_id=$(this).val();
//   var url = "<?php base_url()?>/admin/assign_branch_fso";
//   alert(user_id);
//   // var kms = $("#kilometer").val();
//   alert(url);
// });
// $('#excel_import').addClass('hidden');
$('body').on('change','#branchManager',function() {
    var user_id = $(this).val();
    var url = "<?php echo base_url()?>admin/assign_branch_fso";
    // alert(user_id);
    // alert(url);return false;
    if (user_id != "" || user_id != 0) {
        $.ajax({
            url: url,
            data: {
                user_id: user_id,
            },
            type: "post",
            dataType: "JSON",
            success: function(result) {
                // console.log(JSON.stringify(result.branch_manager_fso));
                // return false;  
                 if (result.branch_manager_fso.length > 0) {
                    var user_id = "";
                    var lead_fso = '<label>Branch Manager FSO:</label> <select name="branch_manager_fso" id="branch_manager_fso" required >';
                    lead_fso += '<option value="" >--Select Branch Manager Fso--</option>';
                    $.each(result.branch_manager_fso, function(index, element) {

                        lead_fso += '<option value="' + element.user_id + '"> ' + element.user_title + ' ' + element.first_name + ' ' + element.last_name + '</option>';
                    });
                    //user id getting
                    //console.log(result.user_id);
                    lead_fso += '</select>';
                    $('.lead_list_view_fso').html(lead_fso);
                    //remove attribute
                    $('#excel_import').removeAttr('hidden');
                } else {
                    $('.lead_list_view_fso').html('');
                    //add attribute
                    $('#excel_import').attr('hidden', true);
              }

            }
        });
        return false;
    } else {
        $('.lead_list_view_fso').html('');
        //add attribute
        $('#excel_import').attr('hidden', true);
    }
}); 
</script>