<?php include_admin_header(); ?>
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="page-head">
				<div class="page-title">
					<h1><?php echo $page_title; ?></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box grey-cascade">
						<!--<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Assign Filter Data
							</div>
							
						</div>-->
	<div class="row filter-select">
	<div class="form-group col-md-3">
	<select name="leadStatus" id="leadStatus" class="form-control custom-select" data-column="1">
       <option value="0" >--LEAD STATUS--</option>
       <?php if(!empty($lead_status)){
          foreach($lead_status  as  $leadStatus){
          	echo  '<option value="'.$leadStatus['status_id'].'">'.ucfirst($leadStatus['status_name']).'</option>';
          }
          
          } ?>
    </select>
	</div>		
	<div class="form-group col-md-3">
		<select name="leadCategory" id="leadCategory" class="form-control custom-select" data-column="3">
       <option value="0" >--LEAD CAEGORY--</option>
       <?php if(!empty($lead_category)){
          foreach($lead_category  as  $leadCategory){
          	echo  '<option value="'.$leadCategory['category_id'].'">'.ucfirst($leadCategory['category_name']).'</option>';
          }
          
          } ?>
        </select>		
	</div>
	<div class="form-group col-md-3">
		<select name="branch_manager" id="branch_manager" class="form-control custom-select" data-column="3">
       <option value="0" >--BRANCH MANAGER--</option>
        <?php 
        // echo "<pre>";print_r($branchManagers);exit();
        if(!empty($branchManagers)){
           foreach($branchManagers  as  $branchManager){
           	echo  '<option value="'.$branchManager['user_id'].'">'.ucfirst($branchManager['first_name']).'</option>';
          }
          } ?>
        </select>		
	</div>
	
                     <!-- <div class="form-group col-md-3">
                     <form method="post" id="lead_data" enctype="multipart/form-data">
                        <div class="lead_list_view"></div>
                        <div class="user_id"></div>
                        <div id="hidden_user_id"></div>
                        <div id="submit_data"></div>
                     </form>
                     </div> -->
		<div class="col-lg-3 form-group" >      
            <div class="fso-list-show" >
            </div>
        </div>
	</div>
						<div class="portlet-body clearfix">
        <div class="col-lg-12">      
            <div id="render-list-of-order">
            </div>
        </div>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include_admin_footer(); ?>
	<script type="text/javascript">
		// DATATABLE SCRIPT CODE BEGIN 
$(document).ready(function () {
	generateOrderTable();
$(document).on('change', '#leadStatus', function () {
                generateOrderTable();
            });
$(document).on('change', '#leadCategory', function () {
                generateOrderTable();
            }); 
$(document).on('change','#branch_manager',function(){
window.b_m_user_id = $(this).val();
 generateOrderTable(true);
//alert(window.user_id);return false;
});
$(document).on('change','#fso_filter_data',function(){
 // alert();
window.b_m_fso_id = $(this).val();
//alert(window.lead_id);
generateOrderTable();
// alert(lead_id);
});
	function generateOrderTable(getData) {
                 var element = {
                    leadStatus: $('#leadStatus').val(),
                    leadCategory: $('#leadCategory').val(),
                    b_m_user_id:window.b_m_user_id,
                    b_m_fso_id:window.b_m_fso_id
                }; 
				//alert(JSON.stringify(element));return false;
				var url="<?php echo base_url(); ?>lead_list/filterleads";
                $.ajax({
					url: url,
					data:{
						lead_status :element.leadStatus,
                        lead_category :element.leadCategory,
                        b_m_user_id :element.b_m_user_id,
						b_m_fso_id :element.b_m_fso_id
					},
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function () {
                        $('#render-list-of-order').html('<div class="text-center mrgA padA"><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i></div>');
                    },
                    success: function (html) { 
                   //console.log(html.fso_lead.length);return false;
                      if(html.fso_lead.length>1){
                      var leadHtml_fso='<select name="fso_filter_data" id="fso_filter_data" class="form-control ">';
                      leadHtml_fso+='<option value="0" >--Select FSO --</option>';
                      $.each(html.fso_lead, function(index, element) {
                                 
                      leadHtml_fso+='<option value="'+element.user_id+'">'+element.first_name+' '+element.last_name+'</option>';
                       });
                         //user id getting
                          //console.log(result.user_id);
                         leadHtml_fso+='</select>';
                         
                     } 
                    if(getData==true) {
                        $('.fso-list-show').html(leadHtml_fso);
                    }
                              
						
						var dataTable = '<table id="order-datatable" class="table table-striped" cellspacing="0" width="100%"></table>';
						//$('#fso-list-show').html(html);
                        $('#render-list-of-order').html(dataTable);
                        var table = $('#order-datatable').DataTable({
                            aoColumnDefs: [
                                {
                                    'bSortable': false,
                                    'aTargets': [-1]
                                }
                            ],
                            bJQueryUI : true,
                            bDestroy : true,
                            aaSorting : [[0, 'asc']],

                            data: html.filter_data.data,
                            "bPaginate": true,
                            // "pageLength": 3,
                            "bLengthChange": true,
                            "bFilter": true,
                            "bInfo": true,
                            "bAutoWidth": true,
                            columns: [
                                {title: "S.NO"},
								{title: "FIRST NAME"},
                                {title: "STATUS NAME"},
                                {title: "CATEGORY NAME"},
                                {title: "LEAD TITLE"},
                                {title: "APPOINTMENT DATE"},
                                {title: "RENEWAL DATE"},
                                {title: "ACTION"},
                            ],
                        });
                    },
                    error: function (html) {
                        JSON.stringify(html);
                        console.log("error" + html);
                    }
                });
            }
		});
</script>
