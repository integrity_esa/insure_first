<?php
include_admin_header();
?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php
include_admin_side_bar();
?>
	<style type="text/css">
	.error {
		color: red;
	}
</style>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?php
echo $page_title;
?></h1>
				</div>
			</div>
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?php
echo base_url('admin/dashboard');
?>">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#"><?php
echo $page_title;
?></a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i><?php
echo $page_title;
?>
							</div>
						</div>
						<div class="portlet-body">
							<form method="post" id="addsettings" enctype="multipart/form-data" autocomplete="off" data-url="<?php
echo base_url();
?>">
								<div class="inner-content clearfix">
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-5">
											<div class="profile-details">
												<div class="profile-photo">
												<label>Logo image</label>
														<input type="file" onchange="readURL(this);" name="logo_image" id="logo_image" accept="image/*" value="">
														<?php

if (!empty($settings['logo_image']))
	{
	$image_file_path = FCPATH . 'assets/images/logo_images/' . $settings['logo_image'];
	if (file_exists($image_file_path))
		{
?>

															<img style="width: 150px; height: 150px; border-radius: 50%;" src="<?php
		echo site_url('assets/images/logo_images/' . $settings['logo_image']);
?>" />
													<?php
		}
	}

?>
													<input type="hidden" name="old_logo_image" value="<?php
echo !empty($settings['logo_image']) ? $settings['logo_image'] : '';
?>">					
												</div>
											</div>
										</div>
										<div class="col-md-5">
											<div class="profile-details">
												<div class="profile-photo">
												<label>Favicon image</label>

														<input type="file" onchange="readURL(this);" name="favicon_image" id="favicon_image"  accept="image/*" value="">
														<?php

if (!empty($settings['favicon_image']))
	{
	$image_file_path1 = FCPATH . 'assets/images/favicon_images/' . $settings['favicon_image'];
	if (file_exists($image_file_path1))
		{
?>

<img style="width: 150px; height: 150px; border-radius: 50%;" src="<?php
		echo site_url('assets/images/favicon_images/' . $settings['favicon_image']);
?>" />
<?php
		}
	}

?>		
<input type="hidden" name="old_favicon_image" value="<?php
echo !empty($settings['favicon_image']) ? $settings['favicon_image'] : '';
?>">	
												</div>
											</div>
										</div>
										<div class="col-md-1"></div>
									</div>
									
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Site Name</label>
													<input type="text" value="<?php
echo !empty($settings['site_name']) ? $settings['site_name'] : '';
?>" name="site_name" id="site_name" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-5">
											<div class="form-group">
											<label>Admin Email</label>
											<input type="text" name="admin_email" value="<?php
echo !empty($settings['admin_email']) ? $settings['admin_email'] : '';
?>"  id="admin_email" class="form-control" placeholder="">
											<span class="mandatory" style="color:red;"></span>
										</div>
											</div>
											
											<div class="col-lg-1"></div>
										</div>										
						
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Kilometer</label>
											<input type="text" value="<?php
echo !empty($settings['kilometer']) ? $settings['kilometer'] : '';
?>" name="kilometer" id="kilometer" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control" placeholder="">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-5">
									<div class="form-group">
											<label>Assign Item Number</label>
											<input type="text" value="<?php
echo !empty($settings['item_number']) ? $settings['item_number'] : '';
?>" name="item_number" id="item_number" onkeypress="return isNumberKey(event)" class="form-control" placeholder="">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-1"></div>
								</div>
								<input type="hidden" name="edit_id" value="<?php
echo !empty($settings['id']) ? $settings['id'] : '';
?>">
					
							<div class="row">
								<div class="col-lg-4 mx-auto">
									<button type="button" class="btn btn-info photo-btn purchase-but add_settings" value="setting_submit" name="setting_submit">SAVE</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>

<?php
include_admin_footer();
?>
