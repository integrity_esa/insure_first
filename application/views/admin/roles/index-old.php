<?php include_admin_header(); ?>
	
<?php include_admin_side_bar(); ?>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<?php echo $page_title;?>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#"><?php echo $page_title;?></a>
							<i class="fa fa-angle-right"></i>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<!--<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							Add a users
							</button>-->
							<a href="<?php echo base_url().'admin/add_role';?>" class="add_role_link"> Add Role</a>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-cogs"></i><?php echo $page_title;?>
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse">
									</a>
									<a href="#portlet-config" data-toggle="modal" class="config">
									</a>
									<a href="javascript:;" class="reload">
									</a>
									<a href="javascript:;" class="remove">
									</a>
								</div>
							</div>
							<div class="portlet-body">
							<?php //echo $this->dynamic_menu->build_menu('2'); ?>
								<div class="table-scrollable">
									<table class="table table-hover">
									<thead>
									<tr>
										<th>
											 #
										</th>
										<th>
											Role
										</th>
										<th>
											Permissions
										</th>
										<th>Actions</th>										
									</tr>
									</thead>
									<tbody>
									<?php
									if(!empty($roles)) {
										$i=1;
										foreach($roles as $role){ 

											$per =json_decode($role['permissions']);?>

										 	<tr id="<?php echo $role['role_id'];?>">
												<td><?php echo $i;?></td>
												<td><?php echo $role['role_name'];?></td>
												<td><?php 
												if(!empty($per)){
													foreach($per as $key=>$p){
													 echo '<b>'.$key.'</b></br>';
													foreach($p as $action){
														if($action == 'all'){
															$action ='';
														}
														echo  ucfirst($action).' ';

														
																										
													} 
													echo '</br>';
													}
												}
												?></td>
												<td> 
													<a href="<?php echo base_url().'admin/edit_role';?>" class="edit_link">Edit</a>
													<a data-url ="<?php echo base_url().'admin/delete_role';?>" href="javascript:void(0);" class="delete_link">Delete</a>
													<a href="<?php echo base_url().'admin/view_role';?>" class="view_link">View</a>
												</td>
											</tr>
									<?php
											$i++;
										 } 
									 } ?>
									</tbody>
									</table>
									<div class="confirm_box">
										ARe you Confirm to delete this role?
										<span class="confirm">Yes</span>
										<span class="cancel">Cancel</span>

									</div>
								</div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				
				
				
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->

		<?php include_admin_footer(); ?>