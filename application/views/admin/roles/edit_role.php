<?php include_admin_header(); ?>
	
<?php include_admin_side_bar(); ?>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				
				<!-- BEGIN PAGE HEADER-->
				<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?php
				echo $page_title;
				?></h1>
				</div>
			</div>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="<?php
				echo base_url('admin/dashboard');
				?>">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#"><?php
				echo $page_title;
				?></a>
					</li>
				</ul>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-cogs"></i><?php echo $page_title;?>
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse">
									</a>
									<a href="#portlet-config" data-toggle="modal" class="config">
									</a>
									<a href="javascript:;" class="reload">
									</a>
									<a href="javascript:;" class="remove">
									</a>
								</div>
							</div>
							<div class="portlet-body">
							<?php //echo $this->dynamic_menu->build_menu('2'); ?>
								<div class="portlet-body">
								<form id="new_role" class="new_role" action="post">
								<div>
									<label>Role Name</label>
									<input type="text" class="form-control" name="role_name" id="role_name" />
								</div>
								<div>
								<span> Role Permissions</span><br>
									<label>Role Management</label><br>
									<input type="checkbox" class="r_permissions checkall" name="r_permissions" value="all">--Select all--<br>
  									<input type="checkbox" class="r_permissions " name="r_permissions" value="add" >Add<br>
  									<input type="checkbox" class="r_permissions" name="r_permissions" value="edit" >Edit<br>
  									<input type="checkbox" class="r_permissions" name="r_permissions" value="delete" >Delete<br>
  									<label>User Management</label><br>
									<input type="checkbox" class="u_permissions checkall" name="u_permissions" value="all">--Select all--<br>
  									<input type="checkbox" class="u_permissions" name="u_permissions" value="add" >Add<br>
  									<input type="checkbox" class="u_permissions" name="u_permissions" value="edit" >Edit<br>
  									<input type="checkbox" class="u_permissions" name="u_permissions" value="delete" >Delete<br>
  									<label>Branch Management</label><br>
									<input type="checkbox" class="b_permissions checkall" name="b_permissions" value="all">--Select all--<br>
  									<input type="checkbox" class="b_permissions" name="b_permissions" value="add" >Add<br>
  									<input type="checkbox" class="b_permissions" name="b_permissions" value="edit" >Edit<br>
  									<input type="checkbox" class="b_permissions" name="b_permissions" value="delete" >Delete<br>
  									<label>Policy Management</label><br>
									<input type="checkbox" class="p_permissions checkall" name="p_permissions" value="all">--Select all--<br>
  									<input type="checkbox" class="p_permissions" name="p_permissions" value="add" >Add<br>
  									<input type="checkbox" class="p_permissions" name="p_permissions" value="edit" >Edit<br>
  									<input type="checkbox" class="p_permissions" name="p_permissions" value="delete" >Delete<br>
									<label>Settings</label><br>
  									<input type="checkbox" class="settings_permissions" name="settings_permissions" value="edit" >Edit<br>
  									
								</div>
								<input type="button" id="role_submit" data-url="<?php echo base_url();?>" class="role_submit" value="Submit">

								</form>							
							</div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
					
				</div>
				
				
				
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<?php include_admin_footer(); ?>