<?php include_admin_header(); ?>
<div class="page-container">
   <?php include_admin_side_bar();?>
   <style type="text/css">.error {color: red;}</style>
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
               <h1><?php
                  echo $page_title;
                  ?></h1>
            </div>
         </div>
         <!-- BEGIN PAGE BREADCRUMB -->
         <ul class="page-breadcrumb breadcrumb">
            <li>
               <a href="<?php
                  echo base_url('admin/dashboard');
                  ?>">Home</a>
               <i class="fa fa-circle"></i>
            </li>
            <li>
               <a href="#"><?php echo $page_title;
                  ?></a>
            </li>
         </ul>
         <!-- END PAGE BREADCRUMB -->
         <div class="row">
            <div class="col-md-12">
               <!-- BEGIN EXAMPLE TABLE PORTLET-->
               <div class="portlet box grey-cascade">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-globe"></i><?php echo $page_title;?>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <form method="post" id="addsettings" enctype="multipart/form-data" autocomplete="off" data-url="<?php echo base_url();?>">
                        <div class="inner-content clearfix">
                           <div class="row">
                              <div class="col-md-1"></div>
                              <div class="col-md-5">
                                 <div class="profile-details">
                                    <div class="fso_list">
                                       <input type="hidden" id="kilometer" name="kilometer" value="<?php echo $distance['kilometer'];?>">
                                       <input type="hidden" id="limit" name="limit" value="<?php echo $distance['item_number'];?>">
                                       <label>FSO:</label>
                                       <select name="select_fso" id="select_fso" class="select_fso" data-url ="<?php echo base_url('admin');?>">
                                       <option value="0" >--Select FSO--</option>
                                       <?php if(!empty($fsos)){
                                          foreach($fsos  as  $fso){
                                          	echo  '<option value="'.$fso['user_id'].'">'.$fso['user_title'].' '.ucfirst($fso['first_name']).'</option>';
                                          }
                                          
                                          } ?>
                                          </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </form>
                     <div class="row">
                     <div class="col-md-12">
                     <form method="post" id="lead_data" enctype="multipart/form-data">
                        <div class="lead_list_view"></div>
                        <div class="lead_id"></div>
                         <div class="check_box_fso_lead"></div>
                        <div id="hidden_user_id"></div>
                        <div id="submit_data"></div>
                     </form>
                     </div>
                     </div>

                  </div>
               </div>
               <!-- END EXAMPLE TABLE PORTLET-->
               <!-- END PAGE CONTENT-->
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   include_admin_footer();
   ?>
<script type="text/javascript">
   $('body').on('click', '#submit_form_data', function(){
      var form = $('form#lead_data');
      var formData = new FormData(form[0]);
     // alert(formData);return false;
      $.ajax({
         url: siteUrl+'/check_lead_id_update',
         type: 'post',
         dataType: 'json',
         processData: false,
         contentType: false,
         data: formData,
         success: function(res) {
            if (res == 1) {
               alertify.alert('Success', 'Lead assigned successfully.', function(){
                        window.location.href = siteUrl+'/lead_manage';
               });
            }else if(res==2) {
               alertify.alert('Error', 'Please select atleast one lead to submit');
            } else {
               alertify.alert('Error', 'Failed to assign lead.');
            }
         },
         error: function(html) {
            JSON.stringify(html);
            console.log("error" + html);
         }
      });
      return false;

   });
   $('#select_fso').change(function(){
   	var user_id = $(this).val();
   //  alert(user_id); 
      // return false;
   	var url = $(this).data('url');
   	var kms = $("#kilometer").val();
   	var limit = $("#limit").val();
   	  if (user_id != "" || user_id !=0) {
              $.ajax({
                  url: url+"/lead_manage",
                  data: {user_id:user_id,limit:limit,kms:kms},
                  type: "post",
                  dataType:"JSON",
                  success: function (result) {
                     if(result.leads.length>0){
                        var lead_id="";
                        var check_bx_fso_lead ="";
                     	var leadHtml='<ol>';
                     	$.each(result.leads, function(index, element) {
                          // console.log(element);
                     	 	//leadHtml+='<li>'+element.lead_title+' '+element.first_name+' '+element.last_name+'</li>';
                            leadHtml+='<li><input type="checkbox" name="check_fso_lead[]" value="'+element.lead_id+'"/> '+element.lead_title.toUpperCase()+' '+element.first_name.toUpperCase()+' '+element.last_name.toUpperCase()+' <br/>'+element.address_line1.toUpperCase()+'<br/> '+element.address_line2.toUpperCase()+' <br/> '+element.email+' <br/> '+element.pincode+'<br/> '+element.phone+'</li>';
                               lead_id +='<input type="hidden" name="lead_id[]" value="'+element.lead_id+'"/>';
      		           
      		             });
                         //user id getting
                         // console.log(result.user_id);
                         leadHtml+='</ol>';
                        var hidden_user_id ='<input type="hidden" name="user_id" value="'+result.user_id+'"/>';
                        var submit_form ='<button type="button" name="submit" id="submit_form_data" class="btn btn-primary">submit</button>';
                         //console.log(leadHtml);
                        $('.lead_list_view').html(leadHtml);
                        $('.lead_id').html(lead_id);
                        $('.check_box_fso_lead').html(check_bx_fso_lead);
                        $('#hidden_user_id').html(hidden_user_id);
                        $('#submit_data').html(submit_form);
                     } else{
                         alertify.alert('Message', 'No Leads Available.');
                        $('.lead_list_view').html('');
                        $('.lead_id').html('');
                        $('.check_box_fso_lead').html('');
                        $('#hidden_user_id').html('');
                        $('#submit_data').html('');
                     }
                  }
              });
             return false;
          }
           else{
                        $('.lead_list_view').html('');
                        $('.lead_id').html('');
                        $('.check_box_fso_lead').html('');
                        $('#hidden_user_id').html('');
                        $('#submit_data').html('');
          }
   });
   
</script>