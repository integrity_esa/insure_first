<?php include_admin_header(); ?>
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="page-head">
				<div class="page-title">
					<h1>Lead Type</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Lead Type
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a id="sample_editable_1_new" href="<?php echo base_url() ?>admin/leadCategory" class="btn green">
												Add New <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<form id="listing" name="listing">
								<table class="table table-striped table-bordered table-hover" id="typeX">
									<tbody>
										<?php
										if (!empty($categoryVal)) {
											foreach ($categoryVal as $Ctype) { ?>
												<tr class="odd gradeX">
													<td><?php echo $Ctype['category_id'] ; ?></td>
													<td><?php echo ucwords($Ctype['category_name']); ?></td>
													<td><?php echo date('d-m-Y', strtotime($Ctype['created_date'])); ?></td>
													<td><a href="<?php echo base_url(); ?>admin/leadCategory?id=<?php echo $Ctype['category_id'] ?> " class=" btn btn-info purchase-but">Edit</a>
													<?php if($Ctype['status']==2) { ?>
													    <a href=""  class="btn btn-info purchase-but" disabled>Delete</a>
													<?php }else{ ?>
													    <a  href="javascript:void(0)" data-id="<?php echo $Ctype['category_id'];?>"  class="delete btn btn-info purchase-but">Delete</a>
													<?php } ?></td>
												</tr>
											<?php } } ?>
										</tbody>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include_admin_footer(); ?>
	<script type="text/javascript">
		var TableManaged = function () {
			var initTable1 = function () {
				var table = $('#typeX');
				table.dataTable({
					"language": {
						"aria": {
							"sortAscending": ": activate to sort column ascending",
							"sortDescending": ": activate to sort column descending"
						},
						"emptyTable": "No data available in table",
						"info": "Showing _START_ to _END_ of _TOTAL_ entries",
						"infoEmpty": "No entries found",
						"infoFiltered": "(filtered1 from _MAX_ total entries)",
						"lengthMenu": "Show _MENU_ entries",
						"search": "Search:",
						"zeroRecords": "No matching records found"
					},
					"bStateSave": true,
					"lengthMenu": [
					[5, 10, 15, 20, 25, 50, 100, -1],
					[5, 10, 15, 20, 25, 50, 100, "All"] 
					],
					"pageLength": 5,            
					"pagingType": "bootstrap_full_number",
					"language": {
						"search": "My search: ",
						"lengthMenu": "  _MENU_ records",
						"paginate": {
							"previous":"Prev",
							"next": "Next",
							"last": "Last",
							"first": "First"
						}
					},
					"columnDefs": [{ 
						'orderable': false,
						'targets': [0]
					}],
					"order": [[0, "desc"]],
					"aoColumns": [
					{ title: " ID "},
					{ title: " CATEGORY TYPE " },
					{ title: " CREATED DATE "},
					{ title: " ACTION"}, 
					],  
				});	
			}
			return {
				init: function () {
					if (!jQuery().dataTable) {
						return;
					}
					initTable1();
				}

			};

		}();
//change status
$(".delete").on('click',function(){
	var id = $(this).data('id');
	var url="<?php echo base_url(); ?>admin/delete_lead_category";
	alertify.confirm('Success', 'Do you want to Delete this lead?', function(){
	//once confirm ajax function will load
	$.ajax({
	    url: url,
	    type: 'post',
	    dataType: 'json',
	    data: {id : id },
	    success: function(res) {
	            // location.reload();
	            window.location.href="<?php echo base_url(); ?>admin/Lead_Category";
	    },
	    error: function(res) {
	        alert(res);
	        alert("error");
	    }
	});
	//ajax end
	}, 
	function(){ 
	alertify.error('Cancel');
	});
});
</script>>
