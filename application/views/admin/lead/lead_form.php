<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <?php include_admin_side_bar(); ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-head">
                <div class="page-title">
                    <h1>Lead Listing</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box grey-cascade">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>Lead Table
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a id="sample_editable_1_new" href="<?php echo base_url() ?>admin/adminAddNew" class="btn green">
                                                Add New <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div id="divMsg" style="display:none;">
                                    <img src="../images/loading.gif" alt="Please wait.." />
                                    </div>
                                        <form method="post" id="import_form" enctype="multipart/form-data">
                                            <p><label>Select Excel File</label>
                                            <input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
                                            <br />
                                            <input type="submit" name="import" value="Import" class="btn btn-info" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <form id="listingL" name="listingL">
                                <table class="table table-striped table-bordered table-hover" id="leadList">
                                    <tbody>
                                        <?php
                                        if (!empty($leadLists)) {
                                            foreach ($leadLists as $list) {
                                                $leadValue = ($list['lead_status'] == '1') ? 'Not contacted yet' :($list['lead_status'] == '2' ? 'Call Required':($list['lead_status'] == '3' ? 'Visit required':($list['lead_status'] == '4' ? 'Closed': 'Dropped'))); ?>
                                                <tr class="odd gradeX">

                                                    <td><?php echo $list['first_name'] .' '.$list['last_name']; ?></td>
                                                    <td><?php echo $list['email']; ?></td>
                                                    <td><?php echo $leadValue; ?></td>
                                                    <td><?php echo $list['user_fname'] .' '. $list['user_lname']; ?></td>
                                                    <td><a href="<?php echo base_url(); ?>admin/adminAddNew?id=<?php echo $list['id'] ?> " class=" btn btn-info purchase-but">Edit</a>
                                                        <?php if($list['status']==2) { ?>
                                                            <a href=""  class="btn btn-info purchase-but" disabled>Delete</a>
                                                        <?php }else{ ?>
                                                            <a  href="javascript:void(0)" data-id="<?php echo $list['id'];?>"  class="delete btn btn-info purchase-but">Delete</a>
                                                        <?php } ?></td>
                                                    </tr>
                                                <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_admin_footer(); ?>
<script type="text/javascript">
            //change status
        $(".delete").on('click',function(){
            var valDel = $(this).data('id');
            var url="<?php echo base_url(); ?>admin/delete_Lead";
            alertify.confirm('Success', 'Do you want to Delete this lead?', function(){
            //once confirm ajax function will load
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {valDel : valDel },
                success: function(res) {
                        location.reload();
                },
                error: function(res) {
                    alert(res);
                    alert("error");
                }
            });
            //ajax end
            }, 
            function(){ 
            alertify.error('Cancel');
            });
        });
var TableManaged = function () {

    var initTable1 = function () {

        var table = $('#leadList');

        table.dataTable({

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            "bStateSave": true,
            "lengthMenu": [
            [5, 10, 15, 20, 25, 50, 100, -1],
            [5, 10, 15, 20, 25, 50, 100, "All"]         
            ],
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "My search: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  
                'orderable': false,
                'targets': [0]
            }],
            "order": [[0, "desc"]],
            "aoColumns": [
            { title: " NAME "},
            { title: " EMAIL " },
            { title: " LEAD STATUS "},          
            { title: " HANDLED BY "},          
            { title: " ACTION "},

            ],  
        });	
    }
    return {
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }

    };

}();
/**check mail already exit(24-12-2018)***/
function checkEmail() {
    var email = $("#email").val();

    if (email != "") {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/checkEmail",
            data: "userEmail=" + $("#email").val(),
            type: "post",
            success: function (result) {
                $("#email").parent().next(".validation").remove(); 
                if (result == 1) {
                    $("#email").focus();
                    $("#email").css('border-color', 'red');
                    $("#email").closest('.form-group').find('.mandatory').html('Email address already exists');
                    $("#email").val('');
                    return false;
                } else {
                    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    if (reg.test($("#email").val()) == false) {
                        $("#email").focus();
                        $("#email").css('border-color', 'red');
                        $("#email").closest('.form-group').find('.mandatory').html('');
                        $("#email").val('');
                    } else {
                        $("#email").cssselect('border-color', '');
                        $("#email").closest('.form-group').find('.mandatory').html('');                            
                    }
                }
            }
        });
    }
}

//import excel file
    $('#import_form').on('submit', function(event){
        event.preventDefault();
        // $('#')
        $.ajax({
            url:"<?php echo base_url(); ?>Lead_list/import",
            method:"POST",
            data:new FormData(this),
            contentType:false,
            cache:false,
            processData:false,
            dataType:'JSON',
            success:function(data){
                // alert(data.existMails);
                if (data.existMails!="") {
                    alertify.alert('Error', data.existMails+' Emails already exists.', function(){
                        location.reload();
                    });
                } else {
                    // alertify.alert('Error', 'Failed to upload.');
                    alertify.alert('success', 'successfully upload.', 
                        function(){
                        location.reload();
                    });
                 }
            }
        })
    });
</script>