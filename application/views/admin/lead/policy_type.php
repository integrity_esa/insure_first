<?php include_admin_header(); ?>
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="page-head">
				<div class="page-title">
					<h1>Policy Type</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Policy Type
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a id="sample_editable_1_new" href="<?php echo base_url() ?>admin/policyType" class="btn green">
												Add New <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<?php //echo "";print_r($policyVal);die; ?>
							<form id="policylisting" name="policylisting">
								<table class="table table-striped table-bordered table-hover" id="policytype">
									<tbody>
										<?php
										if (!empty($policyVal)) {
											foreach ($policyVal as $policytype) { ?>
												<tr class="odd gradeX">
													<td><?php echo $policytype['policy_id'] ; ?></td>
													<td><?php echo ucwords($policytype['policy_name']); ?></td>
													<td><?php echo date('d-m-Y', strtotime($policytype['created_at'])); ?></td>
													<td><a href="<?php echo base_url(); ?>admin/policyType?id=<?php echo $policytype['policy_id'] ?> " class=" btn btn-info purchase-but">Edit</a>
													<?php if($policytype['status']==2) { ?>
													    <a href=""  class="btn btn-info purchase-but" disabled>Delete</a>
													<?php }else{ ?>
													    <a  href="javascript:void(0)" data-id="<?php echo $policytype['policy_id'];?>"  class="delete btn btn-info purchase-but">Delete</a>
													<?php } ?></td>
												</tr>
											<?php } } ?>
										</tbody>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include_admin_footer(); ?>
	<script type="text/javascript">
		var TableManaged = function () {
			var initTable1 = function () {
				var table = $('#policytype');
				table.dataTable({
					"language": {
						"aria": {
							"sortAscending": ": activate to sort column ascending",
							"sortDescending": ": activate to sort column descending"
						},
						"emptyTable": "No data available in table",
						"info": "Showing _START_ to _END_ of _TOTAL_ entries",
						"infoEmpty": "No entries found",
						"infoFiltered": "(filtered1 from _MAX_ total entries)",
						"lengthMenu": "Show _MENU_ entries",
						"search": "Search:",
						"zeroRecords": "No matching records found"
					},
					"bStateSave": true,
					"lengthMenu": [
					[5, 10, 15, 20, 25, 50, 100, -1],
					[5, 10, 15, 20, 25, 50, 100, "All"] 
					],
					"pageLength": 5,            
					"pagingType": "bootstrap_full_number",
					"language": {
						"search": "My search: ",
						"lengthMenu": "  _MENU_ records",
						"paginate": {
							"previous":"Prev",
							"next": "Next",
							"last": "Last",
							"first": "First"
						}
					},
					"columnDefs": [{ 
						'orderable': false,
						'targets': [0]
					}],
					"order": [[0, "desc"]],
					"aoColumns": [
					{ title: " ID "},
					{ title: " TYPE NAME " },
					{ title: " CREATED DATE "},  
					{ title: " ACTION "},  
					],  
				});	
			}
			return {
				init: function () {
					if (!jQuery().dataTable) {
						return;
					}
					initTable1();
				}

			};

		}();
//change status
$(".delete").on('click',function(){
	var id = $(this).data('id');
	var url="<?php echo base_url(); ?>admin/delete_policy_type";
	alertify.confirm('Success', 'Do you want to Delete this lead?', function(){
	//once confirm ajax function will load
	$.ajax({
	    url: url,
	    type: 'post',
	    dataType: 'json',
	    data: {id : id },
	    success: function(res) {
	            location.reload();
	    },
	    error: function(res) {
	        alert(res);
	        alert("error");
	    }
	});
	//ajax end
	}, 
	function(){ 
	alertify.error('Cancel');
	});
});
</script>>
