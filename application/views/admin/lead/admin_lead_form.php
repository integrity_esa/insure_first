<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
	<style type="text/css">
	.error {
		color: red;
	}
</style>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Lead Listing</h1>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Lead Table
							</div>
						</div>
						<?php //echo "<pre>";print_r($editListing);exit(); ?>
						<div class="portlet-body">
							<form method="post" id="adminAddUser" enctype="multipart/form-data" autocomplete="off">
								<input type="hidden" name="update_id" value="<?php echo !empty($editListing)?$editListing['id'] : '' ; ?>">
								<div class="inner-content clearfix">
									<div class="row">
										<div class="col-md-3"></div>
										<div class="col-md-6">
											<div class="profile-details">
												<div class="profile-photo">
													<a href="#">
													<?php 
													if(!empty($editListing['lead_img'])){ ?>
													<img style="width: 150px; height: 150px; border-radius: 50%;"  id="user_image" src="<?php echo base_url() .'assets/images/user_images/'.$editListing['lead_img'];?>" onerror="this.onerror=null; this.src='<?php echo base_url('assets/images/user.png') ?>';" src="#" height="100" width="100" />
													<?php }else { ?>
													<img style="width: 150px; height: 150px; border-radius: 50%;"  id="user_image" src="" onerror="this.onerror=null; this.src='<?php echo base_url('assets/images/user.png') ?>';" src="#" height="100" width="100" />
													<?php }
													?>
														<img style="position: relative; top: 60px; right: 45px; cursor: pointer;" onclick="$('#user_images').click()" src="<?php echo base_url() ?>assets/images/cam.png" class="img-fluid" alt="cam-img" />
														<input type="file" onchange="readURL(this);" name="user_image" id="user_images" value="<?php echo !empty($editListing)?$editListing['lead_img'] : '' ; ?>" style="display: none;"  accept="image/*">
														<input type="hidden" name="old_user_image" value="<?php echo !empty($editListing)?$editListing['lead_img'] : '' ; ?>">
													</a>						
												</div>
											</div>
										</div>
										<div class="col-md-3"></div>
									</div>
									<div class="detail-form">
										<div class="row">
											<div class="col-lg-1"></div>
											<!-- <span class="mandatory" style="color:red;"></span> -->
											<div class="col-lg-5">
												<div class="form-group">
													<label>Title</label>
													<select name="title" id="title" class="form-control ">
														<option value="">-- Select --</option>
														<option value="1" <?php if(!empty($editListing['lead_title'])) {
															echo $editListing['lead_title'] == "1"?'selected':'';
															} ?>>Mr.</option>
														<option value="2" 
														 <?php if(!empty($editListing['lead_title'])) {
															echo $editListing['lead_title'] == "2"?'selected':'';
															} ?>>Ms.</option>
														<option value="3" <?php if(!empty($editListing['lead_title'])) {
															echo $editListing['lead_title'] == "3"?'selected':'';
															} ?>>Mrs.</option>
													</select>
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>First Name *</label>
													<input type="text"  name="fname" id="fname" class="form-control" value="<?php echo !empty($editListing['first_name']) ?$editListing['first_name'] : ' ' ?>" placeholder="">
												</div>
											</div>
											<div class="col-lg-1"></div>
										</div>
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Last Name</label>
													<input type="text" value="<?php echo !empty($editListing['last_name']) ? $editListing['last_name'] : ' ' ?>" name="lname" id="lname" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Aadhar Number</label>
													<input type="text" value="<?php echo !empty($editListing['aadhar_id']) ?  $editListing['aadhar_id']: ' ' ?>" name="aadharnum" 
													onkeypress="return isNumberKey(event)" maxlength="16" id="aadharnum" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-1"></div>
										</div>
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Address 1</label>
													<input type="text" value="<?php echo !empty($editListing['address_line1']) ? $editListing['address_line1'] : ' ' ?>" name="address1" id="address1" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Address 2</label>
													<input type="text" value="<?php echo !empty($editListing['address_line2']) ? $editListing['address_line2'] : ' ' ?>" name="address2" id="address2" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-1"></div>
										</div>
										<div class="row">
											<div class="col-lg-1"></div>					
											<div class="col-lg-5">
												<div class="form-group">
													<label>Country</label>
													<select name="country" id="country" oninvalid="this.setCustomValidity('Select Your Country Here')" oninput="this.setCustomValidity('')"
													class="form-control custom-select">
													<option value="">-- Select Country --</option>
													<?php
													foreach ($getCountry as $country) {
		                                        	$selected=$editListing['country_id']==$country['country_id']?'selected':'';
		                                            echo '<option value="' . $country['country_id'] . '" '.$selected.'>' . ucwords($country['country_name']) . '</option>';
			                                        }
													?>
												</select>
											</div>
										</div>
										<div class="col-lg-5">
											<div class="form-group">
												<label>State</label>
												<select name="state" id="state" oninvalid="this.setCustomValidity('Select Your State Here')" oninput="this.setCustomValidity('')"class="form-control custom-select" data-selected="<?php echo !empty($editListing['state_id'])?$editListing['state_id']:''; ?>">
												<option value="">-- Select State --</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>City</label>
											<select name="city" id="city" oninvalid="this.setCustomValidity('Select Your city Here')" oninput="this.setCustomValidity('')"class="form-control custom-select"
											data-selected="<?php echo !empty($editListing['city_id'])?$editListing['city_id']:''; ?>">
												<option value="">-- Select City --</option>
											</select>
										</div>
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Pincode</label>
											<input type="text" onkeypress="return isNumberKey(event)" value="<?php echo !empty($editListing['pincode']) ? $editListing['pincode'] : ' ' ?>" class="form-control" name="pincode" id="pincode" placeholder="">
										</div>
									</div>
									<div class="col-lg-1"></div>
								</div>
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>Phone Number</label>
											<input type="text" value="<?php echo !empty($editListing['phone']) ? $editListing['phone'] : ' ' ?>" name="phone" id="phone" onkeypress="return isNumberKey(event)" class="form-control" placeholder="">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<label>E-Mail</label>
											<input type="text" name="email" value="<?php echo !empty($editListing['email']) ? $editListing['email'] : ' ' ?>"  id="email" class="form-control" placeholder="">
											<span class="mandatory" style="color:red;"></span>
										</div>
									</div>
									<div class="col-lg-1"></div>
								</div>
								<div class="row">
									<div class="col-lg-1"></div>					
									<div class="col-lg-5">
										<div class="form-group">
											<label>Gender</label>
											<select class="form-control" id="gender" name="gender" />
											<option value="">--Select--</option>
											<option value="1" <?php if(!empty($editListing['gender'])) {
												echo $editListing['gender'] == "1"?'selected':''; } ?>>Male</option>
											<option value="2" <?php if(!empty($editListing['gender'])) {
												echo $editListing['gender'] == "2"?'selected':''; } ?>>Female</option>
										</select>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group cal-img">
										<label>Date Of Birth</label>
										<input type="text"  value="<?php echo !empty($editListing['dob']) ? date("d-m-Y", strtotime($editListing['dob'])) : ' ' ?>" name="dob" id="dob" class="form-control" placeholder="">
									</div>
								</div> 
								<div class="col-lg-1"></div>
							</div>

							<div class="row">
								<div class="col-lg-1"></div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Company</label>
										<input type="text" value="<?php echo !empty($editListing['company']) ? $editListing['company'] : ' ' ?>" name="company" id="company" class="form-control" placeholder="">
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Marital Status</label>
										<select name="m_status"  id="m_status" class="form-control custom-select">
											<option value="">-- Select --</option>
											<option value="1" <?php if(!empty($editListing['marital_status'])){
												echo $editListing['marital_status'] =="1"?'selected':'';
												} ?>>Single</option>
											<option value="2" <?php if(!empty($editListing['marital_status'])){
												echo $editListing['marital_status'] =="2"?'selected':'';
												} ?>>Married</option>
										</select>
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
									<div class="form-group">
										<label>Spouse Name</label>
										<input type="text" value="<?php echo !empty($editListing['spouse_name']) ? $editListing['spouse_name'] : ' ' ?>" name="spousename" id="spousename" class="form-control" placeholder="" disabled>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group cal-img">
										<label>Anniversary</label>
										<input type="text" value="<?php echo !empty($editListing['anniversary']) ? date("d-m-Y", strtotime($editListing['anniversary'])) : ' ' ?>" name="anniversary" id="anniversary" class="form-control" id="datepicker2" placeholder="">
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<div class="row">
								<div class="col-lg-1"></div>
								<div class="col-lg-5">
									<label>Policy Type</label>
									<select name="policytype" id="policytype" class="form-control custom-select">
										<option value="">--Select--</option>
										<?php
												foreach ($leadpolicyVal as $ptype) {
	                                        	$selected=$editListing['lead_type']==$ptype['policy_id']?'selected':'';
	                                            echo '<option value="' . $ptype['policy_id'] . '" '.$selected.'>' . ucwords($ptype['policy_name']) . '</option>';
		                                        }
											?>
									</select>

								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Garage Name</label>
										<input type="text" value="<?php echo !empty($editListing['garage_name']) ? $editListing['garage_name'] : ' ' ?>" name="gname" id="gname" class="form-control" placeholder="">
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<div class="row">
								<div class="col-lg-1"></div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Campaign Name/Location of Campaign</label>
										<input type="text" value="<?php echo !empty($editListing['campaign_name']) ? $editListing['campaign_name'] : ' ' ?>" name="campaign" id="campaign" class="form-control" placeholder="">
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Lead Type</label>
										<select name="leadtype" id="leadtype" class="form-control custom-select">
											<option value="">-- Select --</option>
											<?php
												foreach ($leadtypeVal as $type) {
	                                        	$selected=$editListing['lead_type']==$type['lead_id']?'selected':'';
	                                            echo '<option value="' . $type['lead_id'] . '" '.$selected.'>' . ucwords($type['leadname']) . '</option>';
		                                        }
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<div class="row">
								<div class="col-lg-1"></div>					
								<div class="col-lg-5">
									<div class="form-group">
										<label>Lead Category</label>
										<select name="leadcategory" id="leadcategory" class="form-control custom-select">
											<option value="">-- Select --</option>
											<?php
												foreach ($leadcategoryVal as $ctype) 
												{
	                                        	$selected=$editListing['lead_cat']==$ctype['category_id']?'selected':'';
	                                            echo '<option value="' . $ctype['category_id'] . '" '.$selected.'>' . ucwords($ctype['category_name']) . '</option>';
		                                        }
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Lead Status</label>
										<select name="leadstatus" id="leadstatus" class="form-control custom-select">
											<option value="">--Select--</option>
											<?php
												foreach ($leadstatusVal as $statustype) {
	                                        	$selected=$editListing['lead_status']==$statustype['status_id']?'selected':'';
	                                            echo '<option value="' . $statustype['status_id'] . '" '.$selected.'>' . ucwords($statustype['status_name']) . '</option>';
		                                        }
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<div class="row">
								<div class="col-lg-1"></div>
								<div class="col-lg-5">
									<div class="form-group cal-img">
										<label>Renewal Date</label>
										<input type="text" value="<?php echo !empty($editListing['renewal_date']) ? date('d-m-Y', strtotime($editListing['renewal_date'])) : ' '; ?>" name="renewal" id="renewal" class="form-control" placeholder="">
									</div>
								</div>				
								<div class="col-lg-5">
									<div class="form-group cal-img">
										<label>Appointment Date</label>
										<input type="text" value="<?php echo !empty($editListing['appointment_date']) ? date('d-m-Y', strtotime($editListing['appointment_date'])) : ' ' ?>" name="appointment" id="appointment" class="form-control" placeholder="">
									</div>
								</div>
								<div class="col-lg-5">
								</div>
								<div class="col-lg-1"></div>
							</div>
							<div class="row">
								<div class="col-lg-4 mx-auto">
									<button class="btn btn-info photo-btn purchase-but">SAVE</button>
									<input type="button" class="btn btn-info photo-btn purchase-but" id="cancel_btn" value="CANCEL">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>
<script type="text/javascript">

	function checkEmail() {
        var email = $("#email").val();

        if (email != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>admin/checkEmail",
                data: "userEmail=" + $("#email").val(),
                type: "post",
                success: function (result) {
                    $("#email").parent().next(".validation").remove(); // remove it
                    if (result == 1) {
                        $("#email").focus();
                        $("#email").css('border-color', 'red');
                        $("#email").closest('.form-group').find('.mandatory').html('Email address already exists');
                        $("#email").val('');
                        return false;
                    } else {
                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (reg.test($("#email").val()) == false) {
                            $("#email").focus();
                            $("#email").css('border-color', 'red');
                            $("#email").closest('.form-group').find('.mandatory').html('');
                            $("#email").val('');
                        } else {
                            $("#email").cssselect('border-color', '');
                            $("#email").closest('.form-group').find('.mandatory').html('');                            
                        }
                    }
                }
            });
        }
    }

</script>
<?php include_admin_footer(); ?>
<script type="text/javascript">
	    $(function(){
    // redirect cancel_btn
    $('body').on('click','input#cancel_btn',function(){
    	window.location.href= siteUrl+"/load_lead";
    });
    });
</script>