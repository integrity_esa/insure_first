<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
<style type="text/css">
	.error {
		color: red;
	}
</style>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="page-head">
				<div class="page-title">
					<h1>Policy Listing</h1>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Policy Table
							</div>
						</div>
						<div class="portlet-body">
							<form method="post" id="policytype" autocomplete="off">
							<input type="hidden" name="update_id" value="<?php echo !empty($edit_policy_type)?$edit_policy_type['policy_id'] : '' ; ?>">
								<div class="inner-content clearfix">
									<div class="detail-form">
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Policy Type</label>
													<input type="text" value="<?php echo !empty($edit_policy_type)?$edit_policy_type['policy_name'] : '' ; ?>" name="ptype" id="ptype" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-1"></div>
										</div>
							<div class="row">
								<div class="col-lg-4 mx-auto">
									<button class="btn btn-info photo-btn purchase-but">SAVE</button>
									<input type="button" class="btn btn-info photo-btn purchase-but" id="cancel_btn" value="CANCEL">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_admin_footer(); ?>
<script type="text/javascript">
	$(document).ready(function() {

            $("#policytype").validate({
                rules: { 
                    ptype: {  required: true }
                },
                messages :{
                    "ptype" : {
                    required : 'Enter Policy Type'
                    }
                }
            });
/** form submit for policy type (28-12-18) **/
    $(document).on('submit','form#policytype',function(){
        var formss = $('form#policytype');
        var formData = new FormData(formss[0]);
        var url = siteUrl+"/adminpolicyType";
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: formData,
            success: function(res) {
                if (res=="success") {
                    alertify.alert('Success', 'successfully Update.', function(){
                        //location.reload();
                        window.location.href= siteUrl+"/policy_type";
                    });
                } else if (res == "category_exist"){
                	 alertify.alert('Error', 'Already exist this category');
                }
                else{
                    alertify.alert('Error', 'Failed to update user information.');
                }
            },
            error: function(html) {
                JSON.stringify(html);
                console.log("error" + html);
            }
        });
        return false;
    });

    // redirect cancel_btn
    $('#cancel_btn').on('click',function(){
    	window.location.href= siteUrl+"/policy_type";
    });
    });
</script>