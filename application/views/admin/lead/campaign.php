<?php include_admin_header(); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php include_admin_side_bar(); ?>
<style type="text/css">
	.error {
		color: red;
	}
</style>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="page-head">
				<div class="page-title">
					<h1>Campaign</h1>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Campaign Table
							</div>
						</div>
						<div class="portlet-body">
							<form method="post" id="campaigntype" autocomplete="off">
							<input type="hidden" name="update_id" value="<?php echo !empty($edit_campaign)?$edit_campaign['campaign_id'] : '' ; ?>">
								<div class="inner-content clearfix">
									<div class="detail-form">
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label>Campaign</label>
													<input type="text" value="<?php echo !empty($edit_campaign)?$edit_campaign['campaign_name'] : '' ; ?>" name="campaign" id="campaign" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-lg-1"></div>
										</div>
							<div class="row">
								<div class="col-lg-4 mx-auto">
									<button class="btn btn-info photo-btn purchase-but">SAVE</button>
									<input type="button" class="btn btn-info photo-btn purchase-but" id="cancel_btn" value="CANCEL">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_admin_footer(); ?>
<script type="text/javascript">
	$(document).ready(function() {

            $("#campaigntype").validate({
                rules: { 
                    campaign: {  required: true }
                },
                messages :{
                    "campaign" : {
                    required : 'Enter Campaign Name'
                    }
                }
            });
/** form submit for campaign (28-12-18) **/
    $(document).on('submit','form#campaigntype',function(){
        var formss = $('form#campaigntype');
        var formData = new FormData(formss[0]);
        var url = siteUrl+"/adminCampaign";
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: formData,
            success: function(res) {
                if (res=="success") {
                    alertify.alert('Success', 'successfully Update.', function(){
                        //location.reload();
                        window.location.href= siteUrl+"/campaign";
                    });
                } else if (res == "category_exist"){
                	 alertify.alert('Error', 'Already exist this category');
                }
                else{
                    alertify.alert('Error', 'Failed to update user information.');
                }
            },
            error: function(html) {
                JSON.stringify(html);
                console.log("error" + html);
            }
        });
        return false;
    });

    // redirect cancel_btn
    $('#cancel_btn').on('click',function(){
    	window.location.href= siteUrl+"/campaign";
    });
    });
</script>