<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserAssign_fsolead extends Front_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('location_model','dashboard_model'));

    }

      /* Assign Leads for Branch Manager to FSO */
    public function assignFsoLead() {
        $data['page_title']= 'Lead Management';
         $data['distance'] = $this->location_model->lead_dist();
        //echo "<pre>";print_r($data['distance']);
         $id = $this->input->post('user_id');
         $kms = $data['distance']['kilometer'];
         $lmt = $data['distance']['item_number'];
         $login_user = $this->session->userdata('user');
        // echo "<pre>";print_r($login_user);exit();
         $user_id = $login_user['user_id'];
         $role_id = $login_user['role_id'];
         
         //echo "<pre>";print_r($kms);exit();
         $data['fsos'] = $this->location_model->assign_fso_list($kms,$user_id,$role_id);
         if(!empty($id)){
             $data['leads'] = $this->location_model->lead_list($id,$kms,$lmt);
             $data['user_id'] =$id;

             die(json_encode(array('leads'=> $data['leads'] ? $data['leads']: array(),'user_id'=>$data['user_id'])));
             
         }
         $this->template->load('frontTemplate', 'assignFsoLead', $data); 
     }
 
    public function check_fso_lead_id_update() {
         $post = $this->input->post();
       
         $flag = 0; // Failure
         if(!empty($post['check_fso_lead'])){
             $this->db->set('reference_id', $post['user_id']);
             $this->db->where_in('lead_id', $post['check_fso_lead']);
             $this->db->update('assign_lead');
             $flag = 1; // Success
         } else{
            $flag = 2;
         }
         die(json_encode($flag));
     }

     
    
    
}
