<style type="text/css">
		 .error {
      color: red;
   }
	</style>
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>CAMPAIGN</p></li>
			</ul>
		</nav>
		<form method="post" id="edituser" enctype="multipart/form-data" autocomplete="off">
		<div class="inner-content clearfix">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div class="profile-details">
						<div class="profile-photo">
                        <input type="hidden" id="kilometer" name="kilometer" value="<?php echo $distance['kilometer'];?>">
                                       <input type="hidden" id="limit" name="limit" value="<?php echo $distance['item_number'];?>">
						<label>FSO's:</label>
                                       <select name="select_fso" id="select_fso" class="select_fso custom-select" data-url ="<?php echo base_url('admin');?>">
                                       <option value="0" >--Select FSO--</option>
                                       <?php if(!empty($fsos)){
                                          foreach($fsos  as  $fso){
                                          	echo  '<option value="'.$fso['user_id'].'">'.$fso['user_title'].' '.ucfirst($fso['first_name']).'</option>';
                                          }
                                          
                                          } ?>
                                          </select>				
						</div>
					</div>
				</div>
				<div class="col-md-4"></div>
			</div>
		</form>
        <div class="row">
		<div class="col-md-2"></div>
                     <div class="col-md-8">
                     <form method="post" id="lead_data" enctype="multipart/form-data">
                        <div class="lead_list_view"></div>
                        <div class="lead_id"></div>
                        <div class="check_box_fso_lead"></div>
                        <div id="hidden_user_id"></div>
                        <div id="submit_data"></div>
                     </form>
					 </div>
					 <div class="col-md-2"></div>
                     </div>
		</div><!--end inner-content-->
	</div><!--end Page Content Holder -->
</div>
</body>

