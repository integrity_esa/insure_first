<style type="text/css">
    .form-control{
        width: 100% !important;
    }
    .error{
        color: red;
    }
</style>
<section class="login-box">
    <div class="container">	
        <div class="col-lg-5 mx-auto">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" class="img-fluid" alt="logo-img"/>	
            <div id="show_text"></div>		
            <div class="forgot-box">
                <p>Enter your registered Email below to receive OTP number to sign in</p>
                <form id="validate_form" action="" method="Post" novalidate>
                <div class="row">
                    <div class="col-4">
                        <select class="form-control cus-select">
                            <option >+91</option>
                        </select>
                    </div>
                    <div class="col-8 form-group">
                        <input class="form-control number_only" type="text" placeholder="Enter Your Mobile Number" id="mobile_number" name="mobile_number" value="" maxlength="10" required/>
                        <span id="mandatory"></span>
                    </div>
                </div>
                <button type="submit" name="submit" class="btn email_otp_send" id="email_otp_send">SEND</button>
                </form>
            </div>
        </div>
    </div>
</section>
