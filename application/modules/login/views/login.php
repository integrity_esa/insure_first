<section class="login-box">
    <div class="container"> 
        <div class="col-lg-4 mx-auto">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" class="img-fluid" alt="logo-img"/>
            <div id="spnError"></div>
            <form method="POST" id="logForm" name="login_form" onsubmit="return(validate());">      
                <div class="login-form">
                    <div class="form-group">
                        <img src="<?php echo base_url(); ?>assets/images/user-icon.png" alt="user-icon" />
                        <input type="email" name="email" id="email" class="form-control" placeholder="Mobile / Email" >
                        <span id="error_msg"></span>
                    </div>          
                    <div class="form-group">
                        <img src="<?php echo base_url(); ?>assets/images/key-icon.png" alt="key-icon" />
                        <input type="password" name="password" id="password" class="form-control" placeholder="**************" >
                    </div>
                    <div class="login-btn">
                        <a href="<?php echo base_url(); ?>login/forgot_password" class="forgot">Forgot Password?</a><br>
                        <button type="submit"class="btn btn-default" id="submit_data" value="">SIGN IN</button>
                    </div>
                    <ul class="list-inline social-icons">
                        <li class="list-inline-item"><a href="#"><?php echo theme_img('facebook_icon.png', true); ?></a></li>
                        <li class="list-inline-item"><a href="#"><?php echo theme_img('google_plus_icon.png', true); ?></a></li>
                        <li class="list-inline-item"><a href="#"><?php echo theme_img('twitter_icon.png', true); ?></a></li>
                        <li class="list-inline-item"><a href="#"><?php echo theme_img('instagram_icon.png', true); ?></a></li>
                        <li class="list-inline-item"><a href="#"><?php echo theme_img('linked_in_icon.png', true); ?></a></li>
                    </ul>
                    <div class="follow">
                        <a href="#">Follow as on</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-3">
        </div>
    </div>
</section>
