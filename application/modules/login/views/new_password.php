<style type="text/css">
.error {
  color: red;
  margin-left: 5px;
}
</style>
<?php 
$uri = $this->uri->segment(3);
?>
<section class="login-box">
	<div class="container">	
		<div class="col-lg-5 mx-auto">
			<img src="<?php echo base_url(); ?>assets/images/logo.png" class="img-fluid" alt="logo-img"/>	
			<div id="update_text"></div>			
			<div class="forgot-box confirm-box">
				<div class="container">
				  <form class="form-horizontal" action="" id="update_validate_form">
				    <div class="form-group">
				      <div class="col-sm-10 mx-auto">
				      <label>Enter New Password</label>
				        <input type="password" class="form-control" id="password_1" placeholder="Enter New Password" name="password_1" >
				      </div>
				    </div>
				    <div class="form-group">
				      <div class="col-sm-10 mx-auto"> 
				      <label>Enter Confirm Password</label>         
				        <input type="password" class="form-control" id="password_2" placeholder="Enter Confirm Password" name="password_2" >
				        <span class="message"></span>
				      </div>
				    </div>
				    <input type="hidden" class="form-control" value="<?php echo $uri;?>" id="mobile_number" name="mobile_number" >
				    <div class="form-group">        
				      <div class="col-sm-10 mx-auto">
				      <button type="submit" class="btn update_password" id="update_password">UPDATE</button>
				      </div>
				    </div>
				  </form>
				</div>
			</div>
		</div>
	</div>
</section>