<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Login extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('user_login_model', 'email_model'));
    }

    /* SENTHAMIZHSELVI - Display login page 12-12-2018 */

    public function index() {
        $data['page_title'] = "Login";
        $this->load->view('include/header_login', $data);
        $this->load->view('login/login', $data);
        $this->load->view('include/footer_login');
    }

    /* KARTHIK - login users 12-12-2018 */

    public function login_user() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $userData = $this->user_login_model->login($email, $password);
        if (!$userData) {
            $data = "0";
        } else {
            $userArr = array(
                'first_name' => $userData['first_name'],
                'user_id' => $userData['user_id'],
                'email' => $userData['email'],
                'first_name' => $userData['first_name'],
                'last_name' => $userData['last_name'],
                'role_id' => $userData['role_id']
            );
            $this->session->set_userdata(['userEmail' => $userData['email']]);
            $this->session->set_userdata('user', $userArr);
            $this->session->set_flashdata('message', 'Login Successfully');
            $data = $this->session->userdata('user');
        }
        echo json_encode($data);
    }

    /* KARTHIK -  dashboard 13-12-2018 */

    public function home() {
        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
            $data['page_title'] = "Leads Dashboard";
            $data['get_all_leads'] = $this->user_login_model->get_all_leads();
            $data['get_today_leads'] = $this->user_login_model->get_today_leads();
            $this->load->view('include/header_login', $data);
            $this->load->view('leads_dashboard', $data);
            $this->load->view('include/footer_login');
        } else {
            redirect('/');
        }
    }

    /* KARTHIK - logout users 12-12-2018 */

    public function logout() {
        $this->session->unset_userdata('userEmail');
        $this->session->unset_userdata('user');
        return redirect(base_url() . 'login/login');
    }

    /* KARTHIK - Forgot Password view 12-12-2018 */

    public function forgot_password() {

        $data['page_title'] = "Forgot Password";
        $this->load->view('include/header_login', $data);
        $this->load->view('forgot_password', $data);
        $this->load->view('include/footer_login');
    }

    /* KARTHIK -  email_forget_password 13-12-2018 */

    public function forgot_password_check() {

        $mobile = $this->input->post('mobile_number');
        $result = $this->user_login_model->get_phone($mobile);
        $rand = rand(999, 10000);
        if (!$result) {
            $message = ['status' => '0', 'message' => 'Unregistered_mobile_number'];
        } else {
            $data = array('otp_number' => $rand);
            $res = $this->user_login_model->update($data, 'phone', $mobile, 'users');
            if ($res) {
                $result = $this->email_model->forgot_password_mail($mobile, $rand);
                if ($result) {
                    $message = ['status' => '1', 'message' => 'OTP_mail_send_successful', 'mobile' => $mobile];
                } else {
                    $message = ['status' => '0', 'message' => 'Failed_to_send_mail'];
                }
            } else {
                $message = ['status' => '0', 'message' => 'Failed_to_submit'];
            }
        }
        echo json_encode($message);
        die();
    }

    // KARTHIK - email otp Verification 17-12-2018
    public function otp_verification() {
        $data['page_title'] = "OTP Verification";
        $user_mobile = $this->uri->segment(3);
        $data['Verification_code'] = $this->user_login_model->verification_otp($user_mobile);
        $this->load->view('include/header_login', $data);
        $this->load->view('otp_verification', $data);
        $this->load->view('include/footer_login');
    }

    // KARTHIK - check email otp verification 17-12-2018
    public function check_email_otp_verification() {
        $data['page_title'] = "OTP Verification";
        $first_digit = $this->input->post('first_digit');
        $second_digit = $this->input->post('second_digit');
        $third_digit = $this->input->post('third_digit');
        $forth_digit = $this->input->post('forth_digit');
        $check_number = $first_digit . $second_digit . $third_digit . $forth_digit;
        $check_email_otp = $this->user_login_model->check_verification_email_otp($check_number);
        if (!$check_email_otp) {
            $message = ['message' => 'invalid_otp'];
        } else {
            $message = ['message' => 'valid_otp', 'phone' => $check_email_otp['phone']];
        }
        echo json_encode($message);
        die();
    }

    public function change_password() {
    	$data['page_title'] = "New Password";
        // echo "<pre>";
        // $user_mobile = $this->uri->segment(3);
        // print_r($user_mobile);
        $this->load->view('include/header_login', $data);
        $this->load->view('new_password', $data);
        $this->load->view('include/footer_login');
    }
    public function change_password_update() {
    	if($this->user_login_model->update_password()){
    		$message = ['message' => 'updated_success'];
    	} else {
    	 	$message = ['message' => 'updated_failure'];	
    	}
    	echo json_encode($message);
    }
}