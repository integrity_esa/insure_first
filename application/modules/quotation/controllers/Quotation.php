<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation extends Front_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index(){
        $data['title'] ='Quotation';
        $this->template->load('frontTemplate', 'landing', $data);
    }
}
?>