<!-- Page Content Holder -->
<div id="content">
	<nav class="navbar navbar-default">	
		<ul class=" list-inline navbar-header">
			<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
				<i class="fa fa-bars" aria-hidden="true"></i>
			</button></li>
			<li class="list-inline-item"><p>QUOTATION</p></li>
		</ul>
	</nav>
	<div class="inner-content clearfix">
		<div class="quotation">
			<div class="row">
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="two-wheeler">
						<a href="#"><img src="assets/images/scooty.png" class="img-fluid" alt="two-wheeler" /><br>TWO WHEELER</a>
						<p>Secure your life</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="life">
						<a href="#"><img src="assets/images/leaves.png" class="img-fluid" alt="leaves" /><br>LIFE</a>
						<p>Secure your life</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="car">
						<a href="#"><img src="assets/images/car-quote.png" class="img-fluid" alt="car" /><br>CAR</a>
						<p>Secure your life</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="travel">
						<a href="#"><img src="assets/images/bus.png" class="img-fluid" alt="travel" /><br>TRAVEL</a>
						<p>Secure your life</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="two-wheeler">
						<a href="#"><img src="assets/images/scooty.png" class="img-fluid" alt="two-wheeler" /><br>TWO WHEELER</a>
						<p>Secure your life</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="life">
						<a href="#"><img src="assets/images/leaves.png" class="img-fluid" alt="leaves" /><br>LIFE</a>
						<p>Secure your life</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="car">
						<a href="#"><img src="assets/images/car-quote.png" class="img-fluid" alt="car" /><br>CAR</a>
						<p>Secure your life</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12">
					<div class="travel">
						<a href="#"><img src="assets/images/bus.png" class="img-fluid" alt="travel" /><br>TRAVEL</a>
						<p>Secure your life</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end inner-content-->