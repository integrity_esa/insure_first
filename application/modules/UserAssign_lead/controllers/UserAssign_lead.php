<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserAssign_lead extends Front_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('location_model','dashboard_model','admin_model'));
        $this->load->library('excel');
    }

    /* Assign Leads for Branch Head to Branch Mangaer */
    public function assignLead() {
       $data['page_title']= 'Lead Management';
       $data['distance'] = $this->location_model->lead_dist();
         $kms = $data['distance']['kilometer'];
         $lmt = $data['distance']['item_number'];
         $login_user = $this->session->userdata('user');
        // echo "<pre>";print_r($login_user);exit();
         $user_id = $login_user['user_id'];
         $role_id = $login_user['role_id'];
        $data['branchManagers'] = $this->location_model->branchManagerlist($kms,$user_id,$role_id);
        $this->template->load('frontTemplate', 'assignLead', $data); 
    }
    
    public function import()
    {
        if(isset($_FILES["file"]["name"]))
        {
            // echo "<pre>";
            // print_r($this->input->post('branch_manager_user_id'));
            // exit();
            $branch_manager_id = $this->input->post('branch_manager_user_id');
            $today = date("Y-m-d H:i:s");
            $user_data   = $this->session->userdata('user');
            $user_id = $user_data['user_id'];
            $role_id = $user_data['role_id'];
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                $title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                $fname = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $lname = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $aadharnum = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                $address1 = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                $address2 = $worksheet->getCellByColumnAndRow(5, $row)->getValue();

                $country_name = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                $country_where = " REPLACE(`country_name`, ' ', '') LIKE '$country_name%' OR `country_name` LIKE '$country_name%' ";
                $country = $this->admin_model->getDataFromTable('country_id', 'country', $country_where)['country_id'];
                //echo $this->db->last_query();exit();
                //$country = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                //$state = $worksheet->getCellByColumnAndRow(7, $row)->getValue();

                $state_name = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                $state_where = " REPLACE(`name`, ' ', '') LIKE '%$state_name%' OR `name` LIKE '%$state_name%' ";
                $state = $this->admin_model->getDataFromTable('state_id', 'state', $state_where)['state_id'];
                //echo $this->db->last_query();exit();
                $city_name = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $city_where = " REPLACE(`city_name`, ' ', '') LIKE '%$city_name%' OR `city_name` LIKE '%$city_name%' ";
                $city = $this->admin_model->getDataFromTable('city_id', 'city', $city_where)['city_id'];
                
                //$city = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $pincode = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                $phone = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                $email = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                $gender_id = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                if(strtolower($gender_id) == "male"){
                    $gender =1;
                }else{
                    $gender =2;
                }

                $dob = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                $dob = PHPExcel_Shared_Date::ExcelToPHP($dob);
                $company = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                $m_status_id = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                if(strtolower($m_status_id) == "single"){
                    $m_status =1;
                }else{
                    $m_status =2;
                }
                //echo "<pre>";print_r($m_status);exit();
                $spousename = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                $anniversary = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                $anniversary = PHPExcel_Shared_Date::ExcelToPHP($anniversary);
                // $policytype = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                //$policytype_name = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                $policytype_name = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                $policytype_where = " REPLACE(`policy_name`, ' ', '') LIKE '%$policytype_name%' OR `policy_name` LIKE '%$policytype_name%' ";
                $policytype = $this->admin_model->getDataFromTable('policy_id', 'policy_type', $policytype_where)['policy_id'];
                //echo $this->db->last_query();exit();

                $gname = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                $campaign = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                $leadtype = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

                $lead_name = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                $lead_where = " REPLACE(`leadname`, ' ', '') LIKE '%$lead_name%' OR `leadname` LIKE '%$lead_name%' ";
                $leadtype = $this->admin_model->getDataFromTable('lead_id', 'lead_type', $lead_where)['lead_id'];
              //  echo $this->db->last_query();exit();

                //$leadcategory = $worksheet->getCellByColumnAndRow(22, $row)->getValue();

                $leadcategory_name = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                $leadcategory_where = " REPLACE(`category_name`, ' ', '') LIKE '%$leadcategory_name%' OR `category_name` LIKE '%$leadcategory_name%' ";
                $leadcategory = $this->admin_model->getDataFromTable('category_id', 'lead_category', $leadcategory_where)['category_id'];
                //echo $this->db->last_query();exit();

                //$leadstatus = $worksheet->getCellByColumnAndRow(23, $row)->getValue();

                $leadstatus_name = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                $leadstatus_name_where = " REPLACE(`status_name`, ' ', '') LIKE '%$leadstatus_name%' OR `status_name` LIKE '%$leadstatus_name%' ";
                $leadstatus = $this->admin_model->getDataFromTable('status_id', 'lead_status', $leadstatus_name_where)['status_id'];
                //echo $this->db->last_query();exit();

                $renewal = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                $renewal = PHPExcel_Shared_Date::ExcelToPHP($renewal);
                $appointment = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                $campaign_name = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                $campaign_name_where = " REPLACE(`campaign_name`, ' ', '') LIKE '%$campaign_name%' OR `campaign_name` LIKE '%$campaign_name%' ";
                $campaign_type = $this->admin_model->getDataFromTable('campaign_id', 'campaign_type', $campaign_name_where)['campaign_id'];
               // echo $this->db->last_query();exit();

                $appointment = PHPExcel_Shared_Date::ExcelToPHP($appointment);
                // campaign 3 feilds start
                $campaign_owner = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                $campaign_location = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
                // campaign 3 feilds end
                $checkEmail = $this->admin_model->checkEmailExists($email,$phone);
                //print_r($checkEmail);
                if(!empty($checkEmail)){
                // check mail id and phone exists or not

                    $notInsertMails[]=$email;
                    $error[]="error";
                } else{
                // if mail and phone number is not there it will be inserted
                $lead = array(
                    'lead_title'        =>  $title,
                    'first_name'        =>  $fname,
                    'last_name'         =>  $lname,
                    'address_line1'     =>  $address1,
                    'address_line2'     =>  $address2,
                    'city_id'           =>  $city,
                    'state_id'          =>  $state,
                    'country_id'        =>  $country,
                    'pincode'           =>  $pincode,
                    'email'             =>  $email,
                    'phone'             =>  $phone,
                    'dob'               =>  date('Y-m-d', $dob), 
                    'gender'            =>  $gender,
                    'company'           =>  $company,  
                    'aadhar_id'         =>  $aadharnum, 
                    'marital_status'    =>  $m_status, 
                    'spouse_name'       =>  !empty($spousename)?$spousename:'', 
                    'anniversary'       =>  date('Y-m-d', $anniversary),
                    'modified_by'       => $user_id, 
                    'status'            => 1,  
                    'created_at'        => $today,
                    'created_by'        => $user_id,
                    'updated_at'        => $today
                    // 'lead_img'           => $imagename
                );
                $insert_id = $this->db->insert('lead',$lead);
                $lead_id = $this->db->insert_id();
                $assign_lead =array(
                    'lead_id'         => $lead_id,
                    'garage_name'     => $gname ? $gname:'',
                    'campaign_name'   => $campaign_type ? $campaign_type: '',
                    'campaign_owner'  => $campaign_owner ? $campaign_owner: '',
                    'campaign_location'   => $campaign_location,
                    'lead_type'       => $leadtype,
                    'lead_status'     => $leadstatus,
                    'lead_cat'        => $leadcategory,
                    'policy_type'     => $policytype,
                    'reference_id'    => 0,
                    'role_id'         => $role_id,
                    'branch_head'     => $user_id?$user_id:'',
                    'branch_manager'  => $branch_manager_id?$branch_manager_id:'',
                    'created_by'      => $user_id,
                    'created_at' =>$today,
                    'renewal_date'    => date('Y-m-d', $renewal), 
                    'appointment_date'=> date('Y-m-d', $appointment)
                ); 
                $this->db->insert('assign_lead',$assign_lead);
                //$success[] ="success";
                }
                }
            }
            // print_r($notInsertMails);
            // print_r($success);
             die(json_encode(array('existMails'=>!empty($notInsertMails)?implode(' , ',$notInsertMails):'')));
        }   
    }
}
