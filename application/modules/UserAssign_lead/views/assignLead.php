	<style type="text/css">
		 .error {
      color: red;
   }
	</style>
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>LEAD DETAILS</p></li>
			</ul>
		</nav>
		<div class="inner-content clearfix">
			<div class="row lead-upload">
                <!-- <div class="col-md-6"> -->
                    <form method="post" id="importform_data" enctype="multipart/form-data">
                        <p><label>Select Excel File</label>
                        <input type="file" name="file" id="file" accept=".xls, .xlsx" required="" /></p>
                        <br />
                       

                        <!-- <div class="profile-details"> -->
						<!-- <div class="profile-photo"> -->
						   <label>Branch Manager:</label>
                           <select name="select_branchManager" id="select_branchManager" class="select_branchManager custom-select" required="">
                           <option value="" >Select Branch Manager</option>
                           <?php if(!empty($branchManagers)){
                               foreach($branchManagers  as  $branchManager){
                               	echo  '<option value="'.$branchManager['user_id'].'">'.$branchManager['user_title'].' '.ucfirst($branchManager['first_name']).'</option>';
                              }
                              
                              } ?>
                            </select>				
						<!-- </div> -->
						<!-- </div> -->
						<br/><br/>
						<input type="submit" name="import" value="Import" class="btn btn-info" id="excepload"/>
                    </form>
                <!-- </div> -->
				<!-- <div class="col-md-6"> -->

				<!-- </div>   -->
			</div>
		</div><!--end inner-content-->
	</div><!--end Page Content Holder -->
</div>
</body>
