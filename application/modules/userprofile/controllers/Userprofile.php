<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Userprofile extends Front_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('dashboard_model'));
        $this->load->library('Ajax_pagination');
        $this->perPage = 3;
    }
    public function index() {
            $data['page_title']     = "User Profile";
            $userData               = $this->session->userdata('user');
            $userId                 = $userData['user_id'];
            $data['user']           = $this->dashboard_model->get_all('users', 'user_id', $userId);
            $this->template->load('frontTemplate', 'userprofile', $data);
    }
    /**update user profile information(02-01-2019)**/
    public function userProfileLead() {
        // $files                  = $_FILES;
        $data['page_title']     = "User Profile";
        $request                = $this->input->post();
        $userData               = $this->session->userdata('user');
        $userId                 =   $userData['user_id'];
        $old_pass                =  $request['oldpass'];
        $new_pass                =  $request['newpass'];
        // $old_image              = $request['old_image'];
        $data  = $this->dashboard_model->checkPasswordExist($old_pass,$userId);
        if (!empty($data)) {
            $saveuser           = $this->dashboard_model->profileInfo($new_pass,$userId);
           // echo "<pre>";print_r($saveuser);exit();
            if ($saveuser) {
                $status         ="success";
            } else {
                $status         ="Errors";
            } 
        }else{
            $status             ="error";
        }
       echo json_encode($status);
    } 
}
