<html>
<head>
<body>
	<?php //echo "<pre>";print_r($user);die; ?>
<!-- Page Content Holder -->
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>MY PROFILE</p></li>
			</ul>
		</nav>
		<div class="inner-content clearfix">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
				<form method="post" autocomplete="false" action="" id="userpass" >
					<div class="profile-details">				
						<div class="profile-photo">
							<a href="#">
							<!-- <img src="assets/images/profile-img.png" class="img-fluid profile-img" alt="profile-img" />
							<img src="assets/images/cam.png" class="img-fluid cam-img" alt="cam" /> -->	
							<img  style="width: 150px; height: 150px; border-radius: 50%; border: solid #d8d8d8;" id="user_image" src="<?php echo base_url().'assets/images/user_images/'.$user[0]['profile_picture']; ?>" src="#" onerror="this.onerror=null; this.src='<?php echo base_url('assets/images/profile-img.png') ?>';" height="100" width="100" />                           
                            <img style="position: relative; top: 60px; right: 45px; cursor: pointer;"onclick="$('#user_images').click()" src="assets/images/cam.png" class="img-fluid" alt="cam-img" />
							</a>						
						</div>
						<input type="file" onchange="readURL(this);" name="user_image" id="user_images" style="display: none;"  accept="image/*">
						<input type="hidden" id="old_image" name="old_image" value="<?php echo $user[0]['profile_picture']; ?>">
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<div class="detail-form profile-form">
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-10">
						<h5>Personal Information</h5>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-3">
						<h6>First Name</h6>
					</div>
					<div class="col-lg-1">:</div>
					<div class="col-lg-6">
						<p><?php echo $user[0]['first_name'];?></p>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-3">
						<h6>Last Name</h6>
					</div>
					<div class="col-lg-1">:</div>
					<div class="col-lg-6">
						<p><?php echo $user[0]['last_name'];?></p>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-3">
						<h6>Phone Number</h6>
					</div>
					<div class="col-lg-1">:</div>
					<div class="col-lg-6">
						<p><?php echo $user[0]['phone'];?></p>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-3">
						<h6>Email Address</h6>
					</div>
					<div class="col-lg-1">:</div>
					<div class="col-lg-6">
						<p><?php echo $user[0]['email'];?></p>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-10">
						<button class="btn chng-pwd">CHANGE PASSWORD</button>
					</div>
					<div class="col-lg-1"></div>
				</div>
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-10">
							<h5>Change Password</h5>
						</div>
						<div class="col-lg-1"></div>
					</div>
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Old Password</label>
								<input name="oldpass" id="oldpass" type="text" class="form-control" placeholder="" >
							</div>
						</div>					
						<div class="col-lg-1"></div>
					</div>
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>New Password</label>
								<input name="newpass"  id="newpass" type="text" class="form-control" placeholder="" >
							</div>
						</div>					
						<div class="col-lg-1"></div>
					</div>
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Confirm Password</label>
								<input name="conpass" id="conpass"  type="password" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-1"></div>
					</div>			
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-4">
							<button type="submit" name="submit" id="submit" class="btn">SAVE</button>
							<button class="btn">CANCEL</button>
						</div>
						<div class="col-lg-1"></div>
					</div>
				</form>
			</div>
		</div><!--end inner-content-->
	</div><!--end Page Content Holder -->
</div>
<script>
	 /**image priview**/
	 function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#user_image')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

	</script>