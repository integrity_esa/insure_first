<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends Front_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('dashboard_model','user_login_model'));
        $this->load->library('Ajax_pagination');
        $this->perPage = 3;
    }
    public function index() {
        if ($this->session->userdata('user')) {

            $data['page_title'] = "Dashboard";
            $userData = $this->session->userdata('user');
            $data['user_id'] = $userData['user_id'];
            $data['user_name'] = $userData['first_name'];    
            $data['get_day_leads'] = $this->rest->post('api/dashboard/all_assign_lead',$data);                
            $this->template->load('frontTemplate', 'dashboard/dashboard', $data);

        } else {
            redirect('/');
        }
    }
     //get the events
     public function get_events()
    {
     $userData = $this->session->userdata('user');
     $user_id = $userData['user_id'];
     // Our Start and End Dates
     $start = $this->input->get("renewal_date");
     $end = $this->input->get("appointment_date");
     
     $startdt = new DateTime('now'); // setup a local datetime
     $startdt->setTimestamp($start); // Set the date based on timestamp
     $start_format = $startdt->format('Y-m-d H:i:s');
     //print_r($start_format);exit();
     $enddt = new DateTime('now'); // setup a local datetime
     $enddt->setTimestamp($end); // Set the date based on timestamp
     $end_format = $enddt->format('Y-m-d H:i:s');
     // echo "<pre>";print_r($start_format);exit();
     $events = $this->dashboard_model->get_events($user_id,$start_format, $end_format);
      //echo "<pre>";print_r($events->result());exit();
     $data_events = array();

     foreach($events->result() as $r) {

         $data_events[] = array(
             "id" => $r->id,
             "title" => ucfirst($r->name),
             "name" => ucfirst($r->name),
             "policy_name" => ucfirst($r->policy_name),
             "address1" => ucfirst($r->address_line1),
             "address2" => ucfirst($r->address_line2),
             "email" => $r->email,
             "phone" => $r->phone,
             "start" => $r->end,
             "appointment" => $r->end
         );
     }

     echo json_encode(array("events" => $data_events));
     exit();
    }
}
