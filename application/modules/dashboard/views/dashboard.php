<!-- Page Content Holder --> 
<div id="content">
    <nav class="navbar navbar-default"> 
        <ul class=" list-inline navbar-header">
            <li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button></li>
            <li class="list-inline-item"><p>DASHBOARD</p></li>
        </ul>
    </nav>
    <div class="inner-content clearfix">
        <!--loyalty board-->
        <div class="loyalty-board">
            <div class="row">
                <div class="col-md-5">
                    <h3>Hello <span class="username"><?php echo strtoupper($user_name); ?></span><span><img src="<?php echo base_url() ?>assets/images/smiley.png" class="img-fluid" alt="smiley" /></span></h3>
                    <p>Welcome to InsureFirst!</p>
                </div>
                <div class="col-md-5 text-center d-flex align-self-center">
                    <p class="loyal">Your Loyalty Points : <span>400</span></p>
                </div>
                <div class="col-md-2 align-self-center text-center">
                    <img src="<?php echo base_url() ?>assets/images/heart.png" class="img-fluid" alt="heart" />
                </div>
            </div>
        </div><!--end loyalty board-->
       <!--progress-bar-->
        <div class="progress-strip">
                <p>You have reached</p>
                <div class="progress align-self-center d-flex">                 
                <div class="progress-bar progress-amber" id="tooltip-demo" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-html="true" title='
                  <span style="color:red">12L</span> to <span style="color:red">13L</span>  in July'>
                </div>
                <p class="align-self-center">13 days Remaining</p>
        </div>
        </div>
        
        <!--end progress-bar-->
        <!--dashboard-->
        <div class="dashboard">       
            <a href="#calendar" class="add-round flip collapsed" id="calender_view" data-toggle="collapse" aria-expanded="false"> 
				<i class="fa fa-minus" aria-hidden="true"></i>
				<i class="fa fa-plus" aria-hidden="true"></i>
            </a>
            <h3 class="text-uppercase">Day Calendar</h3>  
            <!-- <div id="plus-minus-toggle collapsed"></div> -->
            <div id="calendar" class="panel collapse"></div>
            <!-- <div class="panel">
            <p>This is simple jQuery Show/Hide Example by Crunchify...</p>
            <p>Click below Show/Hide again to Toggle visibility...</p>
            </div> -->
            <!--dashboard-content-->
            <?php
            $today =date('Y-m-d');
           // echo "<pre>";print_r($get_day_leads->get_assign_leads);exit();
            if(!empty($get_day_leads->get_assign_leads)){
            foreach ($get_day_leads->get_assign_leads as $leads) {
                 //echo "<pre>";print_r($leads);
                ?>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <h5 class="lead-name"><?php echo ucfirst($leads->first_name); ?></h5>
                        <?php if(date('Y-m-d',strtotime($leads->appointment_date)) == $today){ ?>
                         <p class="entry-time"><?php echo date('d M Y',strtotime($leads->appointment_date)); ?> <span></span></p> 
                        <p>Brain Storming with tech team</p>
                        
                        <a href="#" style="text-decoration: none;"><?php echo ucfirst($leads->category_name); ?></a>                   
                        </div>
                    <div class="col-md-2 text-center align-self-center">
                        <img src="<?php echo base_url() ?>assets/images/leaf.png" class="img-fluid" alt="leaf-img" />
                    </div>
                    <div class="col-md-4 text-center align-self-center third-column">
                        <ul class="list-unstyled">
                            <li><a href="#" class="personal"><?php echo ucfirst($leads->leadtype); ?></a></li>
                        </ul>
                    </div>
                    <div class="col-md-1"></div>
                    <?php } ?> 
                </div>
            <!--dashboard-content-->
            <?php  } } ?>            
        </div>
        <!--dashboard footer-->
            <div class="dashboard-footer">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-4 align-self-center">
                        <p>Loyalty program status <span>40 LP</span> Available</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <button class="btn btn-light"><img src="<?php echo base_url() ?>assets/images/location-arrow.png" class="img-fluid" alt="location-arrow" />Click to CONVERT</button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <!--end dashboard footer-->
    </div>
</div>
<!-- view events details start -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Appointment Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
      <?php //echo form_open(site_url("calendar/add_event"), array("class" => "form-horizontal")) ?>
      
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Name</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="name" id="name" value="" readonly>
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Policy Name</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="policy_name" id="policy_name" value="" readonly>
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Address 1</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="address1" id="address1" value="" readonly>
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Address 2</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="address2" id="address2" value="" readonly>
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Email</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="email" id="email" value="" readonly>
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Phone</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="phone" id="phone" value="" readonly>
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Appointment Date</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="appointment" id="appointment" value="" readonly>
                </div>
        </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?php //echo form_close() ?>
      </div>
    </div>
  </div>
</div>
<!-- view events details end