<?php 
/* Gets lead information */
$_details = $details->row();
//echo "<pre>"; print_r($_details); die();
?>
<!-- Page Content Holder -->
<div id="content">
    <nav class="navbar navbar-default">
        <ul class=" list-inline navbar-header">
            <li class="list-inline-item">
                <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </li>
            <li class="list-inline-item">
                <p>LEAD DETAILS</p>
            </li>
        </ul>
    </nav>
    <div class="inner-content clearfix">
        <!--lead-details-->
        <div class="lead-details">
            <div class="row">
                <div class="col-lg-6 first-half">
                    <h5>LEAD SOURCE : <span>
                    	<?php 
                    		if (!empty($_details->lead_type)) {
                    			echo strtoupper(show_lead_sourse($_details->lead_type)); 
                    		}
                    	?></span></h5>
                    <?php if (!empty($_details->lead_img)) { ?>
                        <img src="<?php echo base_url("assets/images/user_images/{$_details->lead_img}") ?>" class="img-fluid" alt="profile-img" />
                    <?php } else { ?>
                        <img src="<?php echo base_url("assets/images/profile-img.png") ?>" class="img-fluid" alt="profile-img" />
                    <?php } ?>
                    <div class="row details-row">
                        <div class="col-lg-3 col-md-3"></div>
                        <div class="col-lg-2 col-4 col-md-2">
                            <h5>Name</h5>
                        </div>
                        <div class="col-lg-2 col-1 col-md-1">:</div>
                        <div class="col-lg-5 col-6 col-md-6">
                            <p><?php 
                            	if (!empty($_details->first_name) && !empty($_details->last_name)) {
                            		echo ucfirst($_details->first_name . ' ' . $_details->last_name);
                            	} 
                        	?></p>
                        </div>
                    </div>
                    <div class="row details-row">
                        <div class="col-lg-3 col-md-3"></div>
                        <div class="col-lg-2 col-4 col-md-2">
                            <h5>DOB</h5>
                        </div>
                        <div class="col-lg-2 col-1 col-md-1">:</div>
                        <div class="col-lg-5 col-6 col-md-6">
                            <p><?php 
                            	if (!empty($_details->dob)) {
                            		echo date_slash_formater($_details->dob);
                            	}
                            ?></p>
                        </div>
                    </div>
                    <a href="<?php echo base_url('quotation'); ?>">Generate Quote</a>
                </div>
                <div class="col-lg-6 second-half">
                    <h5>POLICY</h5>
                    <img src="<?php echo base_url("assets/images/leaf.png") ?>" class="img-fluid" alt="profile-img" />
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href=""><img src="<?php echo base_url("assets/images/phone.png") ?>" class="img-fluid" alt="phone" /></a>
                        </li>
                        <li class="list-inline-item">
                            <a data-toggle="modal" data-target="#myModal2"><img src="<?php echo base_url("assets/images/audio.png") ?>" class="img-fluid" alt="audio" /></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="" data-toggle="modal" data-target="#myModal3"><img src="<?php echo base_url("assets/images/video-cam.png") ?>" class="img-fluid" alt="video-cam" /></a>
                        </li>
                        <li class="list-inline-item">
                            <a href=""><img src="<?php echo base_url("assets/images/whatsapp.png") ?>" class="img-fluid" alt="whatsapp" /></a>
                        </li>
                    </ul>
                    <a href="#" class="rec-history">View Recorded History</a>
                </div>
            </div>
        </div>
        <!--end lead-details-->
        <!--lead-history-->
        <div class="lead-history">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-3 history-h5">
                    <h5>LEAD HISTORY</h5>
                </div>
            </div>
            <div class="timeline">
            	<?php 
            	$lp = 0;
            	while ($activity = current($history)) { 
            		$idx = key($history);
            		$nxt = next($history);
            		if ($nxt['act_lead_status_id'] != $activity['act_lead_status_id']) {
        				if ($lp%2 < 1) {
            				$align = 'left';
            			} else {
            				$align = 'right';
            			}
            			$lp++;
            		?>
		                <div class="container1 <?php echo $align ?>">
		                    <div class="content">
		                        <h5><?php echo $activity['status_name'] ?></h5>
		                        <p><?php echo datetime_short_formater($activity['act_status_date']) ?></p>
		                    </div>
		                </div>
	            	<?php } else {
	            		continue;
	            	}
        		} ?>
            </div>
            <?php /* Form Starts */ ?>
            	<?php echo form_open_multipart('lead/history', array(
                        'id' => 'lead_status_form'
                    ), 
                    array(
                		'lead_id' => $lead_id
                	)) ?>
            	<!-- Form -->
		            <div class="row">
		                <div class="col-lg-1"></div>
		                <div class="col-lg-4 form-inline">
		                    <label>STATUS</label>
		                    <?php 
                                if ($_details->lead_status != '1') {
                                    unset($lead_status_options['1']);
                                }
		                    	echo form_dropdown('lead_status', $lead_status_options, $_details->lead_status, array(
		                    		'class' => 'form-control custom-select'
		                    	));
		                    ?>
		                </div>
		                <div class="col-lg-1"></div>
		                <div class="col-lg-4">
		                    <input name="datetime" type="text" id="datetimepicker1" class="form-control" placeholder="<?php echo date('d-m-Y h:i A') ?>"/>
		                </div>
		            </div>
		            <div class="row lead-notes">
		                <div class="col-lg-10 mx-auto">
		                    <div class="row">
		                        <div class="col-lg-9">
		                            <textarea name="comment" rows="10" id="comment" placeholder="Add Notes"></textarea>
		                        </div>
		                        <div class="col-lg-3 align-self-center text-center">
		                            <p>OR</p>
		                            <!-- <a href="#" class="upload-doc">UPLOAD DOCUMENT</a> -->
		                            <div class="upload-doc" onclick="return $('#hidden_upload_doc').trigger('click');">
	                            		UPLOAD DOCUMENT
		                            </div>
		                            <?php echo form_upload('doc', '', array(
		                            	'style' => 'display: none',
		                            	'id' => 'hidden_upload_doc',
		                            	'onchange' => "$('#doc-name').text(this.value)"
		                            )) ?>
		                            <p id="doc-name" style="width: 100%;overflow: hidden;"></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="lead-history-btn text-center">
		                <button class="btn" type="submit">SAVE CHANGES</button>
		                <button class="btn">CANCEL</button>
		            </div>
            	</form>
            <?php /* Form Ends */ ?>
        </div>
        <!--end lead-history-->
    </div>
    <!--end inner-content-->
</div>
<!--end Page Content Holder -->
<!-- The audio Modal -->
<div class="modal add-media" id="myModal2">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal body -->
            <div class="modal-body text-center">
                <?php echo form_open_multipart('lead/recorder_history_audio', array(
                    'id' => 'recorder_history_audio'
                ),  
                array(
                    'lead_id' => $lead_id
                )); ?>
                    <label for="audfile-upload" class="custom-file-upload1">Add</label>
                    <input id="audfile-upload" name="audiup[]" type="file" multiple="multiple" onchange="return fileMultiPreview(this, 'aud', '#audPrew');" accept="audio/mp3"/>
                    <p>Select Audio Files...</p>
                    <div class="audio-files">
                        <div id="audPrew" class="row">

                        </div>
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><!-- end The audio Modal -->
<!-- The video Modal -->
<div class="modal add-media" id="myModal3">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal body -->
            <div class="modal-body text-center">
                <?php echo form_open_multipart('lead/recorder_history_vedio', array(
                    'id' => 'recorder_history_vedio'
                ),  
                array(
                    'lead_id' => $lead_id
                )); ?>
                    <label for="vedfile-upload" class="custom-file-upload1">Add</label>
                    <input id="vedfile-upload" name="vedi[]" type="file" multiple="multiple" onchange="return fileMultiPreview(this, 'ved', '#vidPrew');" accept="video/mp4"/>
                    <p>Select Video Files...</p>
                    <div class="video-files">
                        <div id="vidPrew" class="row">

                        </div>
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div> 
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
</div><!-- end The video Modal -->