<!--tab-inside-content-->
<?php 
// foreach($get_all_leads->get_all_leads as $all){ 
foreach($posts as $all){ 
	?>
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-4">
	<h5 class="lead-name">
		<a href="<?php echo base_url("lead/details/{$all['lead_id']}"); ?>">
			<?php echo ucfirst($all['first_name']); ?>
		</a>
	</h5>
	<p class="entry-time"><?php echo date('d M Y',strtotime($all['appointment_date']));?> <span></span></p>
	<p>Brain Storming with tech team</p>
	<a href="#"><?php echo ucfirst($all['category_name']) ; ?></a>
</div>
<div class="col-md-2 text-center align-self-center">
	<img src="<?php echo base_url() ?>assets/images/leaf.png" class="img-fluid" alt="leaf-img" />
</div>
<div class="col-md-4 text-center align-self-center third-column">
	<ul class="list-unstyled">
		<li><a href="#" class="personal">Personal</a></li>
	</ul>
</div>
<div class="col-md-1"></div>
</div>  <!--end-tab-inside-content-->
<?php } ?>
<?php echo $this->ajax_pagination->create_links(); ?>