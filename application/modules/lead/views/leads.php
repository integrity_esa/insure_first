<!-- Karthik -->
<!-- Page Content Holder -->
<?php //print_r($filter['lead_type']); die(); ?>
<div id="content">
	<nav class="navbar navbar-default">	
		<ul class=" list-inline navbar-header">
			<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
				<i class="fa fa-bars" aria-hidden="true"></i>
			</button></li>
			<li class="list-inline-item"><h3>DASHBOARD</h3></li>
		</ul>
	</nav>
	<div class="inner-content clearfix">
		<div class="set-icons">
			<ul class="list-inline">
				<li class="list-inline-item"><a href="#"><img src="<?php echo base_url() ?>assets/images/location.png" class="img-fluid" /></a></li>
				<li class="list-inline-item"><a href="#" data-toggle="modal" data-target="#myModal1"><img src="<?php echo base_url() ?>assets/images/settings-icon.png" class="img-fluid" /></a></li>
				<li class="list-inline-item"><a href="<?php echo base_url() ?>lead/addNew"><img src="<?php echo base_url() ?>assets/images/plus.png" class="img-fluid" /></a></li>
			</ul>
		</div>
		<div class="leads">
			<!-- Nav tabs -->
		  <ul class="nav nav-tabs">
			<li class="nav-item">
			  <a class="nav-link active" data-toggle="tab" href="#home">TODAY'S LEADS</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" data-toggle="tab" href="#menu1">ALL LEADS</a>
			</li>
		  </ul><!-- end Nav tabs -->
		  <!-- Tab panes -->
		  <div class="tab-content">
			<div id="home" class="container tab-pane active"><br>
			  <!--tab-inside-content-->
			  <?php
			  $today_date =date('Y-m-d');
			 // echo "<pre>";print_r($get_today_leads);exit();
			  if(!empty($get_today_leads->day_leads)) {
			  foreach($get_today_leads->day_leads as $today){
			  	//print_r($today); die();
			  		?> 
			  <div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<!-- lead show start-->
					<h5 class="lead-name">
						<a href="<?php echo base_url("lead/details/{$today->lead_id}"); ?>">
							<?php echo ucfirst($today->first_name); ?>		
						</a>
					</h5>
                    <?php if(date('Y-m-d',strtotime($today->created_at)) == $today_date){ ?>
                     <p class="entry-time"><?php echo date('d M Y',strtotime($today->created_at)); ?> <span></span></p> 
                    <p>Brain Storming with tech team</p>
                    <a href="#" style="text-decoration:none"><?php echo ucfirst($today->category_name); ?></a>
                    <?php } ?>
					<!-- lead show end-->
				</div>
				<div class="col-md-2 text-center align-self-center">
					<img src="<?php echo base_url() ?>assets/images/leaf.png" class="img-fluid" alt="leaf-img" />
				</div>
				<div class="col-md-4 text-center align-self-center third-column">
					<ul class="list-unstyled">
						<li><a href="#" class="personal"><?php echo ucfirst($today->leadtype);?></a></li>
					</ul>
				</div>
				<div class="col-md-1"></div>
			  </div>  <!-- end-tab-inside-content -->
			  <?php } } else { ?>
			  		<p class="alert text-left">No data available</p>
			  <?php } ?> 
			
			</div>
			<div id="menu1" class="container tab-pane fade" ><br>
			  <!--tab-inside-content-->
			  <div class="post-list" id="postList">
			  <?php 
			  $today_date =date('Y-m-d');
			  if(!empty($get_all_leads)) {
			  	 // echo "<pre>";print_r($get_all_leads);exit();
			  foreach($get_all_leads as $all){
			  	//echo "<pre>";print_r($all);
			  	?>
			  <div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<!-- lead show start-->
					<h5 class="lead-name">
						<a href="<?php echo base_url("lead/details/{$all['lead_id']}"); ?>">
							<?php echo ucfirst($all['first_name']); ?>
						</a>
					</h5>
                    <?php if(date('Y-m-d',strtotime($all['appointment_date']))){ ?>
                     <p class="entry-time"><?php echo date('d M Y',strtotime($all['appointment_date'])); ?> <span></span></p> 
                    <p>Brain Storming with tech team</p>
                    <a href="#"><?php echo ucfirst($all['category_name']) ; ?></a>
                    <?php } 
                    ?>
					<!-- lead show end-->

				<!-- 	<h5 class="lead-name"><?php //echo ucfirst($all['first_name']); ?></h5>
					<p class="entry-time"><?php //echo date('d M Y',strtotime($all['appointment_date']));?> <span></span></p>
					<p>Brain Storming with tech team</p>
					<a href="#">Priority</a> -->
				</div>
				<div class="col-md-2 text-center align-self-center">
					<img src="<?php echo base_url() ?>assets/images/leaf.png" class="img-fluid" alt="leaf-img" />
				</div>
				<div class="col-md-4 text-center align-self-center third-column">
					<ul class="list-unstyled">
					<li><a href="#" class="personal"><?php echo $all['leadname'];?></a></li>
					</ul>
				</div>
				<div class="col-md-1"></div>
			  </div>  <!--end-tab-inside-content-->
			 
			<?php }  ?>
			<?php echo $this->ajax_pagination->create_links(); 
			} else { ?>
		  		<p class="alert text-left">No data available</p>
		    <?php } ?> 
			  </div>

			</div>
		  </div><!--end Tab panes -->
		</div><!-- end leads -->
	</div><!--end inner-content-->
</div><!--end Page Content Holder -->

<!-- The Modal -->
<div class="modal" id="myModal1">
	<div class="modal-dialog">
		<div class="modal-content">        
			<!-- Modal body -->
			<form id="filter-query" method="get">
				<div class="modal-body">
					<h2>Filter Options</h2>
					<div class="col-lg-12">
						<h3>Lead Name</h3>
						<div class="form-group">
							<img src="<?php echo base_url('assets/images/search.png'); ?>" alt="user-icon">
							<input type="text" name="leadname" class="form-control" placeholder="Search" value="<?php echo (!empty($filter['leadname'])) ? $filter['leadname']: '' ?>">
						</div>
					</div>
					<div class="col-lg-12">
						<?php if (!empty($lead_category)) { ?>
							<h3>Lead Category</h3>
							<?php foreach ($lead_category as $key => $category) { 
								$categ_checked = '';
								if (!empty($filter['category']) && in_array($key, $filter['category'])) {
									$categ_checked = 'checked="checked"';
								}
								?>
								<div class="form-check-inline">
									<label class="filter-check"><?php echo $category ?>
										<input type="checkbox" name="category[]" value="<?php echo $key; ?>" <?php echo $categ_checked; ?>>
										<span class="checkmark"></span>
									</label>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<div class="col-lg-12">
						<?php if (!empty($lead_source)) { ?>
							<h3>Lead Type</h3>
							<?php foreach ($lead_source as $key => $source) {  
								$source_checked = '';
								if (!empty($filter['lead_type']) && $key == $filter['lead_type']) {
									$source_checked = 'checked="checked"';
								} ?>
								<div class="form-check-inline">
									<label class="filter-radio">
										<?php echo $source; ?><input type="radio" <?php echo $source_checked; ?> name="lead_type" value="<?php echo $key; ?>">
										<span class="checkmark"></span>
									</label>
								</div>
							<?php } ?>
						<?php } ?>
					</div>	
					<div class="col-lg-12">
						<?php if (!empty($lead_status)) { ?>
							<h3>Lead Status</h3>
							<?php foreach ($lead_status as $key => $status) { 
								$status_checked = '';
								if (!empty($filter['lead_status']) && in_array($key, $filter['lead_status'])) {
									$status_checked = 'checked="checked"';
								} ?>
								<div class="form-check-inline">
									<label class="filter-check"><?php echo $status; ?>
									<input type="checkbox" name="lead_status[]" value="<?php echo $key; ?>" <?php echo $status_checked; ?>>
										<span class="checkmark"></span>
									</label>
								</div>
							<?php } ?>
						<?php } ?>
					</div>			
				</div>        
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="submit" class="btn btn-link">OK</button>
					<button type="button" class="btn btn-link" data-dismiss="modal">CANCEL</button>
				</div>
			</form>      
		</div>
	</div>
</div>

<script>
function searchFilter(page_num) {
page_num = page_num?page_num:0;
// var keywords = $('#keywords').val();
// var sortBy = $('#sortBy').val();
$.ajax({
    type: 'POST',
    url: '<?php echo base_url(); ?>lead/ajaxPaginationData/'+page_num,
    data:'page='+page_num,
    beforeSend: function () {
        $('.loading').show();
    },
    success: function (html) {
        $('#postList').html(html);
        $('.loading').fadeOut("slow");
    }
});
}
</script>