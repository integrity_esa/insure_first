	<style type="text/css">
		 .error {
      color: red;
   }
	</style>
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>LEAD DETAILS</p></li>
			</ul>
		</nav>
		<form method="post" id="edituser" enctype="multipart/form-data" autocomplete="off">
		<div class="inner-content clearfix">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="profile-details">
						<div class="profile-photo">
							<a href="#">
							<img style="width: 150px; height: 150px; border-radius: 50%;"  id="user_image" src="<?php echo base_url() ?>assets/images/profile-img.png" src="#" height="100" width="100" />
                            <img style="position: relative; top: 60px; right: 45px; cursor: pointer;" onclick="$('#user_images').click()" src="<?php echo base_url() ?>assets/images/cam.png" class="img-fluid" alt="cam-img" />
                            <input type="file" onchange="readURL(this);" name="user_image" id="user_images" style="display: none;"  accept="image/*">
							<!-- <img src="<?php echo base_url() ?>assets/images/profile-img.png" class="img-fluid profile-img" alt="profile-img" />
							<img src="<?php echo base_url() ?>assets/images/cam.png" class="img-fluid cam-img" alt="cam" /> -->
							</a>						
						</div>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<div class="detail-form">
				<div class="row">
					<div class="col-lg-1"></div>
							<!-- <span class="mandatory" style="color:red;"></span> -->
					<div class="col-lg-5">
						<div class="form-group">
						  <label>Title</label>
						  <select name="title" id="title" class="form-control ">
						  	<option value="">-- Select --</option>
                            <option value="1">Mr.</option>
                            <option value="2">Ms.</option>
                            <option value="3">Mrs.</option>
						  </select>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>First Name *</label>
							<input type="text" name="fname" id="fname" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Last Name</label>
							<input type="text" name="lname" id="lname" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Aadhar Number</label>
							<input type="text" name="aadharnum" 
							onkeypress="return isNumberKey(event)" maxlength="16" id="aadharnum" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Address 1</label>
							<input type="text" name="address1" id="address1" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Address 2</label>
							<input type="text" name="address2" id="address2" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>					
					<div class="col-lg-5">
						<div class="form-group">
						  <label>Country</label>
						  <select name="country" id="country" oninvalid="this.setCustomValidity('Select Your Country Here')" oninput="this.setCustomValidity('')"
						   class="form-control custom-select">
							<option value="">-- Select Country --</option>
							<?php
                            foreach ($getCountry as $country) {
                                echo '<option value="' . $country['country_id'] . '">' . $country['country_name'] . '</option>';
                            }
                            ?>
						  </select>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
						  <label>State</label>
						  <select name="state" id="state" oninvalid="this.setCustomValidity('Select Your State Here')" oninput="this.setCustomValidity('')"
						   class="form-control custom-select">
							<option value="">-- Select State --</option>
						  </select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
						  <label>City</label>
						  <select name="city" id="city" oninvalid="this.setCustomValidity('Select Your city Here')" oninput="this.setCustomValidity('')"class="form-control custom-select">
							<option value="">-- Select City --</option>
						  </select>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Pincode</label>
							<input type="text" maxlength="06" onkeypress="return isNumberKey(event)" class="form-control" name="pincode" id="pincode" placeholder="">
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Phone Number</label>
							<input type="text" name="phone" id="phone" 
							onkeypress="return isNumberKey(event)" maxlength="10" class="form-control" placeholder="">
							<span class="mandatory" style="color:red;"></span>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>E-Mail</label>
							<input type="text" name="email" onfocusout="checkEmail()" id="email" class="form-control" placeholder="">
							<span class="mandatory" style="color:red;"></span>
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>					
					<div class="col-lg-5">
						<div class="form-group">
							<label>Gender</label>
							 <select class="form-control" id="gender" name="gender" />
	                            <option value="">--Select--</option>
	                            <option value="1">Male</option>
	                            <option value="2">Female</option>
                            </select>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group cal-img">
							<label>Date Of Birth</label>
							<input type="text" name="dob" id="dob" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Company</label>
							<input type="text" name="company" id="company" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-5">
						 <div class="form-group">
						  <label>Marital Status</label>
						  <select name="m_status"  id="m_status" class="form-control custom-select">
						  	<option value="">-- Select --</option>
							<option value="1">Single</option>
							<option value="2">Married</option>
						  </select>
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row" id="lol" >
					<div class="col-lg-1"></div>					
					<div class="col-lg-5">
						<div class="form-group">
							<label>Spouse Name</label>
							<input type="text" name="spousename" id="spousename" class="form-control" placeholder="" >
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group cal-img">
							<label>Anniversary</label>
							<input type="text" name="anniversary" id="anniversary" class="form-control" id="datepicker2" placeholder="">
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<label>Policy Type</label>
						  <select name="policytype" id="policytype" class="form-control custom-select">
						    <option value="">--Select--</option>
							<?php
								foreach ($leadpolicyVal as $ptype) {
	                        	
	                            echo '<option value="' . $ptype['policy_id'] . '">' . $ptype['policy_name'] . '</option>';
	                            }
							?>							
						  </select>
						
					</div>
					<div class="col-lg-5">
						 <div class="form-group">
							<label>Garage Name</label>
							<input type="text"  name="gname" id="gname" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Campaign Name/Location of Campaign</label>
							<input type="text" name="campaign" id="campaign" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-lg-5">
						 <div class="form-group">
							<label>Lead Type</label>
							 <select name="leadtype" id="leadtype" class="form-control custom-select">
						  	<option value="">-- Select --</option>
							<?php
								foreach ($leadtypeVal as $type) {
                                echo '<option value="' . $type['lead_id'] . '">' . $type['leadname'] . '</option>';
                                }
							?>
							
						  </select>
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>					
					<div class="col-lg-5">
						 <div class="form-group">
						  <label>Lead Category</label>
						  <select name="leadcategory" id="leadcategory" class="form-control custom-select">
						  	<option value="">-- Select --</option>
							<option value="1">Hot Leads With Appointment</option>
							<option value="2">Hot</option>
							<option value="3">Warm</option>
							<option value="4">Cold</option>
						  </select>
						</div>
					</div>
					<div class="col-lg-5">
						 <div class="form-group">
						  <label>Lead Status</label>
						  <select name="leadstatus" id="leadstatus" class="form-control custom-select">
						  	<option value="">-- Select --</option>
						  	<?php
								foreach ($leadstatusVal as $statustype) {
                                echo '<option value="' . $statustype['status_id'] . '">' . $statustype['status_name'] . '</option>';
                                }
							?>
						  </select>
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>				
					<div class="col-lg-5">
						<div class="form-group cal-img">
							<label>Appointment Date</label>
							<input type="text" name="appointment" id="appointment" class="form-control" placeholder="">
						</div>
					</div>
						<div class="col-lg-5">
						<div class="form-group cal-img">
							<label>Renewal Date</label>
							<input type="text" name="renewal" id="renewal" class="form-control" id="datepicker1" placeholder="">
						</div>
					</div>
					<div class="col-lg-5">
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-4 mx-auto">
						<button class="btn btn-info photo-btn purchase-but">SAVE</button>
						<button class="btn btn-info photo-btn purchase-but">CANCEL</button>
					</div>
				</div>
			</div>
		</form>
		</div><!--end inner-content-->
	</div><!--end Page Content Holder -->
</div>
</body>
<script type="text/javascript">
/**check mail already exit(19-12-2018)***/
function checkEmail() {
        var email = $("#email").val();

        if (email != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>lead/checkEmail",
                data: "userEmail=" + $("#email").val(),
                type: "post",
                success: function (result) {
                    $("#email").parent().next(".validation").remove(); // remove it
                    if (result == 1) {
                        $("#email").focus();
                        $("#email").css('border-color', 'red');
                        $("#email").closest('.form-group').find('.mandatory').html('Email address already exists');
                        $("#email").val('');
                        return false;
                    } else {
                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (reg.test($("#email").val()) == false) {
                            $("#email").focus();
                            $("#email").css('border-color', 'red');
                            $("#email").closest('.form-group').find('.mandatory').html('');
                            $("#email").val('');
                        } else {
                            $("#email").cssselect('border-color', '');
                            $("#email").closest('.form-group').find('.mandatory').html('');                            
                        }
                    }
                }
            });
        }
    }


    /**check mail already exit(19-12-2018)***/
/*function checkPhone() {
        var phone = $("#phone").val();
        if (phone != "") {
            $.ajax({
                url: "<?php //echo base_url(); ?>lead/checkPhone",
                data: "userPhone=" + $("#phone").val(),
                type: "post",
                success: function (result) {
                    $("#phone").parent().next(".validation").remove(); // remove it
                    if (result == 1) {
                        $("#phone").focus();
                        $("#phone").css('border-color', 'red');
                        $("#phone").closest('.form-group').find('.mandatory').html('Phone Number already exists');
                        $("#phone").val('');
                        return false;
                    } else {
                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (reg.test($("#phone").val()) == false) {
                            $("#phone").focus();
                            $("#phone").css('border-color', 'red');
                            $("#phone").closest('.form-group').find('.mandatory').html('');
                            $("#phone").val('');
                        } else {
                            $("#phone").cssselect('border-color', '');
                            $("#phone").closest('.form-group').find('.mandatory').html('');                            
                        }
                    }
                }
            });
        }
    }*/
</script>
