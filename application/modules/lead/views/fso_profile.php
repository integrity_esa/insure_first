	<style type="text/css">
		 .error {
      color: red;
   }
	</style>
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>ASSIGN LEADS DETAILS</p></li>
			</ul>
		</nav>
		<?php //echo "<pre>";print_r($assign_lead_profile);?>
		<div class="inner-content clearfix">
		<div class="assign-lead">
			<!--first-row-->
		<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Lead Title</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['lead_title']);?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Full Name</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['first_name']).' '.ucfirst($assign_lead_profile['last_name']);?></p>
							</div>
						</div>
					</div>
				</div><!--end first-row-->

					<!--second-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Address 1</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['address_line1']);?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Address 2</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['address_line2']);?></p>
							</div>
						</div>
					</div>
				</div><!--end second-row-->

					<!--third-row-->
					<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Country</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['country_name']);?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>State</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['name']);?></p>
							</div>
						</div>
					</div>
				</div><!--end third-row-->

				<!--fourth-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>City</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['city_name']);?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Pincode</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['pincode'];?></p>
							</div>
						</div>
					</div>
				</div><!--end fourth-row-->

				<!--fifth-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Email</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['email'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Phone</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['phone'];?></p>
							</div>
						</div>
					</div>
				</div><!--end fifth-row-->

				<!--sixth-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>DOB</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['dob'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Gender</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php 
									if($assign_lead_profile['gender'] ==1){
										echo "Male";
									}else if($assign_lead_profile['gender'] ==2){
										echo "Female";
									}
								?></p>
							</div>
						</div>
					</div>
				</div><!--end sixth-row-->

				<!--seventh-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Company</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo ucfirst($assign_lead_profile['company']);?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Aadhar Id</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
							<p><?php echo $assign_lead_profile['aadhar_id'];?></p>
							</div>
						</div>
					</div>
				</div><!--end seventh-row-->

				<!--eight-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Marital Status</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
							<p><?php 
									if($assign_lead_profile['marital_status'] ==1){
										echo "UnMarried";
									}else if($assign_lead_profile['marital_status'] ==2){
										echo "Married";
									}
								?></p>
							</div>
						</div>
					</div>
					<?php if($assign_lead_profile['marital_status'] ==2){ ?>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Spouse Name</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
							<p><?php echo ucfirst($assign_lead_profile['spouse_name']);?></p>
							</div>
						</div>
					</div>
					<?php } ?>
				</div><!--end eight-row-->

				<!--ninth-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Anniversary</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['anniversary'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Lead Category</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
							<p><?php echo ucfirst($assign_lead_profile['category_name']);?></p>
							</div>
						</div>
					</div>
				</div><!--end ninth-row-->


				<!--tenth-row-->
				<div class="row">
									<div class="col-lg-6">
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<h5>Appointment Details</h5>
											</div>
											<div class="col-lg-1">:</div>
											<div class="col-lg-5">
												<p><?php echo $assign_lead_profile['appointment_date'];?></p>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<h5>Policy Type</h5>
											</div>
											<div class="col-lg-1">:</div>
											<div class="col-lg-5">
											<p><?php echo ucfirst($assign_lead_profile['policy_name']);?></p>
											</div>
										</div>
									</div>
				</div><!--end tenth-row-->

				<!--eleventh-row-->
				<div class="row">
									<div class="col-lg-6">
										<div class="row">
											<div class="col-lg-1"></div>
											<div class="col-lg-5">
												<h5>Lead Owner</h5>
											</div>
											<div class="col-lg-1">:</div>
											<div class="col-lg-5">
												<p><?php echo ucfirst($assign_lead_profile['lead_owner']);?></p>
											</div>
										</div>
									</div>
				</div><!--end eleventh-row-->

			<!--tweleth-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Location</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['campaign_location'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Campaign Name</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['campaign_name'];?></p>
							</div>
						</div>
					</div>
				</div><!--end tweleth-row-->

				<!--thirteeth-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Lead Status</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['status_name'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Unassigned Lead</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php 
									if($assign_lead_profile['reference_id'] ==0){
										echo "Not Assigned Lead";
									}else{
										echo "Assigned Lead";
									}
								?></p>
							</div>
						</div>
					</div>
				</div><!--end thirteeth-row-->

				<!--fourteeth-row-->
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Last updated lead status date</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['created_at'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-5">
								<h5>Campaign Owner</h5>
							</div>
							<div class="col-lg-1">:</div>
							<div class="col-lg-5">
								<p><?php echo $assign_lead_profile['campaign_owner'];?></p>
							</div>
						</div>
					</div>
				</div><!--end fourteeth-row-->
			</div>
		</div><!--end inner-content-->
	</div><!--end Page Content Holder -->
</div>
</body>
