<!-- Karthik -->
<!-- Page Content Holder -->
<div id="content">
	<nav class="navbar navbar-default">	
		<ul class=" list-inline navbar-header">
			<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
				<i class="fa fa-bars" aria-hidden="true"></i>
			</button></li>
			<li class="list-inline-item"><p>ASSIGN LEADS DATA</p></li>
		</ul>
	</nav>
	<div class="inner-content clearfix">
	<div class="row filter-select">
	<div class="form-group col-md-3">
	<select name="leadStatus" id="leadStatus" class="form-control custom-select" data-column="1">
       <option value="0" >--LEAD STATUS--</option>
       <?php if(!empty($lead_status)){
          foreach($lead_status  as  $leadStatus){
          	echo  '<option value="'.$leadStatus['status_id'].'">'.ucfirst($leadStatus['status_name']).'</option>';
          }
          
          } ?>
    </select>
	</div>		
	<div class="form-group col-md-3">
		<select name="leadCategory" id="leadCategory" class="form-control custom-select" data-column="3">
       <option value="0" >--LEAD CAEGORY--</option>
       <?php if(!empty($lead_category)){
          foreach($lead_category  as  $leadCategory){
          	echo  '<option value="'.$leadCategory['category_id'].'">'.ucfirst($leadCategory['category_name']).'</option>';
          }
          
          } ?>
        </select>		
	</div>
	<?php 
	if(!empty($this->session->userdata('user'))){
		$userdata = $this->session->userdata('user');
		$role_id = $userdata['role_id'];
	}
	?>
	<?php if(!empty($role_id)){
	if($role_id==1) {
	?>
	<div class="form-group col-md-3">
		<select name="branch_manager" id="branch_manager" class="form-control custom-select" data-column="3">
       <option value="0" >--BRANCH MANAGER--</option>
        <?php 
        // echo "<pre>";print_r($branchManagers);exit();
        if(!empty($branchManagers)){
           foreach($branchManagers  as  $branchManager){
           	echo  '<option value="'.$branchManager['user_id'].'">'.ucfirst($branchManager['first_name']).'</option>';
          }
          } ?>
        </select>		
	</div>
	<?php } } ?>
	<?php if(!empty($role_id)){
	if($role_id==2) {
	?>
	<div class="col-md-3 form-group">
		<select name="fso_filter" id="fso_filter" class="form-control custom-select">
        <option value="0" >--SELECT FSO--</option>
        <?php 
         //echo "<pre>";print_r($fso_list);exit();
       if(!empty($fso_list)){
          foreach($fso_list  as  $fso_list_data){
          	echo  '<option value="'.$fso_list_data['user_id'].'">'.ucfirst($fso_list_data['first_name']).'</option>';
         }
         } ?>
        </select>		
	</div>
	<?php } } ?>
	</div>
		<div class="row">
        <div class="col-lg-12">      
            <div id="render-list-of-order">
            </div>
        </div>			
   		</div>
	</div><!--end inner-content-->
</div><!--end Page Content Holder -->