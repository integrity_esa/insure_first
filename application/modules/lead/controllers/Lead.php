<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lead extends Front_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('lead_model','dashboard_model','location_model'));
        $this->load->library(array('Ajax_pagination', 'File_handling'));
        $this->perPage = 3;
    }
    public function index(){
        $data['title'] ='List data';
        $data['lead_status']    = $this->lead_model->lead_status();
        $data['lead_category']  = $this->lead_model->lead_category();
        //branch head filter for branch manager 
       $data['distance'] = $this->location_model->lead_dist();
         $kms = $data['distance']['kilometer'];
         $lmt = $data['distance']['item_number'];
         $login_user = $this->session->userdata('user');
        // echo "<pre>";print_r($login_user);exit();
         $user_id = $login_user['user_id'];
         $role_id = $login_user['role_id'];
        $data['branchManagers'] = $this->location_model->branchManagerlist($kms,$user_id,$role_id);
        $data['fso_list'] = $this->location_model->branch_manager_fso_list($kms,$user_id,$role_id);
        //echo "<pre>";print_r($data['branchManagers']);exit();
        //$this->template->load('frontTemplate', 'assignLead', $data); 
        //
        $this->template->load('frontTemplate', 'lead/filterleads',$data);
    }
    public function addNew() {
        $request = $this->input->post();
        $files = $_FILES;
        $data['page_title'] = "Lead Form";
        $userData = $this->session->userdata('user');
        $userId = $userData['user_id'];
        if (!empty($this->input->post())) {
            // $saveuser       = $this->lead_model->insert_user($request,$userId,$files);
            $saveuser       = $this->lead_model->add_Lead($request,$userId,$files);
            if (!empty($saveuser)) {
                $status="success";
                $this->session->set_flashdata('message', 'Updated Successfully !!!');
            } else {
                $status="Error";
                $this->session->set_flashdata('error', 'Error Occurred in Insert !!!');
            }
            die(json_encode(array('status'=>$status)));
        }
        $data['getCountry'] = $this->lead_model->get_all_order('country', '', '', 'country_name', 'ASC');
        $data['leadtypeVal']    = $this->lead_model->get_all_order('lead_type', '', '', 'leadname', 'ASC');
        $data['leadcategoryVal']= $this->lead_model->get_all_order('lead_category', '', '', 'category_name', 'ASC');
        $data['leadpolicyVal']  = $this->lead_model->get_all_order('policy_type', '', '', 'policy_name', 'ASC');
        $data['leadstatusVal']  = $this->lead_model->get_all_order('lead_status', '', '', 'status_name', 'ASC');              
        $data['user_name'] = $userData['first_name']; 

        $this->template->load('frontTemplate', 'leads_form', $data);
    }
    
/**get state value based on contury**/  
     public function getAllState() {
        $request = $this->input->post();
        $states = $this->lead_model->statevalue($request);

        $stateHtml='<option value="">-- Select State --</option>';

        if(!empty($states)){
            foreach($states as $state){
                $stateHtml.='<option value="'.$state['state_id'].'">'.$state['name'].'</option>';
            }

        }
        die(json_encode(array('stateHtml'=>$stateHtml)));
    }

/**get state value based on city**/  
    public function getAllCity() {
        $request = $this->input->post();
        $citys = $this->lead_model->cityvalue($request);

        $cityHtml='<option value="">-- Select City --</option>';
        if(!empty($citys)){
            foreach($citys as $city){
                $cityHtml.='<option value="'.$city['city_id'].'">'.$city['city_name'].'</option>';
            }

        }

        die(json_encode(array('cityHtml'=>$cityHtml)));
    }
/**eamil exit or not check**/
    public function checkEmail() {
        $email = $this->input->post('userEmail');
        $emailCheck = $this->lead_model->get('email', $email, 'lead');
        if (!$emailCheck) {
            echo 0;
        } else {
            echo 1;
        }
    }
    /**PHONE NUMBER exit or not check**/
    /*public function checkPhone() {
        $phone = $this->input->post('checkPhone');
        $phoneCheck = $this->lead_model->get('phone', $phone, 'lead');
        if (!$phoneCheck) {
            echo 0;
        } else {
            echo 1;
        }
    }*/

    //karthik : get the lead datails with pagination
    public function leads(){
        $data['page_title'] = "Lead";
        $userData = $this->session->userdata('user');
        $data['user_name'] = $userData['first_name']; 

        $data = array();
        $filter = $this->input->get();
        //total rows count
        $totalRec = count($this->lead_model->get_all_leads_page(array(), $filter));
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'lead/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
      //  $config['link_func']   = 'searchFilter';
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $data['get_all_leads'] = $this->lead_model->get_all_leads_page(array('limit'=>$this->perPage), $filter);
        $data['filter'] = $filter;
        $data['user_id'] = $userData['user_id']; 
        $data['get_today_leads'] = $this->rest->post('api/lead/day_leads',$data);
        $data['lead_source'] = show_lead_sourse();
        $data['lead_status'] = lead_status_dropdown_options();
        $data['lead_category'] = lead_category_dropdown_options();
        //print_r($data['lead_status']); die();

        //echo "<pre>"; print_r($filter);

        $this->template->load('frontTemplate', 'lead/leads', $data);
    }

    /**
     * Saving recorded history for audio
     * Created by Siva Shankar
     * @param lead ID, Files
     */
    function recorder_history_audio() {
        $rtn_status = array(
            'status' => false
        );
        $lead_id = $this->input->post('lead_id');
        $files = $_FILES;

        //print_r(array_filter($files['audiup']['name'])); die();

        $user = get_logged_user();

        if (!empty($user) && !empty($lead_id) && !empty(array_filter($files['audiup']['name']))) {

            $folder_location = "uploads/recorded_history/audio/{$lead_id}";
            if (!is_dir($folder_location)) {
                mkdir("./$folder_location", 0777, TRUE);
            }

            if (!empty($files['audiup']['name'])) {
                $filesCount = count($files['audiup']['name']);

                for ($i=0; $i < $filesCount; $i++) { 
                    $upName = '';
                    if (!empty($files['audiup']['name'][$i]) && empty($files['audiup']['error'][$i])) {
                        $pass = array(
                            'name' => $files['audiup']['name'][$i],
                            'type' => $files['audiup']['type'][$i],
                            'tmp_name' => $files['audiup']['tmp_name'][$i],
                            'error' => $files['audiup']['error'][$i],
                            'size' => $files['audiup']['size'][$i]
                        );
                    //print_r($pass); die();
                        $upName = $this->file_handling->file_upload(
                            "$folder_location", 
                            $files['audiup']['name'][$i], 
                            $pass, 
                            '*'
                        );
                        if (!empty($upName)) {
                            $recorded_history = array();
                            $recorded_history['lead_id'] = $lead_id;
                            $recorded_history['file_type'] = 'audio';
                            $recorded_history['file_path'] = "$folder_location/$upName";
                            $recorded_history['mime_type'] = $files['audiup']['type'][$i];
                            $recorded_history['created_by'] = $user['user_id'];

                            $this->lead_model->recorded_history($recorded_history);
                        }
                    }
                }
            }

            $rtn_status['status'] = true;
            echo json_encode($rtn_status);            
        } else {
            echo json_encode($rtn_status);
        }
    }    

    /**
     * Saving recorded history for vedio
     * Created by Siva Shankar
     * @param lead ID, Files
     */
    function recorder_history_vedio() {
        $rtn_status = array(
            'status' => false
        );
        $lead_id = $this->input->post('lead_id');
        $files = $_FILES;

        //print_r($files); die();

        $user = get_logged_user();

        if (!empty($user) && !empty($lead_id) && !empty(array_filter($files['vedi']['name']))) {

            $folder_location = "uploads/recorded_history/vedio/{$lead_id}";
            if (!is_dir($folder_location)) {
                mkdir("./$folder_location", 0777, TRUE);
            }

            if (!empty($files['vedi']['name'])) {
                $filesCount = count($files['vedi']['name']);

                for ($i=0; $i < $filesCount; $i++) { 
                    $upName = '';
                    if (!empty($files['vedi']['name'][$i]) && empty($files['vedi']['error'][$i])) {
                        $pass = array(
                            'name' => $files['vedi']['name'][$i],
                            'type' => $files['vedi']['type'][$i],
                            'tmp_name' => $files['vedi']['tmp_name'][$i],
                            'error' => $files['vedi']['error'][$i],
                            'size' => $files['vedi']['size'][$i]
                        );
                    //print_r($pass); die();
                        $upName = $this->file_handling->file_upload(
                            "$folder_location", 
                            $files['vedi']['name'][$i], 
                            $pass, 
                            '*'
                        );
                        if (!empty($upName)) {
                            $recorded_history = array();
                            $recorded_history['lead_id'] = $lead_id;
                            $recorded_history['file_type'] = 'vedio';
                            $recorded_history['file_path'] = "$folder_location/$upName";
                            $recorded_history['mime_type'] = $files['vedi']['type'][$i];
                            $recorded_history['created_by'] = $user['user_id'];

                            $this->lead_model->recorded_history($recorded_history);
                        }
                    }
                }
            }

            $rtn_status['status'] = true;
            echo json_encode($rtn_status);            
        } else {
            echo json_encode($rtn_status);
        }
    }   

    /**
     * Showing lead information in details
     * Created by Siva Shankar
     * @param lead ID
     */
    function details($id) {
        $data = array();
        if (get_logged_user()) {
            if (!empty($id)) {
                $details     = $this->lead_model->get_lead_display_details($id);
                $lead_status = lead_status_dropdown_options();
                $history     = $this->lead_model->get_lead_display_activity($id);
                //echo "<pre>";
                //print_r($history->result()); die();
                if (!empty($details) && $details->num_rows() > 0) {
                    $this->template->load('frontTemplate', 'lead/details', array(
                        'lead_id'             => $id,
                        'details'             => $details,
                        'lead_status_options' => $lead_status,
                        'history'             => $history->result_array()
                    ));           
                } else {
                    redirect(base_url('lead/leads'));
                }
            }
        }
    }

    /**
     * Saving history of leads
     * Created by Siva Shankar
     * @param POST
     */
    function history() {
        $rtn_status = array(
            'status' => 'false'
        );

        $data = array();
        $user = get_logged_user();
        if (!empty($user) && $this->input->post()) {
            $activity_data = array(
                'act_lead_id' => $this->input->post('lead_id'),
                'act_lead_status_id' => $this->input->post('lead_status'),
                'act_status_date' => datetime_saveformater($this->input->post('datetime')),
                'act_comment' => $this->input->post('comment'),
                'act_doc_path' => '',
                'act_created_by' => $user['user_id']
            );

            $upName = '';
            if (!empty($_FILES['doc']['name']) && empty($_FILES['doc']['error'])) {
                $upName = $this->file_handling->file_upload(
                    'uploads/lead_doc', 
                    'doc', 
                    $_FILES['doc'], 
                    'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF|pdf|PDF|doc|docx|DOC|DOCX|xls|XLS|xlsx|XLSX'
                );
                $activity_data['act_doc_path'] = $upName;
            }
            
            $this->lead_model->lead_activity_status($activity_data);
            $rtn_status['status'] = 'true';
            //redirect(base_url("lead/details/{$activity_data['act_lead_id']}"));
            echo json_encode($rtn_status);
        } else {
            //redirect(base_url('lead/details/5'));
            echo json_encode($rtn_status);
        }
    }

    //karthik : ajax pagination call when click
    function ajaxPaginationData(){
        $conditions = array();
        $data['page_title'] = "Ajax pagination";
        $userData = $this->session->userdata('user');
        $data['user_id'] = $userData['user_id'];
        $data['user_name'] = $userData['first_name']; 
        //calc offset number
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        //total rows count
        $totalRec = count($this->lead_model->get_all_leads_page($conditions));
        
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'lead/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['link_func']   = 'searchFilter';
        $this->ajax_pagination->initialize($config);
        
        //set start and limit
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        
        //get posts data
        $data['posts'] = $this->lead_model->get_all_leads_page($conditions);
        
        //load the view
        $this->load->view('lead/ajax-pagination-data', $data, false);
    }
//Get all leads filter

    public function filterleads(){
        $data['page_title'] = "Lead";
        $draw    = intval($this->input->get("draw"));
        $start   = intval($this->input->get("start"));
        $length  = intval($this->input->get("length"));

        $branch_manager_id = $this->input->post('user_id');
        if(!empty($branch_manager_id)){
            $this->lead_model->branch_manager_lead($branch_manager_id);
        }
       //echo "<pre>";print_r($user_id);exit();
        $lead_status = $this->input->post('lead_status');
       // echo "<pre>";print_r($lead_status);exit();
        if(!empty($lead_status)){
            $this->lead_model->lead_status_name($lead_status);
        }
        $lead_category = $this->input->post('lead_category');
        if(!empty($lead_category)){
            $this->lead_model->lead_category_name($lead_category);
        }
        
        $fso_user_id = $this->input->post('fso_user_id');
       // echo "<pre>";print_r($fso_user_id);exit();
        if(!empty($fso_user_id)){
            //echo "<pre>";print_r($fso_user_id);exit();
            $this->lead_model->branch_manger_fso_filter($fso_user_id);
            
        }

        $all_leads_filter= $this->lead_model->get_all_leads_filter();
        $data = [];
        //$i=0;
        $no = $this->input->post('start');
        if(!empty($all_leads_filter)){
                    foreach ($all_leads_filter as $leads_filter) {
            $data[] = array(
               // $leads_filter['id'],
                ///$leads_filter['id'],
                ++$no,
                ucwords($leads_filter['first_name']),
                ucwords($leads_filter['status_name']),
                $leads_filter['category_name'],
                $leads_filter['lead_title'],
                $leads_filter['appointment_date'],
                $leads_filter['renewal_date'],
                '<a style="cursor:pointer;"  class="btn but-blue" href="lead/lead_view/' . $leads_filter['id'] . '"><i class="fa fa-eye" aria-hidden="true"></i>
                </a>'
                // $action_button
            );
        }
  
        }
        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($all_leads_filter),
            "recordsFiltered" => count($all_leads_filter),
            "data" => $data
        );
        echo json_encode($output);
        exit();
       // $this->template->load('frontTemplate', 'lead/filterleads');
    }
    public function lead_view(){
        $id = $this->uri->segment(3);
        // $data['user_profile']=
        $data['assign_lead_profile'] = $this->lead_model->assign_lead_profile($id);
        //echo "<pre>";print_r($data['assign_lead_profile']);exit();
        $this->template->load('frontTemplate', 'lead/fso_profile',$data);
    }
    
}
