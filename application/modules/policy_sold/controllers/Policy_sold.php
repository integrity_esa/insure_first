<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Policy_sold extends Front_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('location_model', 'lead_model'));
    }
    
    public function index() {
        $data['page_title'] = "Policy Sold";
        $userData = $this->session->userdata('user');
        $userId = $userData['user_id'];
        $data['policy_sold'] = $this->location_model->policy_sold($userId);
        $this->template->load('frontTemplate', 'policy_sold', $data);
    }
    public function policy_sold_view(){
        $policy_sold_id = $this->uri->segment('3');
        $data['policy_sold_view'] = $this->location_model->policy_sold_view($policy_sold_id);
      //  echo json_encode(array('policy_sold_view'=>$data));
        //redirect base_url('');
        $this->template->load('frontTemplate', 'policy_sold_view', $data);
        //echo "<pre>";print_r($data['policy_sold_view']);exit();
    }

    /**
     * Created By Siva Shankar
     */
    public function add() {
        $data = array(
            'page_title' => 'Add customer policy'
        );
        $rtn_status = array();

        $request = $this->input->post();
        $user = get_logged_user();
        if ($request && !empty($user)) {
            $param = array();
            $param['lead_id'] = $request['lead_id'];
            $param['policy_type_id'] = $request['policy_type_id'];
            $param['payment_method'] = $request['payment_method'];
            $param['contact_no'] = $request['contact_no'];
            $param['sold_date'] = datetime_saveformater($request['purchase_date']);
            $param['renewal_date'] = datetime_saveformater($request['renewal_date']);
            
            $param['active_status'] = '1';
            $param['user_id'] = $user['user_id'];
            $param['policy_no'] = 'POLICY'.rand(100000,999999);
            $param['created_by'] = $user['user_id'];
            $param['user_id'] = $user['user_id'];

            $this->db->insert('policy_sold', $param);
            $rtn_status['status'] = true;
            echo json_encode($rtn_status);
            die();
        }
        $data['polycy_type_option'] = policy_type_dropdown_options();
        $data['leads_option'] = $this->lead_model->get_leads_dropdown($user['user_id'], true);
        //print_r($data['leads_option']); die();
        $this->template->load('frontTemplate', 'policy_sold_add', $data);
    }

    /**
     * Created By Siva Shankar
     */
    public function validate_customer_policy() {
        $lead_id = $this->input->post('lead_id');
        $policy_type_id = $this->input->post('policy_type_id');

        $rtn_status = array(
            'status' => true
        );

        if (!empty($lead_id) && !empty($policy_type_id)) {
            $this->db->where(array(
                'PS.lead_id' => $lead_id,
                'PS.policy_type_id' => $policy_type_id
            ));
            $resObj = $this->db->get('policy_sold PS');
            //echo $this->db->last_query(); die();
            if (!empty($resObj) && $resObj->num_rows() > 0) {
                $rtn_status['status'] = false;
            }
        }
        echo json_encode($rtn_status);
    }
    
}
