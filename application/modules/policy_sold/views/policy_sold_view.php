<!-- Page Content Holder --> 
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>POLICY DETAILS</p></li>
			</ul>
		</nav>
		<div class="inner-content clearfix">
			<!--my-policy-details-->
			<div class="my-policy-details">
				<!--first row-->
				<?php
				// echo "<pre>";
				// echo $this->db->last_query();
				//print_r($policy_sold_view);exit();
				if(!empty($policy_sold_view)){
					?>
				<div class="row row1">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-5 col-5">
								<h5>Customer Name</h5>
							</div>
							<div class="col-lg-1 col-1">:</div>
							<div class="col-lg-5 col-5">
								<p><?php echo $policy_sold_view['customer_name']; ?></p>
							</div>
						<!-- </div>
						<div class="row"> -->
							<div class="col-lg-5 col-5">
								<h5>Policy Name</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $policy_sold_view['policy_name']; ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-5 col-5">
								<h5>Policy Number</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $policy_sold_view['policy_no']; ?></p>
							</div>
						<!-- </div>
						<div class="row"> -->
							<div class="col-lg-5 col-5">
								<h5>Policy Date</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo date('d-m-Y',strtotime($policy_sold_view['sold_date'])); ?></p>
							</div>
						<!-- 	<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php //echo date('d-m-Y',strtotime($value['sold_date'])); ?></p>
							</div> -->

							<div class="col-lg-5 col-5">
								<h5>Phone</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $policy_sold_view['phone']; ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>DOB</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo date_slash_formater($policy_sold_view['dob']); ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>Gender</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo ($policy_sold_view['gender'] == '1') ? 'Male': ($policy_sold_view['gender'] == '2') ? 'Female' : ''; ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>Primary Address</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $policy_sold_view['address_line1']; ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>Secondary Address</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $policy_sold_view['address_line2']; ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>Puchased Date</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo date_slash_formater($policy_sold_view['sold_date']); ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>Renewal Date</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo date_slash_formater($policy_sold_view['renewal_date']); ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>Contact Number</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $policy_sold_view['contact_no']; ?></p>
							</div>

							<div class="col-lg-5 col-5">
								<h5>Payment Method</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo ucfirst($policy_sold_view['payment_method']); ?></p>
							</div>

						</div>
					</div>
					</div>
				<?php }  ?>
				<!-- </div> -->
				<!--end first row-->
			</div><!--end my-policy-details-->
		</div><!--end inner-content-->
	</div><!--end Page Content Holder-->