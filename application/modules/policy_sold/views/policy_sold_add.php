<!-- Page Content Holder -->
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>CUSTOMER POLICY DETAILS</p></li>
			</ul>
		</nav>
		<div class="inner-content clearfix">
			<?php echo form_open(base_url('policy_sold/add'), array(
				'id' => 'customer-policy-add',
				'data-validate' => base_url('policy_sold/validate_customer_policy')
			)); ?>
			<div class="detail-form">
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Lead</label>
							<?php 
							echo form_dropdown('lead_id', $leads_option, '', array(
								'id' => 'lead_identity',
								'class' => 'form-control custom-select'
							)) ?>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Policy Type</label>
							<?php 
							echo form_dropdown('policy_type_id', $polycy_type_option, '', array(
								'id' => 'policy_type_identity',
								'class' => 'form-control custom-select'
							)) ?>
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>

				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Payment Method</label>
							<?php 
							$option = array(
								 '' => 'Select',
								 'monthly' => 'Monthly',
								 'quarterly' => 'Quarterly',
							);
							echo form_dropdown('payment_method', $option, '', array(
								'class' => 'form-control custom-select'
							)) ?>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group">
							<label>Contact Number</label>
							<?php echo form_input('contact_no', '', array(
								'class' => 'form-control',
								'maxlength' => 10,
							)) ?>
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<div class="form-group cal-img">
							<label>Date of purchase</label>
							<?php echo form_input('purchase_date', '', array(
								'class' => 'form-control',
								'id' => 'purchase-date',
							)) ?>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group cal-img">
							<label>Renewal Date</label>
							<?php echo form_input('renewal_date', '', array(
								'class' => 'form-control',
								'id' => 'renewal-date',
							)) ?>
						</div>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div class="row">
					<div class="col-lg-4 mx-auto">
						<button type="submit" class="btn">SAVE</button>
						<a href="<?php echo base_url('policy_sold'); ?>" type="button" class="btn">CANCEL</a>
					</div>
				</div>
			</div>
			</form>
		</div><!--end inner-content-->
	</div><!--end Page Content Holder -->