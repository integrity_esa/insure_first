<!-- Page Content Holder --> 
	<div id="content">
		<nav class="navbar navbar-default">	
			<ul class=" list-inline navbar-header">
				<li class="list-inline-item"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button></li>
				<li class="list-inline-item"><p>POLICY DETAILS</p></li>
			</ul>
		</nav>
		<div class="inner-content clearfix">
			<!--my-policy-details-->
			<div class="my-policy-details">
				<a href="<?php echo base_url('policy_sold/add'); ?>" class="add-round" id="calender_view"> 
					<img src="http://localhost/insure_first/assets/images/add-round.png" class="img-fluid">
				</a>
				<!--first row-->
				<?php
				// echo "<pre>";
				// echo $this->db->last_query();
				//print_r($policy_sold);exit();
				if(!empty($policy_sold)){
					foreach ($policy_sold as $value) {
					?>
				<div class="row row1">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-5 col-5">
								<h5>Customer Name</h5>
							</div>
							<div class="col-lg-1 col-1">:</div>
							<div class="col-lg-5 col-5">
								<p><?php echo $value['customer_name']; ?></p>
							</div>
						<!-- </div>
						<div class="row"> -->
							<div class="col-lg-5 col-5">
								<h5>Policy Name</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $value['policy_name']; ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-5 col-5">
								<h5>Policy Number</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo $value['policy_no']; ?></p>
							</div>
						<!-- </div>
						<div class="row"> -->
							<div class="col-lg-5 col-5">
								<h5>Policy Date</h5>
							</div>
							<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php echo date('d-m-Y',strtotime($value['sold_date'])); ?></p>
							</div>
							<div class="col-lg-5 col-5">
								<!-- <input type="text" name="policy_sold_id" value="" id="policy_sold_id" class="policy_sold_id"></input> -->
								<!-- <button type="submit" name="submit" class="btn btn-primary policy_id" id="policy_id" data-id="<?php //echo $value['policy_sold_id'];?>">View Details</button> -->
								<a  href="<?php echo base_url('policy_sold/policy_sold_view/'.$value['policy_sold_id']); ?>" class="btn btn-primary policy_id">View Details</a>
							</div>
						<!-- 	<div class="col-lg-1 col-1"><span>:</span></div>
							<div class="col-lg-5 col-5">
								<p><?php //echo date('d-m-Y',strtotime($value['sold_date'])); ?></p>
							</div> -->
						</div>
					</div>
					</div>
				<?php } } ?>
				<!-- </div> -->
				<!--end first row-->
			</div><!--end my-policy-details-->
		</div><!--end inner-content-->
	</div><!--end Page Content Holder-->