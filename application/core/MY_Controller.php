<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Front_Controller extends CI_Controller {	
	
	function __construct(){
		parent::__construct();

			$this->load->helper('url');	
	    	$this->load->helper(array('assets', 'integrate'));
	    	$this->load->library('template');
	    	$this->load->library('session');
	        $this->load->database();
			$this->config->load('restclient');
			$this->load->library('rest', $this->config->item('restclient'));
	}
}

class Admin_Controller extends CI_Controller {
   
	function __construct()
	{
		parent::__construct();
                ob_start();

                $this->load->helper('url');	
		    	$this->load->helper('assets');
		    	$this->load->library('session');
		        $this->load->database();

		        $ctrl_name = $this->uri->segment(1);
		        $method_name = $this->uri->segment(2);

		        if($ctrl_name == 'admin' && $method_name == 'dashboard'){
		        	if ($this->session->userdata('adminEmail') == null) {
			            redirect('admin');
			        }

		        } 
		        
	}
}