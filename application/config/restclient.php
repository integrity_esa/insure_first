<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** Librairie REST Full Client 
 * @author Yoann VANITOU
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache 2.0
 * @link https://github.com/maltyxx/restclient
 */
$config['restclient'] = array(
    'api_key' => 'CODEX@123',
    'api_name' => 'X-API-KEY',
    'http_user' => 'admin',
    'http_pass' => '1234',
    'http_auth' => 'basic'
);

/* End of file restclient.php */
/* Location: ./config/restclient.php */
