<?php
function ssl_support()
{
    $CI =& get_instance();
    return $CI->config->item('ssl_support');
}
if ( ! function_exists('force_ssl'))
{
    function force_ssl()
    {
        if (ssl_support() &&  (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off'))
        {
            $CI =& get_instance();
            $CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
            redirect($CI->uri->uri_string());
        }
    }
}
if ( ! function_exists('remove_ssl'))
{
    function remove_ssl()
    {   
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
        {
            $CI =& get_instance();
            $CI->config->config['base_url'] = str_replace('https://', 'http://', $CI->config->config['base_url']);
            redirect($CI->uri->uri_string());
        }
    }
}
function admin_js($uri, $tag=false)
{
    $CI =& get_instance();
    if($tag)
    {
        return '<script type="text/javascript" src="'.site_url('admin_assets/'.$uri).'"></script>';
    }
    else
    {
        return site_url('admin_assets/js/'.$uri);
    }
}
function admin_img($uri, $tag=false)
{
    $CI =& get_instance();
    if($tag)
    {
        return '<img src="'.site_url('admin_assets/image/'.$uri).'" alt="'.$tag.'">';
    }
    else
    {
        return site_url('admin_assets/image/'.$uri);
    }
}
function admin_css($uri, $tag=false)
{
    $CI =& get_instance();
    if($tag)
    {
        $media=false;
        if(is_string($tag))
        {
            $media = 'media="'.$tag.'"';
        }
        return '<link href="'.site_url('admin_assets/'.$uri).'" type="text/css" rel="stylesheet" '.$media.'/>';
    }
    return site_url('admin_assets/css/'.$uri);
}
function theme_url($uri)
{
    $CI =& get_instance();
    return $CI->config->base_url($uri);
}
function theme_img($uri, $tag=false)
{
    if($tag)
    {
        return '<img src="'.theme_url('assets/images/'.$uri).'" alt="'.$tag.'">';
    }
    else
    {
        return theme_url('assets/images/'.$uri);
    }
}
function theme_js($uri, $tag=false)
{
    if($tag)
    {
        return '<script type="text/javascript" src="'.theme_url('assets/js/'.$uri).'"></script>';
    }
    else
    {
        return theme_url('assets/js/'.$uri);
    }
}
//custom js add karthik
function custom_js($uri, $tag=false)
{
    if($tag)
    {
        return '<script type="text/javascript" src="'.theme_url('assets/js/custom_js/'.$uri).'"></script>';
    }
    else
    {
        return theme_url('assets/js/custom_js'.$uri);
    }
}
function theme_css($uri, $tag=false)
{
    if($tag)
    {
        $media=false;
        if(is_string($tag))
        {
            $media = 'media="'.$tag.'"';
        }
        return '<link href="'.theme_url('assets/css/'.$uri).'" type="text/css" rel="stylesheet" '.$media.'/>';
    }
    return theme_url('assets/css/'.$uri);
}
function theme_profile_img($uri)
{
    $CI =& get_instance();
    return $CI->config->base_url('uploads/profile/source/'.$uri);   
}

function init_admin_tail()
{
    $CI =& get_instance();
    $CI->load->view('admin/common_script_files');
}

function include_admin_header()
{
    $CI =& get_instance();
    $CI->load->view('admin/include/header');
}
function include_admin_side_bar() {
    $CI =& get_instance();
    $CI->load->view('admin/include/sidebar');   
}
function include_admin_footer() {
    $CI =& get_instance();
    $CI->load->view('admin/include/footer');      
}
function include_header_login() {
    $CI =& get_instance();
    $CI->load->view('admin/include/header_login');      
}
function include_footer_login() {
    $CI =& get_instance();
    $CI->load->view('admin/include/footer_login');      
}


function is_logged_in()
{
    $CI =& get_instance();
    if (!$CI->session->has_userdata('member')) {
        return false;
    }
    return true;
}

function is_client_logged_in()
{
    $CI =& get_instance();
    $member = $CI->session->userdata('member');
    if ($member['client_logged_in'] != false) {
        return true;
    }
    return false;
}

function get_client_user_id()
{
    $CI =& get_instance();
    $admin = $CI->session->userdata('admin');
    if ($admin['client_logged_in'] == false) {
        return false;
    }
    return $admin['id'];
}

function is_admin_logged_in()
{
    $CI =& get_instance();
    $admin = $CI->session->userdata('admin');
    if ($admin['admin_logged_in'] != false) {
        return true;
    }
    return false;
}

function get_admin_user_id()
{
    $CI =& get_instance();
    $admin = $CI->session->userdata('admin');
    if ($admin['admin_logged_in'] == false) {
        return false;
    }
    return $admin['id'];
}
