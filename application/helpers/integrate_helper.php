<?php

/**
 * Showing lead belongs to which source
 * Created By Siva Shankar
 * @param lead type
 */
function show_lead_sourse($lead_type='') {
	$ci =& get_instance();
	$lead_source = $ci->lead_model->lead_source();
	//echo "<pre>"; print_r($lead_source->result()); die();
	$lead_source = $lead_source->result();

	if (!empty($lead_source)) {
		$corban = array();
		foreach ($lead_source as $key => $source) {
			$corban[$source->lead_id] = $source->leadname;
		}

		if (!empty($lead_type)) {
			if (!empty($corban[$lead_type])) {
				return $corban[$lead_type];
			}
		} else {
			return $corban;
		}
	}
	return array();
}

/**
 * Generate status dropdown data for dropdown
 * Created by Siva Shankar
 * @param Status ID
 */
function lead_status_dropdown_options($status_id='') {
	$ci =& get_instance();
	$lead_status = $ci->lead_model->lead_status();

	$rtn_data = array();
	if (!empty($lead_status)) {
		foreach ($lead_status as $key => $status) {
			$rtn_data[$status['status_id']] = $status['status_name'];
		}

		if (!empty($status_id)) {
			if (!empty($rtn_data[$status_id])) {
				return $rtn_data[$status_id];
			}
		}
	}
	return $rtn_data;
}

/**
 * Generate policy type dropdown data for dropdown
 * Created by Siva Shankar
 * @param Status ID
 */
function policy_type_dropdown_options($option_id='') {
	$ci =& get_instance();
	$policy_type = $ci->lead_model->policy_type();

	$rtn_data = array();
	if (!empty($policy_type)) {
		$rtn_data[''] ='Select';
		foreach ($policy_type as $key => $status) {
			$rtn_data[$status['policy_id']] = $status['policy_name'];
		}

		if (!empty($option_id)) {
			if (!empty($rtn_data[$option_id])) {
				return $rtn_data[$option_id];
			}
		}
	}
	return $rtn_data;
}

/**
 * Generate category dropdown data for dropdown
 * Created by Siva Shankar
 * @param Status ID
 */
function lead_category_dropdown_options($category_id='') {
	$ci =& get_instance();
	$lead_category = $ci->lead_model->lead_category();

	$rtn_data = array();
	if (!empty($lead_category)) {
		foreach ($lead_category as $key => $category) {
			$rtn_data[$category['category_id']] = $category['category_name'];
		}

		if (!empty($category_id)) {
			if (!empty($rtn_data[$category_id])) {
				return $rtn_data[$category_id];
			}
		}
	}
	return $rtn_data;
}

/**
 * Gets the logged in user informations
 * Created by Siva Shankar
 * @param null
 */
function get_logged_user() {
	$rtn_data = array();
	$ci =& get_instance();

	$user_info = $ci->session->userdata('user');

	if (!empty($user_info['user_id'])) {
		$rtn_data = $user_info;
	}
	return $rtn_data;
}

/**
 * Date Time Functions
 * Created By Siva Shankar
 */

/* General */
function clean_date_time($datetime) {
	return str_replace('/', '-', $datetime);
}

/* Date */

function get_datetime_short_formater() {
	return "d-m-Y h:i A";
}

function get_datetime_format() {
	return "d-m-Y h:i:s A";
}

function get_date_format() {
	return "d-m-Y";	
}

function get_date_slash_format() {
	return "d/m/Y";		
}

function get_ui_datetime_format() {
	return 'DD-MM-YYYY HH:mm:ss';
}

function get_ui_date_format() {
	return 'dd-mm-yyyy';	
}

function get_datetime_saveformat() {
	return "YmdHis";
}

function get_date_saveformat() {
	return "Ymd";	
}

function get_date_short_month_format() {
	return "F";
}

function get_date_short_day_format() {
	return "l";
}

function get_min_date_format() {
	return "d-m-y";		
}

function get_date_short_day_month_format() {
	return "d F";	
}

/* Time */
function get_date_half_format() {
	return "H:i";
}

function get_date_midday_format() {
	return "Ha";
}

function get_datetime_full_view_format() {
	return "l, d M, Y";
}

/* Converter */
function is_empty_date_or_time($datetime) {
	if (empty($datetime) || $datetime == '0000-00-00' || $datetime == '0000-00-00 00:00:00' || $datetime == '01-01-1970' || $datetime == '01-01-1970 00:00:00') {
		return true;
	}
	return false;
}

/* Date */
function datetime_formater($datetime) {
	$datetime = clean_date_time($datetime);
 	return (!is_empty_date_or_time($datetime)) ? date(get_datetime_format(), strtotime($datetime)) : '';
}

function datetime_short_formater($datetime) {
	$datetime = clean_date_time($datetime);
 	return (!is_empty_date_or_time($datetime)) ? date(get_datetime_short_formater(), strtotime($datetime)) : '';
}

function date_formater($date) {
	$date = clean_date_time($date);
	return (!is_empty_date_or_time($date)) ? date(get_date_format(), strtotime($date)) : '';	
}

function date_slash_formater($date) {
	$date = clean_date_time($date);
	return (!is_empty_date_or_time($date)) ? date(get_date_slash_format(), strtotime($date)) : '';	
}

function date_short_month_formater($datetime) {
	$datetime = clean_date_time($datetime);
	return (!is_empty_date_or_time($datetime)) ? date(get_date_short_month_format(), strtotime($datetime)) : '';
}

function date_mindate_formater($datetime) {
	$datetime = clean_date_time($datetime);
	return (!is_empty_date_or_time($datetime)) ? date(get_min_date_format(), strtotime($datetime)) : '';
}

function date_short_day_formater($datetime) {
	$datetime = clean_date_time($datetime);
	return (!is_empty_date_or_time($datetime)) ? date(get_date_short_day_format(), strtotime($datetime)) : '';
}

function date_short_day_month_formater($datetime) {
	$datetime = clean_date_time($datetime);
	return (!is_empty_date_or_time($datetime)) ? date(get_date_short_day_month_format(), strtotime($datetime)) : '';	
}

/* Time */
function time_half_formater($datetime) {
	$datetime = clean_date_time($datetime);
	return (!is_empty_date_or_time($datetime)) ? date(get_date_half_format(), strtotime($datetime)) : '';	
}

function time_hours_midday($datetime) {
	$datetime = clean_date_time($datetime);
	return (!is_empty_date_or_time($datetime)) ? date(get_date_midday_format(), strtotime($datetime)) : '';		
}

function datetime_full_view($datetime) {
	$datetime = clean_date_time($datetime);
	return (!is_empty_date_or_time($datetime)) ? date(get_datetime_full_view_format(), strtotime($datetime)) : '';		
}

/* Database */
function datetime_saveformater($datetime) {
    $rtn_datetime = '';
	$datetime = clean_date_time($datetime);
    if (!is_empty_date_or_time($datetime)) {
    	$rtn_datetime = date(get_datetime_saveformat(), strtotime($datetime));
    }
    return $rtn_datetime;
}

function date_saveformater($date) {
	$rtn_datetime = '';
	$date = clean_date_time($date);
    if (!is_empty_date_or_time($date)) {
    	$rtn_datetime = date(get_date_saveformat(), strtotime($date));
    }
    return $rtn_datetime;
}

/**
 * Date Time Settings End
 */