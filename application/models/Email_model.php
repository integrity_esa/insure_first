<?php

Class Email_model extends CI_Model {
  
    
    public $admin_email = '';
    public $sender_host_name = '';
    public $sender_port = '';
    public $sender_smtp_secure = '';
    public $sender_email = '';
    public $sender_user_email = '';
    public $sender_user_password = '';
    public $sender_reply_to = '';

    function __construct() {
        parent::__construct();
        $this->admin_email = $this->config->item('admin_email');
        $this->sender_port = $this->config->item('sender_port');
        $this->sender_host_name = $this->config->item('sender_host_name');
        $this->sender_smtp_secure = $this->config->item('sender_smtp_secure');

        $this->sender_email = $this->config->item('sender_email');
        $this->sender_user_email = $this->config->item('sender_user_email');
        $this->sender_user_password = $this->config->item('sender_user_password');
        $this->sender_reply_to = $this->config->item('sender_reply_to');
        $this->load->library("MailSender");
    }

    /** SENTHAMIZH SELVI - Common Header 14-12-2018 * */
    function get_header() {

        $message = '<html>
                    <head>
                    <title>INSURE FIRST - {title}</title> 
                    </head>
                    <style>
                    @import url('.'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900'.');
                    </style>
                    <body style="margin-top:0px;">
                    <table width = "620" border = "0" cellpadding = "0" cellspacing = "0" align = "center" style = "background-image: url();font-family: \'Roboto\', sans-serif;"">
                    <tr>
                    <td>
                    <table width = "620" border = "0" cellpadding = "0" cellspacing = "0" align = "center">
                    <tr>
                    <td width = "100%" style = "border-left: 1px solid #e7e7e7;border-right: 1px solid #e7e7e7;border-top: 5px solid #265aa1;">
                    <table border = "0" cellpadding = "0" cellspacing = "0" align = "left">
                    <tr>
                    <td style = "padding:10px 15px" class = "center">
                    <a href = "#"><img src = "'.base_url('assets/images/logo.png').'" alt = "Logo" border = "0" style = "width:150px" /></a>
                    </td>
                    </tr>
                    </table>
                    <table border = "0" cellpadding = "0" cellspacing = "0" align = "right">
                    <tr>
                    <td class = "center" style = "font-size: 16px; color: #fff; font-weight: light; text-align: right; line-height: 22px; vertical-align: middle; padding:10px 20px; font-style:italic;background-color:#275aa0;">
                    <span style = "text-decoration: none; color: #fff;">{sub_title}</span>
                    </td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </table>';
        return $message;
    }

    /** SENTHAMIZH SELVI - Common Footer 14-12-2018 * */
    function get_footer() {
        $message = '<table width="580" border="0" cellpadding="0" cellspacing="0" align="right" style="text-align:left;line-height:26px; font-size: 15px;">

                        <tr><td>Have a question? Contact us: <a href="mailto:support@insurefirst.com" style="color:#275aa0;">support@insurefirst.com</a></td>                               
                        </tr>
                    </table>
                    <table style="height:0px">&nbsp;</table><!-- spacer -->
                    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="text-align: center;border-top: 1px solid #02b4b5;line-height:24px;">
                        <tr>
                            <td style="padding: 10px;">&copy;'.date('Y').' Insure-First. All rights reserved</td>
                        </tr>
                    </table>
                    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#202022" style="background:#202022;">
                        <tr>
                            <td style="background:#444;"><div style="height:6px">&nbsp;</div></td>
                        </tr>                
                    </table>						
                        </td>
                </tr>
        </table> 
        </body>
        </html>';
        return $message;
        
    }

    /* SENTHAMIZH SELVI - Forget password mail - 14-12-2018 */
    function forgot_password_mail($phone, $otp) {
        
        $from_email = $this->admin_email;   
        
        $query = $this->db->query("select * from mail_template where slug='forgot_password'")->row_array();
        $mail_title = $query['mail_title'];
        $mail_subject = $query['mail_subject'];
        $mail_content = $query['mail_content'];
        
        $user = $this->db->query("select * from users where phone='$phone'")->row_array();
        $fullname = ucwords($user['first_name'].' '.$user['last_name']);
        $email = $user['email'];
        
        // Getting the header
        $header_content = $this->get_header();
        
        // Replacing the header dynamic value
        $mail_head1 = str_replace("{title}", $mail_title, $header_content);
        $header_content = str_replace("{sub_title}", $mail_title, $mail_head1);
        
        // Getting the footer
        $footer_content = $this->get_footer();
        
        //Replacing the dynamic values in main content
        $mail_desc1 = str_replace("{username}", $fullname, $mail_content);
        $mail_desc = str_replace("{otp_number}", $otp, $mail_desc1);
        
        $message = $header_content.$mail_desc.$footer_content;
        
        $mailsender = $this->mailsender->load();
        $mailsender->isSMTP();
        $mailsender->Host = $this->sender_host_name;        // Specify main and backup SMTP servers
        $mailsender->SMTPAuth = true;                      // Enable SMTP authentication
        $mailsender->Username = $this->sender_user_email;   // SMTP username
        $mailsender->Password = $this->sender_user_password; // SMTP password
        $mailsender->Port = $this->sender_port;        // SMTP port to connect to GMail
        $mailsender->setFrom($from_email, $mail_title);
        //$mailsender->addReplyTo($this->sender_reply_to, 'Forgot Password Reply');
        $mailsender->addAddress($email);        // Add a recipient
        $mailsender->isHTML(true);        // Set email format to HTML
        $mailsender->Subject = $mail_subject;
        $mailsender->Body = $message;

        if($mailsender->send()){
            return true;
        }
        else{
            return false;
        }        
    }

       /* VELMURUGAN - Admin Forget password mail - 21-12-2018 */
    function admin_forgot_password_mail($email) {
        
        $from_email = $this->admin_email;   
        
        $query = $this->db->query("select * from mail_template where slug='admin_forgot_password'")->row_array();
        $mail_title = $query['mail_title'];
        $mail_subject = $query['mail_subject'];
        $mail_content = $query['mail_content'];
        
        $admin = $this->db->query("select * from admin where email_id='$email'")->row_array();
        $fullname = ucwords($admin['admin_name']);
        $email = $admin['email_id'];
        $password = $admin['real_password'];
        $reset = base_url().'admin/change_password?email='.$email;
        
        // Getting the header
        $header_content = $this->get_header();
        
        // Replacing the header dynamic value
        $mail_head1 = str_replace("{title}", $mail_title, $header_content);
        $header_content = str_replace("{sub_title}", $mail_title, $mail_head1);
        
        // Getting the footer
        $footer_content = $this->get_footer();
        
        //Replacing the dynamic values in main content
        $mail_desc3 = str_replace("{username}", $fullname, $mail_content);
        $mail_desc2 = str_replace("{useremail}", $email, $mail_desc3);
        $mail_desc1 = str_replace("{password}", $password, $mail_desc2);
        $mail_desc = str_replace("{reset}", $reset, $mail_desc1);

        
        $message = $header_content.$mail_desc.$footer_content;
        
        $mailsender = $this->mailsender->load();
        $mailsender->isSMTP();
        $mailsender->Host = $this->sender_host_name;        // Specify main and backup SMTP servers
        $mailsender->SMTPAuth = true;                      // Enable SMTP authentication
        $mailsender->Username = $this->sender_user_email;   // SMTP username
        $mailsender->Password = $this->sender_user_password; // SMTP password
        $mailsender->Port = $this->sender_port;        // SMTP port to connect to GMail
        $mailsender->setFrom($from_email, $mail_title);
        //$mailsender->addReplyTo($this->sender_reply_to, 'Forgot Password Reply');
        $mailsender->addAddress($email);        // Add a recipient
        $mailsender->isHTML(true);        // Set email format to HTML
        $mailsender->Subject = $mail_subject;
        $mailsender->Body = $message;

        if($mailsender->send()){
            return true;
        }
        else{
            return false;
        }        
    }


}
