<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard_model extends CI_Model {
    public function __construct() {
            parent::__construct();

        }
    // Karthik get appoind lead and assigned lead home page
    public function get_today_leads_assign($userId) {
        date_default_timezone_set('Asia/Calcutta');
        $date = new DateTime("now");
        $today = $date->format('Y-m-d');
        $this->db->select('al.id,al.lead_status,al.lead_type,al.lead_cat,al.policy_type,al.lead_id,ls.status_name,lt.leadname as leadtype,lc.category_name,al.policy_type,pt.policy_name,l.company,al.appointment_date,al.renewal_date,l.lead_id,l.first_name,al.reference_id');
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','l.lead_id=al.lead_id','left');
        $this->db->join('lead_status as ls','`ls`.`status_id`=`al`.`lead_status`','left');
        $this->db->join('lead_type as lt','`lt`.`lead_id`=`al`.`lead_type`','left');
        $this->db->join('lead_category as lc','`lc`.`category_id`=`al`.`lead_cat`','left');
        $this->db->join('policy_type as pt','`pt`.`policy_id`=`al`.`policy_type`','left');
        $this->db->group_start();
        $this->db->where('DATE(al.renewal_date)',$today);
        $this->db->or_where('DATE(al.appointment_date)',$today);
        $this->db->group_end();
        $this->db->where('al.reference_id',$userId);
        $get_all_leads = $this->db->get();
        return $get_all_leads->result_array();
    }
    //get event model
    public function get_events($user_id,$start, $end){
        $this->db->select("l.lead_id,l.lead_title as title,CONCAT(l.first_name, ' ', l.last_name) as name,l.address_line1,l.address_line2,l.email,l.phone,al.id,al.lead_id,al.garage_name,al.campaign_name,al.renewal_date as start,al.appointment_date as end,al.lead_status,al.lead_type,pt.policy_name");
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','l.lead_id = al.lead_id','left');
        $this->db->join('policy_type as pt','`pt`.`policy_id`=`al`.`policy_type`','left');
      // $this->db->where("renewal_date >=", $start);
        $this->db->where("appointment_date <=", $end);
        $this->db->where("reference_id", $user_id);
       return $get_all_leads = $this->db->get();
        // echo $this->db->last_query();
    }
        //get event model
    public function get_events_details($user_id,$renewal_date, $appointment_date){
        $this->db->select("l.lead_id,l.first_name as title,al.id,al.lead_id,al.garage_name,al.campaign_name,al.renewal_date as start,al.appointment_date as end,al.lead_status,al.lead_type");
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','l.lead_id = al.lead_id','left');
        $this->db->like('renewal_date', $renewal_date);
        $this->db->or_like('appointment_date', $appointment_date);
        $this->db->where('reference_id', $user_id);
        //$this->db->where(array("date(renewal_date)" => $renewal_date, "date(appointment_date)" => $appointment_date, "reference_id" => $user_id);
        $get_all_leads = $this->db->get()->result_array();
        //echo $this->db->last_query(); exit;
        echo '<pre>'; print_r($get_all_leads); exit;
        return $get_all_leads;       
    }

    /**login user information(03-01-2019)**/
    function get_all($table, $autofield = "", $autofield_val = "")
        {
            $this->db->select('*');
            if ($autofield != "" && $autofield_val != "") {
                $this->db->where($autofield, $autofield_val);
            }
            $result   = $this->db->get($table);
            $data_all = $result->result_array();
            return $data_all;
        }
        
    /**password TRUE OR FALSE(03-01-2019)**/
    public function checkPasswordExist($oldpassword,$userId){
            $user_old_pass_update="SELECT u.user_id,u.password FROM users as u WHERE user_id ='".$userId."' AND password ='".sha1($oldpassword)."'";
            $query = $this->db->query($user_old_pass_update);
            return $query->row_array();
        }

    /**update user information(03-01-2019)**/
    public function profileInfo($new_pass,$userId){
        // $imagename = !empty($files['user_image']['name']) ? ($this->File_Upload('assets/images/user_images', 'user_image', $files['user_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')) :$old_image;
        //$imagename  =!empty($imagename)?$imagename:'';
        $data     = array("real_password"       => $new_pass ,
                            "password"          => sha1($new_pass),
                            "updated_at"        => date('Y-m-d H:i:s'));   
        $this->db->where("user_id", $userId);
        $this->db->update('users', $data);
        return true;
    }

    /**user image upload(03-01-2019)**/
    public function File_Upload($path, $name, $file, $allowed_extensions, $key = '') {
         $config['upload_path']             = $path;
        if (is_numeric($key)) {
            $_FILES['images']['name']       = $file_name = $file['name'];
            $_FILES['images']['type']       = $file['type'];
            $_FILES['images']['tmp_name']   = $file['tmp_name'];
            $_FILES['images']['error']      = $file['error'];
            $_FILES['images']['size']       = $file['size'];
        } else{           
             $file_name=$name;
        }
        $config['allowed_types']            = '*';
        $config['encrypt_name']             = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);  
        if ($this->upload->do_upload('user_image')) {
            $file_data                      = $this->upload->data();
            $filename                       = $file_data['file_name'];            
        } else
            $filename                       = '';
        return $filename;
    }

    /*protected function get_event_by_day($params, $day) {
        $day = str_pad($day, 2, '0', STR_PAD_LEFT);
        $month = str_pad($params['month'], 2, '0', STR_PAD_LEFT);
        $formatted_date = $params['year'].'-'.$month.'-'.$day;
        $user_id = $params['user_id'];
        $sql = "SELECT `l`.`lead_id`, `l`.`first_name` as `title`, `al`.`lead_id`, `al`.`lead_status`, `al`.`lead_type`, 'renewal' as `event_type` FROM `assign_lead` as `al` LEFT JOIN `lead` as `l` ON `l`.`lead_id` = `al`.`lead_id` WHERE date(renewal_date) = '$formatted_date' AND `reference_id` = '$user_id' UNION ALL SELECT `l`.`lead_id`, `l`.`first_name` as `title`,`al`.`lead_id`, `al`.`lead_status`, `al`.`lead_type`, 'appointment' as `event_type` FROM `assign_lead` as `al` LEFT JOIN `lead` as `l` ON `l`.`lead_id` = `al`.`lead_id` WHERE date(appointment_date) = '$formatted_date' AND `reference_id` = '$user_id'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_all_day_events_on_month_year_wise($params) {
        $get_all_leads = array();
        //to get no of days for that month
        $no_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $params['month'], $params['year']);
        // to select the range date
        $days_on_month = range(1, $no_of_days_in_month);

        foreach($days_on_month as $day) {
            $data = array(
                'day' => $day,
                'renewal' => $this->get_event_by_day($params, $day)
                // 'appointment' => $this->get_event_by_day($params, $day),
            );
            $get_all_leads[] = $data;
        }

        return $get_all_leads;
    }*/

    protected function get_event_by_day($params, $day, $flag) {
        // flag => 0 - Checks renewal_date column, 1 - Checks appointment_date column
        // to append before zero and STR_PAD_LEFT this functions is used reverse
        $day = str_pad($day, 2, '0', STR_PAD_LEFT);
        $month = str_pad($params['month'], 2, '0', STR_PAD_LEFT);

        $formatted_date = $params['year'].'-'.$month.'-'.$day;
        $this->db->select("l.lead_id,CONCAT(l.first_name, ' ', l.last_name) AS user_name,al.lead_id, COALESCE(ls.status_name, '') AS status,l.address_line1 as address,al.appointment_date");
        $this->db->from('assign_lead as al');
        $this->db->join('lead_status as ls','ls.status_id = al.lead_status','left');
        $this->db->join('lead as l','l.lead_id = al.lead_id','left');
        if($flag == '0') { // Checks renewal_date column
            $this->db->where('date(renewal_date)', $formatted_date);
        } else if($flag == '1') { // Checks appointment_date column
            $this->db->where('date(appointment_date)', $formatted_date);
        }
        $this->db->where('reference_id', $params['user_id']);

        $get_all_leads = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return $get_all_leads;
    }

    public function get_all_day_events_on_month_year_wise($params) {
        $get_all_leads = array();
        //to get no of days for that month
        $no_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $params['month'], $params['year']);
        // to select the range date
        $days_on_month = range(1, $no_of_days_in_month);

        foreach($days_on_month as $day) {
            $data = array(
                'day' => $day, // get month days
                'renewal' => $this->get_event_by_day($params, $day, '0'), // get renewal leads records
                'appointment' => $this->get_event_by_day($params, $day, '1') // get appointment leads records
            );
            $get_all_leads[] = $data;
        }

        return $get_all_leads;
    }
}

?>  