<?php

Class Admin_model extends CI_Model {

    function checkAdmin($request) {
        $this->db->select('*');
        $this->db->where('email_id', $request['email']);
        $this->db->where('password', sha1($request['password']));
        $query = $this->db->get('admin');
        $result = $query->row_array();
        return $result;
    }  

	function getlead($admin_id){
       $this->db->select('l.first_name,l.status,l.last_name,l.email,al.lead_status,u.first_name as user_fname,u.last_name as user_lname,id');
       $this->db->from('lead l');
       $this->db->join('assign_lead al', 'al.lead_id=l.lead_id ', 'left');
       $this->db->join('users u', 'al.reference_id=u.user_id', 'left');
       $this->db->where('l.status !=', '2');
       $result = $this->db->get();
       return $result->result_array();
    }

    function editLead($id =""){
       $this->db->select('l.*,al.*,u.first_name as user_fname,u.last_name as user_lname');
       $this->db->from('lead as l');
       $this->db->join('assign_lead as al', 'al.lead_id=l.lead_id ', 'left');
       $this->db->join('users as u', 'al.reference_id=u.user_id', 'left');
      // $this->db->join('lead_type as lt', 'lt.lead_id=al.lead_type', 'left');
       //$this->db->group_start();
       $this->db->where('id ', $id);
      // $this->db->group_end();
       $result = $this->db->get();
      // echo $this->db->last_query();exit();
       return $result->row_array();
    }

    function deleteLead($id,$adminId){
        $today = date("Y-m-d H:i:s");
        $log_history = array(
        'created_at' =>$today,
        'created_by' =>$adminId,
        'status' =>'2'
        );
        $this->db->insert('lead_history', $log_history);
        $data = array('status'=>'2');
        $this->db->where('lead_id', $id); 
        $query = $this->db->update('lead', $data);
        return $query;
    }

/***GET COUNTRY VALUE IN DB*****/
public function get_all_order($table, $autofield = "", $autofield_val = "", $order_by = "", $ordersort = "ASC"){
    $this->db->select('*');
    if ($autofield != "" && $autofield_val != "") {
        $this->db->where($autofield, $autofield_val);
    }
    if($table == 'country'){
        $this->db->where('country_id', '101');
    }
    if ($order_by) {
        $this->db->order_by($order_by, $ordersort);
    }
    $result   = $this->db->get($table);
    $data_all = $result->result_array();
    return $data_all;
 }
/**check email AND phone already exit or not **/
public function get( $field_name, $val, $table,$id ){

    
    if($id !=0){
        $this->db->where($field_name, $val);
        $this->db->where('user_id', $id);
        $val = $this->db->get($table)->row_array();
        if(!empty($val))
            return 0;
        else
           return 1;


    }
    else{
        $this->db->where($field_name, $val);
        return $this->db->get($table)->row_array();

    }
 }

/**query for country based  state**/
public function statevalue($request=" "){
    $state ="SELECT state_id,name,country_id FROM state WHERE country_id='".$request['country_id']."'";
    $result     = $this->db->query($state)->result_array();
    return $result;
 }

 /**query for state based city**/
 public function cityvalue($request){
    $city                   ="SELECT city_id,city_name,state_id FROM city WHERE state_id='".$request['state_id']."'";
    $result                 = $this->db->query($city)->result_array();
    return $result;
 }

/** insert and update lead type**/ 
public function insertLeadType($request){
    $today = date("Y-m-d H:i:s");
    $data  =array(
        "leadname"   => $request['ltype'] ,
        "created_at" => $today
        );   
    if(!empty($request['update_id'])){
        $this->db->where('lead_id', $request['update_id']);
        $query=$this->db->update('lead_type', $data);
    } else { 
        $query=$this->db->insert('lead_type', $data);
    }
    return $query;
}
/** insert Category Type**/ 
public function insertCategoryType($request){
    $today                  = date("Y-m-d H:i:s");
    $data                   = array("category_name"  => $request['cname'] ,
                                    "created_date"  => $today);
    if(!empty($request['update_id'])){
        $this->db->where('category_id', $request['update_id']);
        $query=$this->db->update('lead_category', $data);
    } else { 
        $query=$this->db->insert('lead_category', $data);
    }
    return  $query;
}

/** insert policy Type**/ 
public function insertpolicyType($request){
    $today                  = date("Y-m-d H:i:s");
    $data                   = array("policy_name"  => $request['ptype'] ,
                                    "created_at"  => $today);
    if(!empty($request['update_id'])){
        $this->db->where('policy_id', $request['update_id']);
        $query=$this->db->update('policy_type', $data);
    } else { 
        $query=$this->db->insert('policy_type', $data);
    }
    // $this->db->insert('policy_type', $data);
    // $query                  = $this->db->insert_id();
    return  $query;
}
/** insert policy Type**/ 
public function insertLeadStatus($request){
    $today                  = date("Y-m-d H:i:s");
    $data                   = array("status_name"  => $request['lstatus'] ,
                                    "created_at"  => $today);
    if(!empty($request['update_id'])){
        $this->db->where('status_id', $request['update_id']);
        $query=$this->db->update('lead_status', $data);
    } else { 
        $query=$this->db->insert('lead_status', $data);
    }
    return  $query;
}

/** insert user information **/ 
public function admininsert_user($request,$adminId,$files=""){
    // $today              = date("Y-m-d H:i:s");
    // echo "<pre>";print_r($request);
    // echo "<pre>";print_r($today);
    // echo "<pre>";print_r($files);exit();
    //delete old image
    if(!empty($files['user_image']['name']) && !empty($request['old_user_image'])){
       unlink('assets/images/user_images/'.$request['old_user_image']);
    }
    $imagename          = !empty($files['user_image']['name']) ? ($this->adminFile_Upload('assets/images/user_images', 'user_image', $files['user_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')) :'';
    $imagename          =!empty($imagename)?$imagename:'';
    $today              = date("Y-m-d H:i:s");
    $spousename         =!empty($request['spousename'])?$request['spousename']:'';
    $data               = array(
        'lead_title'      => $request['title'] ,
        'first_name'      => $request['fname'] ,
        'last_name'       => $request['lname'] ,
        'address_line1'   => $request['address1'] ,
        'address_line2'   => $request['address2'] , 
        'city_id'         => $request['city'] ,
        'state_id'        => $request['state'] ,
        'country_id'      => $request['country'] , 
        'pincode'         => $request['pincode'], 
        'email'           => $request['email'], 
        'phone'           => $request['phone'] ,
        'dob'             => date('Y-m-d', strtotime($request['dob'])) , 
        'gender'          => $request['gender'] ,
        'company'         => $request['company'] ,  
        'aadhar_id'       => $request['aadharnum'] , 
        'marital_status'  => $request['m_status'], 
        'spouse_name'     => $spousename , 
        'anniversary'     => date('Y-m-d', strtotime($request['anniversary'])) ,
        'modified_by'     => $today , 
        'status'          => '1' ,  
        'created_at'      => $today ,
        'updated_at'      => $today           
        );

        /*if(!empty($request['update_id']) && !empty($files['user_image']['name'])) {
            $data['lead_img'] = $imagename;
        } else if(empty($request['update_id'])) {
            $data['lead_img'] = $imagename;
        }*/

        if (!empty($files['user_image']['name'])) {
            $data['lead_img'] = $imagename;
        }

        if (!empty($request['update_id'])) {
            $this->db->where('lead_id', $request['update_id']);
            $query = $this->db->update('lead', $data);
        } else {
            $query = $this->db->insert('lead', $data);
        }
        $lead_id = $this->db->insert_id();
         if(!empty($request['update_id'])){
             $lead_id=$request['update_id'];
         }else{
             $lead_id= $lead_id;
         }
       $data=array(
        'lead_id'         => $lead_id,
        'garage_name'     => $request['gname'], 
        'campaign_name'   => $request['campaign'] ,
        'lead_status'     => $request['leadstatus'],
        'lead_cat'        => $request['leadcategory'],
        'policy_type'     => $request['policytype'],
        'lead_type'       => $request['leadtype'],
        // 'reference_id'    => $adminId,
        'created_by'      => $adminId, 
        'renewal_date'    => date('Y-m-d', strtotime($request['renewal'])), 
        'appointment_date'=> date('Y-m-d', strtotime($request['appointment']))
                );
        /* Adding lead activity - created by siva shankar */
        if (!empty($lead_id)) {
            $activity_data = array(
                'act_lead_id' => $lead_id,
                'act_lead_status_id' => $request['leadstatus'],
                'act_status_date' => date('Y-m-d H:i:s'),
                'act_created_by' => '0'
            );

            $this->admin_model->lead_activity_status($activity_data);
        }

        if (!empty($request['update_id'])) {
            $lead_id = $request['update_id'];
        } else {
             $lead_id = $lead_id;
        }
       
        $data = array(
            'lead_id'         => $lead_id,
            'garage_name'     => $request['gname'], 
            'campaign_name'   => $request['campaign'] ,
            'lead_status'     => $request['leadstatus'],
            'lead_cat'        => $request['leadcategory'],
            'policy_type'     => $request['policytype'],
            'lead_type'       => $request['leadtype'],
            'reference_id'    => $adminId,
            'created_by'      => $adminId, 
            'renewal_date'    => date('Y-m-d', strtotime($request['renewal'])), 
            'appointment_date'=> date('Y-m-d', strtotime($request['appointment']))
        );
       //to update lead log details
        if(!empty($request['update_id'])){
        $log_history = array(
        'lead_id' =>$lead_id,
        'lead_status' =>$request['leadstatus'],
        'created_at' =>$today,
        'created_by' =>$adminId,
        'status' =>'1'
        );
        $this->db->insert('lead_history', $log_history);
        } else {
        $log_history = array(
        'lead_id' =>$lead_id,
        'lead_status' =>$request['leadstatus'],
        'created_at' =>$today,
        'created_by' =>$adminId
        );
        $this->db->insert('lead_history', $log_history);
            // $query=$this->db->insert('assign_lead', $data);
        }

        if(!empty($request['update_id'])){
            $this->db->where('lead_id', $request['update_id']);
            $query=$this->db->update('assign_lead', $data);
            } else{
            $query=$this->db->insert('assign_lead', $data);
            }
        return $query;
}
/**user image upload**/
public function adminFile_Upload($path, $name, $file, $allowed_extensions, $key = '') {
    $config['upload_path'] = $path;
    if (!is_numeric($key)) {
        $_FILES['images']['name']       = $file_name = $file['name'];
        $_FILES['images']['type']       = $file['type'];
        $_FILES['images']['tmp_name']   = $file['tmp_name'];
        $_FILES['images']['error']      = $file['error'];
        $_FILES['images']['size']       = $file['size'];

    } else{

        $file_name=$name;
    }

    $config['allowed_types'] = '*';
   // $config['encrypt_name'] = TRUE;

    $this->load->library('upload', $config);
    $this->upload->initialize($config);

 //if ($this->upload->do_upload('user_image')) {
    if ($this->upload->do_upload('images')) {
        $file_data = $this->upload->data();
        $filename = $file_data['file_name'];
        return $filename;
    } else{
        $filename = '';
        return $filename;
    }
    }

    ///////////////////////////// VELMURUGAN - 26-12-2018


    function forgot_password($request) {
        $this->db->select('*');
        $this->db->where('email_id', $request);
        $query = $this->db->get('admin');
        $result = $query->row_array();
        // echo $this->db->last_query();exit;
        return $result;
    } 

    function user_list(){
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->join('roles as r','r.role_id=u.role_id','left');
        $this->db->where('u.status !=','2');
        $query = $this->db->get();

        $result = $query->result_array();
        return $result;

    } 
    function new_user($request,$files){
    //delete old image
    if(!empty($files['user_image']['name']) && !empty($request['old_user_image'])){
       unlink('assets/images/user_images/'.$request['old_user_image']);
    }
    // echo "<pre>";print_r($values);print_r($files);exit();
    $imagename = !empty($files['user_image']['name']) ? ($this->adminFile_Upload('assets/images/user_images', 'user_image', $files['user_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')) :'';
    $imagename  =!empty($imagename)?$imagename:'';

    $today = date("Y-m-d H:i:s");
    $data  = array(
        'user_title' => $request['title'] ,
        'first_name' => $request['fname'] ,
        'last_name'  => $request['lname'] ,
        'zip_code'   => $request['location'] ,
        'email'      => $request['email'] , 
        'phone'      => $request['phone'] ,
        'role_id'    => $request['role'] ,
        'password'   => sha1($request['password']), 
        'real_password'  => $request['password'], 
        //'authority_id'   => $request['report_to'], 
        //'isp'            => $request['isp'] ,
        'created_by'     => $today, 
        'modified_by'    => $today,
        'status'         => 1,  
        'created_at'     => $today, 
        'updated_at'     => $today          
        );
        if(!empty($files['user_image']['name'])) {
            $data['profile_picture'] = $imagename;
        }
        if(!empty($request['update_id'])){
        $this->db->where('user_id', $request['update_id']);
        $query=$this->db->update('users', $data);
        } else{
        $query=$this->db->insert('users', $data);
        }
        return $query;
}
function get_user($id){
        $this->db->select('*');
       // $this->db->where('status !=','2');
         $this->db->where('user_id =',$id);
        $query = $this->db->get('users');
        $result = $query->row_array();
        return $result;
}

function user_status($id,$status){
    $this->db->set('status',$status);
    $this->db->where('user_id',$id);
    $result = $this->db->update('users');

    return $result;
 }
function user_delete($id){
    $this->db->set('status','2');
    $this->db->where('user_id',$id);
    $result = $this->db->update('users');

    return $result;
 }
    function update_user($values,$id,$files){
        $imagename = !empty($files['user_image']['name']) ? ($this->adminFile_Upload('assets/images/user_images', 'user_image', $files['user_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')) :'';
    $imagename  =!empty($imagename)?$imagename:'';

    $today = date("Y-m-d H:i:s");
         if(!empty($values)){
            $user_update    = "UPDATE  users SET
            user_title      = '".$values['title']."' ,
            first_name      = '".$values['fname']."' ,
            last_name       = '".$values['lname']."' ,            
            zip_code        = '".$values['location']."',             
            email           = '".$values['email']."' , 
            phone           = '".$values['phone']."' , 
            role_id         = '".$values['role']."',
            password        = '".sha1($values['password'])."',
            real_password   = '".$values['password']."',
            authority_id    = '".$values['report_to']."',
            isp             = '".$values['isp']."',
            created_by      = '".$today."' , 
            modified_by     = '".$today."' , 
            status          = '1' ,  
            created_at      = '".$today."' ,
            updated_at      = '".$today."', 
            profile_picture = '".$imagename."' WHERE user_id = $id";
    $query                  = $this->db->query($user_update);
    
       return $query;

    }
}

function random_password() 
    {
        $alphabet       = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password       = array(); 
        $alpha_length   = strlen($alphabet) - 1; 
        
        for ($i = 0; $i < 8; $i++) 
        {
            $n          = rand(0, $alpha_length);
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password); 
    }
    //echo random_password();

      function role_list(){
        
        $this->db->select('role_id,role_name,permissions');
        $this->db->where('status','1');
        $query          = $this->db->get('roles');
        $result         = $query->result_array();
        $query = $this->db->get('roles');
        $result = $query->result_array();
        // echo $this->db->last_query();exit;
        return $result;

    } 
    
    function add_new_role($data){
        $this->db->select('role_name');
        $this->db->where('status','1');
        $this->db->where('role_name',$data['role_name']);
        $query          = $this->db->get('roles');
        $result         = $query->result_array();
        $message        = '';
        if(($data['role_name'] != NULL || $data['role_name']=='') && empty($result)){
            $insert     =  $this->db->insert('roles',$data);
            if($insert == 1)
            {
                $message = 'success';
            }else{
                $message = 'fail';
            }

        }
        else{
            $message    = 'exists';
        }             


        return $message;

    } 

    function delete_role($data){
        if($data != NULL || $data==''){
        $result =  $this->db->delete('roles', array('role_id' => $data)); 
        }
        return $result;

    } 

    function branch_list(){
        
        $this->db->select('branch_id,branch_name,location');
        $this->db->where('status','1');
        $query          = $this->db->get('branch');
        $result         = $query->result_array();
        
        // echo $this->db->last_query();exit;
        return $result;

    } 

     function add_new_branch($data){
           
        $this->db->select('branch_name');
        $this->db->where('status','1');
        $this->db->where('branch_name',$data['branch_name']);
        $query          = $this->db->get('branch');
        $result         = $query->result_array();
        $message        = '';
        
        if(($data['branch_name'] != NULL || $data['branch_name']=='') && empty($result)){
            $insert     =  $this->db->insert('branch',$data);
            $insert =  $this->db->insert('branch',$data);
            if($insert == 1)
            {
                $message = 'success';
            }else{
                $message = 'fail';
            }

        }
        else{
            $message     = 'exists';
        }        
      
        return $message;
    }

/**list lead type**/
    function leadTypeList(){
        $this->db->select('*')->from('lead_type')->where('status','1');
        $query         = $this->db->get();
        $result        = $query->result_array();
        return $result;
    }
    function edit_lead_type($lead_id=""){
        $this->db->select('l.lead_id,l.leadname,l.status');
        $this->db->from('lead_type as l');
        $this->db->where('l.lead_id',$lead_id);
        $get_lead_type = $this->db->get();
        return $get_lead_type->row_array();
    }
    function delete_lead_type($id){
        $data = array('status'=>'2');
        $this->db->where('lead_id', $id); 
        $query= $this->db->update('lead_type', $data);
        return $query;
    }
/**list lead Category**/
    function leadCategoryList(){
        $this->db->select('*')->from('lead_category')->where('status','1');
        $query         = $this->db->get();
        $result        = $query->result_array();
        return $result;
    }
    function edit_category($category_id=""){
        $this->db->select('l.category_id,l.category_name,l.status');
        $this->db->from('lead_category as l');
        $this->db->where('l.category_id',$category_id);
        $get_lead_type = $this->db->get();
        return $get_lead_type->row_array();
    }
    function delete_lead_category($id){
        $data = array('status'=>'2');
        $this->db->where('category_id', $id); 
        $query= $this->db->update('lead_category', $data);
        return $query;
    }
/**list lead policy**/
    function leadpolicyList(){
        $this->db->select('*')->from('policy_type')->where('status','1');
        $query        = $this->db->get();
        $result       = $query->result_array();
        return $result;
    }
    function edit_policy_type($policy_id=""){
        $this->db->select('l.policy_id,l.policy_name,l.status');
        $this->db->from('policy_type as l');
        $this->db->where('l.policy_id',$policy_id);
        $get_policy_id = $this->db->get();
        return $get_policy_id->row_array();
    }
    function delete_policy_type($id){
        $data = array('status'=>'2');
        $this->db->where('policy_id', $id); 
        $query= $this->db->update('policy_type', $data);
        return $query;
    }
/**list lead status**/
    function leadstatusList(){
        $this->db->select('*')->from('lead_status')->where('status','1');
        $query        = $this->db->get();
        $result       = $query->result_array();
        return $result;
    }
    function edit_lead_status($lead_status=""){
        $this->db->select('l.status_id,l.status_name,l.status');
        $this->db->from('lead_status as l');
        $this->db->where('l.status_id',$lead_status);
        $get_lead_type = $this->db->get();
        return $get_lead_type->row_array();
    }
    function delete_lead_status($id){
        $data = array('status'=>'2');
        $this->db->where('status_id', $id); 
        $query= $this->db->update('lead_status', $data);
        return $query;
    }
   /** Karthik query for check Email and Phone exists**/
    public function checkEmailExists($email='',$phone=''){
        $check_mail   = "SELECT lead.email,lead.phone FROM lead WHERE email='".$email."' or phone='".$phone."' AND status !=2";
        $result = $this->db->query($check_mail)->result_array();
        return $result;
     }
    //check existing catogory name in add
    public function check_category($id,$table_name,$request,$col_name)
    {
        // print_r($request);exit();
        if(!empty($request['update_id'])){
        $this->db->select('*');
        $this->db->from($table_name);
        if(!empty($request['ltype'])){
        $this->db->where($col_name,trim($request['ltype']));   
        }
        if(!empty($request['cname'])){
        $this->db->where($col_name,trim($request['cname']));
        }
        if(!empty($request['lstatus'])){
        $this->db->where($col_name,trim($request['lstatus']));
        }
        if(!empty($request['ptype'])){
        $this->db->where($col_name,trim($request['ptype']));
        }
        if(!empty($request['campaign'])){
        $this->db->where($col_name,trim($request['campaign']));
        }
        $this->db->where($id.'!=',$request['update_id']);
        $this->db->where('status','1');
        $result1 = $this->db->get();
        return $result1;

        }else{
        $this->db->select('*');
        $this->db->from($table_name);
        if(!empty($request['ltype'])){
        $this->db->where($col_name,trim($request['ltype']));
        }
        if(!empty($request['cname'])){
        $this->db->where($col_name,trim($request['cname']));
        }
        if(!empty($request['lstatus'])){
        $this->db->where($col_name,trim($request['lstatus']));
        }
        if(!empty($request['ptype'])){
        $this->db->where($col_name,trim($request['ptype']));
        }
        if(!empty($request['campaign'])){
        $this->db->where($col_name,trim($request['campaign']));
        }
        $this->db->where('status','1');
        $result1 = $this->db->get();
        return $result1;
        }
    }
    /** Naveen query for Settings **/
    public function getSettings(){
        $check_mail   = "SELECT * FROM settings WHERE id=1";
        $result = $this->db->query($check_mail)->row_array();
        return $result;
    }

    public function new_settings($values,$files){
        $logoimagename = !empty($files['logo_image']['name']) ? ($this->adminFile_Upload('assets/images/logo_images', 'logo_image', $files['logo_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')) :'';
        $logoimagename  =!empty($logoimagename)?$logoimagename:'';
        $faviconimagename = !empty($files['favicon_image']['name']) ? ($this->adminFile_Upload('assets/images/favicon_images', 'favicon_image', $files['favicon_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')): '';
        $faviconimagename  =!empty($faviconimagename)?$faviconimagename:'';

        $today = date("Y-m-d H:i:s");
        $data=array(
            'site_name'=>$values['site_name'],
            'admin_email'=>$values['admin_email'],
            'kilometer'=>$values['kilometer'],
            'item_number'=>$values['item_number'],
            'created_at'=>$today,
            'updated_at'=>$today
        ); 
        if(!empty($files['logo_image']['name']) && $values['old_logo_image']){
            unlink('assets/images/logo_images/'.$values['old_logo_image']);
        }
        if(!empty($files['favicon_image']['name']) && $values['old_favicon_image'])
        {
            unlink('assets/images/favicon_images/'.$values['old_favicon_image']);
        }
        if(!empty($files['logo_image']['name'])){
        $data['logo_image']=$logoimagename; 
        }
        if(!empty($files['favicon_image']['name'])){
        $data['favicon_image']=$faviconimagename;
        }
        if(!empty($values['edit_id'])){
            $this->db->where('id',$values['edit_id']);
            $res= $this->db->update('settings', $data);

        } else{
            $res= $this->db->insert('settings', $data);
        }
        return $res;
    }
    // Get selected field data values from table
    public function getDataFromTable($fieldname, $tablename, $where) {
        $res = $this->db->select($fieldname)->where($where)->limit(1, 0)->get($tablename)->row_array();
        return $res;
    }

    // campaign
    function campaignList(){
        $this->db->select('*')->from('campaign_type')->where('status','1');
        $query        = $this->db->get();
        $result       = $query->result_array();
        return $result;
    }
    function edit_campaign($campaign_id=""){
        $this->db->select('c.campaign_id,c.campaign_name,c.status');
        $this->db->from('campaign_type as c');
        $this->db->where('c.campaign_id',$campaign_id);
        $get_campaign_id = $this->db->get();
        return $get_campaign_id->row_array();
    }
    function delete_campaign($id){
        $data = array('status'=>'2');
        $this->db->where('campaign_id', $id); 
        $query= $this->db->update('campaign_type', $data);
        return $query;
    }
    public function insertCampaign($request){
    $today                  = date("Y-m-d H:i:s");
    $data                   = array("campaign_name"  => $request['campaign'] ,
                                    "created_at"  => $today);
    if(!empty($request['update_id'])){
        $this->db->where('campaign_id', $request['update_id']);
        $query=$this->db->update('campaign_type', $data);
    } else { 
        $query=$this->db->insert('campaign_type', $data);
    }
    // $this->db->insert('campaign_type', $data);
    // $query                  = $this->db->insert_id();
    return  $query;
}

    /**
     * Save lead activity log
     * Created by siva shankar
     * @param status data
     */
    function lead_activity_status($status) {
        if (!empty($status)) {
            $result = $this->db->insert('lead_activity', $status);
            $last_insert = $this->db->insert_id();
            if (!empty($result)) {
                $assign_lead = array(
                    'lead_status' => $status['act_lead_status_id'],
                    'appointment_date' => $status['act_status_date'],
                );  
                $this->db->where(array(
                    'lead_id' => $status['act_lead_id'],
                ));
                $this->db->update('assign_lead', $assign_lead);
            }
            return $last_insert;
        }
        return false;
    }

    public function checkEmail($email,$id){
        if(!empty($id)){
        $check_mail   = "SELECT email FROM users WHERE email='$email' AND user_id != $id";  
        }else{
        $check_mail   = "SELECT email FROM users WHERE email='$email'";
        }
        $result = $this->db->query($check_mail)->result_array();
        //echo $this->db->last_query();
        return $result;
     }

     public function checkPhone($phone,$id){
        if(!empty($id)){
        $check_phone   = "SELECT phone FROM users WHERE phone='$phone' AND user_id != $id";  
        }else{
        $check_phone   = "SELECT phone FROM users WHERE phone='$phone'";
        }
        $result = $this->db->query($check_phone)->result_array();
        //echo $this->db->last_query();
        return $result;
     }
 }

?>