<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead_model extends CI_Model {
public function __construct() {
        parent::__construct();
        
    }
    private $_lead_status;
    private $_lead_category;
    private $_branch_manager;
    private $_branch_manger_fso_filter;
    public function lead_status_name($lead_status) {
        $this->_lead_status = $lead_status;
    }
    public function lead_category_name($lead_category) {
        $this->_lead_category = $lead_category;
    }
    public function branch_manager_lead($branch_manager_id) {
        $this->_branch_manager = $branch_manager_id;
    }
    public function branch_manger_fso_filter($fso_user_id) {
        $this->_branch_manger_fso_filter = $fso_user_id;
    }
    
/***GET COUNTRY VALUE IN DB*****/
public function get_all_order($table, $autofield = "", $autofield_val = "", $order_by = "", $ordersort = "ASC")
        {
            $this->db->select('*');
            if ($autofield != "" && $autofield_val != "") {
                $this->db->where($autofield, $autofield_val);
            }
            if ($order_by) {
                $this->db->order_by($order_by, $ordersort);
            }
            $result   = $this->db->get($table);
            //echo $this->db->last_query();exit;
            $data_all = $result->result_array();
            return $data_all;
        }

/**check email AND phone already exit or not **/
public function get( $field_name, $val, $table ) 
        {
            $this->db->where($field_name, $val);
            return $this->db->get($table)->row_array();
        }

/**query for country based  state**/
public function statevalue($request){
            $state ="SELECT state_id,name,country_id FROM state WHERE country_id='".$request['country_id']."'";
            $result     = $this->db->query($state)->result_array();
            return $result;
 }

 /**query for state based city**/
 public function cityvalue($request){
            $city ="SELECT city_id,city_name,state_id FROM city WHERE state_id='".$request['state_id']."'";
            $result     = $this->db->query($city)->result_array();
            return $result;
 }

/** insert user information **/ 
public function insert_user($request,$userId,$files=""){
    //echo "<pre>";print_r($request);exit();
    $imagename = !empty($files['user_image']['name']) ? ($this->File_Upload('assets/images/user_images', 'user_image', $files['user_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')) :'';

    $imagename  =!empty($imagename)?$imagename:'';
    $today = date("Y-m-d H:i:s");

         if(!empty($request)){
            $user_update    = "INSERT INTO lead SET
            lead_title      = '".$request['title']."' ,
            first_name      = '".$request['fname']."' ,
            last_name       = '".$request['lname']."' ,
            address_line1   = '".$request['address1']."' ,
            address_line2   = '".$request['address2']."' , 
            city_id         = '".$request['city']."' ,
            state_id        = '".$request['state']."' ,
            country_id      = '".$request['country']."' , 
            pincode         = '".$request['pincode']."' , 
            email           = '".$request['email']."' , 
            phone           = '".$request['phone']."' ,
            dob             = '".date('Y-m-d', strtotime($request['dob']))."' , 
            gender          = '".$request['gender']."' ,
            company         = '".$request['company']."' ,  
            aadhar_id       = '".$request['aadharnum']."' , 
            marital_status  = '".$request['m_status']."', 
            spouse_name     = '".$request['spousename']."' , 
            anniversary     = '".date('Y-m-d', strtotime($request['anniversary']))."' ,
            modified_by     = '".$today."' , 
            status          = '".$today."' ,  
            created_at      = '".$today."' ,
            updated_at      = '".$today."', 
            lead_img        = '".$imagename."'";
    $query     = $this->db->query($user_update);
    $lead_id = $this->db->insert_id();

    $garage_name = $request['gname'] ? $request['gname']: '';
    $campaign = $request['campaign'] ? $request['campaign']: '';
    $campaignnamelocation = $request['campaignnamelocation'] ?$request['campaignnamelocation']: '';

    /* Adding lead activity - created by siva shankar */
    if (!empty($lead_id)) {
        $activity_data = array(
            'act_lead_id' => $lead_id,
            'act_lead_status_id' => $request['leadstatus'],
            'act_status_date' => datetime_saveformater(date('Y-m-d H:i:s')),
            'act_created_by' => $userId
        );

        $this->lead_model->lead_activity_status($activity_data);
    }
    $user_upadte1="INSERT INTO assign_lead SET 
                lead_id         = '".$lead_id."',
                garage_name     = '".$garage_name."' , 
                campaign_name   = '".$campaign."' ,
                lead_status     = '".$request['leadstatus']."' ,
                lead_type       = '".$request['leadtype']."' ,
                lead_cat        = '".$request['leadcategory']."' ,
                policy_type     = '".$request['policytype']."' ,
                reference_id    = '".$userId."' ,
                created_by      = '".$userId."' ,
                campaign_location      = '".$campaignnamelocation."' ,
                renewal_date    = '".date('Y-m-d', strtotime($request['renewal']))."' , 
                appointment_date= '".date('Y-m-d', strtotime($request['appointment']))."' ";  
    $query     = $this->db->query($user_upadte1);
    return $query;
    }
}
/** insert user information **/ 
public function add_Lead($request,$userId,$files=""){
   // echo "<pre>";print_r($request);exit();
    $imagename = !empty($files['user_image']['name']) ? ($this->File_Upload('assets/images/user_images', 'user_image', $files['user_image'], 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF')) :'';

    $imagename  =!empty($imagename)?$imagename:'';
    $today = date("Y-m-d H:i:s");

         if(!empty($request)){
            $user_update    = "INSERT INTO lead SET
            lead_title      = '".$request['title']."' ,
            first_name      = '".$request['fname']."' ,
            last_name       = '".$request['lname']."' ,
            address_line1   = '".$request['address1']."' ,
            address_line2   = '".$request['address2']."' , 
            city_id         = '".$request['city']."' ,
            state_id        = '".$request['state']."' ,
            country_id      = '".$request['country']."' , 
            pincode         = '".$request['pincode']."' , 
            email           = '".$request['email']."' , 
            phone           = '".$request['phone']."' ,
            dob             = '".date('Y-m-d', strtotime($request['dob']))."' , 
            gender          = '".$request['gender']."' ,
            company         = '".$request['company']."' ,  
            aadhar_id       = '".$request['aadharnum']."' , 
            marital_status  = '".$request['m_status']."', 
            spouse_name     = '".$request['spousename']."' , 
            anniversary     = '".date('Y-m-d', strtotime($request['anniversary']))."' ,
            modified_by     = '".$today."' , 
            status          = '".$today."' ,  
            created_at      = '".$today."' ,
            updated_at      = '".$today."', 
            lead_img        = '".$imagename."'";
    $query     = $this->db->query($user_update);
    $lead_id = $this->db->insert_id();

    $garage_name = $request['gname'] ? $request['gname']: '';
    $campaign = $request['campaign'] ? $request['campaign']: '';

    /* Adding lead activity - created by siva shankar */
    if (!empty($lead_id)) {
        $activity_data = array(
            'act_lead_id' => $lead_id,
            'act_lead_status_id' => $request['leadstatus'],
            'act_status_date' => datetime_saveformater(date('Y-m-d H:i:s')),
            'act_created_by' => $userId
        );

        $this->lead_model->lead_activity_status($activity_data);
    }
    $user_upadte1="INSERT INTO assign_lead SET 
                lead_id         = '".$lead_id."',
                garage_name     = '".$garage_name."' , 
                campaign_name   = '".$campaign."' ,
                lead_status     = '".$request['leadstatus']."' ,
                lead_type       = '".$request['leadtype']."' ,
                lead_cat        = '".$request['leadcategory']."' ,
                policy_type     = '".$request['policytype']."' ,
                reference_id    = '".$userId."' ,
                created_by      = '".$userId."' ,
                campaign_location      = '".$campaign."' ,
                renewal_date    = '".date('Y-m-d', strtotime($request['renewal']))."' , 
                appointment_date= '".date('Y-m-d', strtotime($request['appointment']))."' ";  
    $query     = $this->db->query($user_upadte1);
    return $query;
    }
}
/**user image upload**/
public function File_Upload($path, $name, $file, $allowed_extensions, $key = '') {
    $config['upload_path'] = $path;
    //echo"<pre>";print_r($path);die;
    if (is_numeric($key)) {
        $_FILES['images']['name']       = $file_name = $file['name'];
        $_FILES['images']['type']       = $file['type'];
        $_FILES['images']['tmp_name']   = $file['tmp_name'];
        $_FILES['images']['error']      = $file['error'];
        $_FILES['images']['size']       = $file['size'];
    } else{

        $file_name=$name;
    }

    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;

    $this->load->library('upload', $config);
    $this->upload->initialize($config);


    if ($this->upload->do_upload('user_image')) {
        $file_data = $this->upload->data();
        $filename = $file_data['file_name'];

    } else
    $filename = '';
    return $filename;
    }

    // karthik :  get today leads in lead page
    public function get_day_leads($userId, $filter=array()) {
        date_default_timezone_set('Asia/Calcutta');
       // $today = date("Y-m-d");
        $date = new DateTime("now");
        $today = $date->format('Y-m-d');

        $this->db->select('al.id,al.lead_status,al.lead_type,al.created_at,al.lead_cat,al.policy_type,al.lead_id,ls.status_name,lt.leadname as leadtype,lc.category_name,pt.policy_name,l.company,al.appointment_date,al.renewal_date,l.lead_id,l.first_name,al.reference_id');
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','l.lead_id=al.lead_id','left');
        $this->db->join('lead_status as ls','`ls`.`status_id`=`al`.`lead_status`','left');
        $this->db->join('lead_type as lt','`lt`.`lead_id`=`al`.`lead_type`','left');
        $this->db->join('lead_category as lc','`lc`.`category_id`=`al`.`lead_cat`','left');
        $this->db->join('policy_type as pt','`pt`.`policy_id`=`al`.`policy_type`','left');
        $this->db->group_start();
        // $this->db->where('DATE(al.renewal_date)',$today);
        $this->db->where('DATE(al.created_at)',$today);
        $this->db->or_where('DATE(al.appointment_date)',$today);
        $this->db->group_end();
        $this->db->where('al.reference_id',$userId);

        /* Created by Siva Shankar - Filter for leaded */
        /* Filter By Name */
        if (!empty($filter['leadname'])) {
            $this->db->like('l.first_name', $filter['leadname']);
            $this->db->or_like('l.last_name', $filter['leadname']);
        }

        /* Category filter */
        if (!empty($filter['category'])) {
            $this->db->where_in('al.lead_cat', $filter['category']);
        }

        /* Category filter */
        if (!empty($filter['lead_status'])) {
            $this->db->where_in('al.lead_status', $filter['lead_status']);
        }

        /* Filter Lead Type */
        if (!empty($filter['lead_type'])) {
            $this->db->where('al.lead_type', $filter['lead_type']);
        }
        /* Filter End */

        $get_all_leads = $this->db->get();
        return $get_all_leads->result_array();
    }

    //Karthik pagiantion with demo
    function get_all_leads_page($params = array(), $filter=array()){
        $userData = $this->session->userdata('user');
        $userId = $userData['user_id'];
        $today = date('Y-m-d');
        $this->db->select(array(
            "al.lead_status", 
            "al.appointment_date", 
            "al.created_at", 
            "al.renewal_date",
            "l.lead_id", 
            "l.first_name",
            "lt.leadname","lc.category_name"
        ));
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','`l`.`lead_id`=`al`.`lead_id`','left');
        $this->db->join('lead_type as lt','`lt`.`lead_id`=`al`.`lead_type`','left');
         $this->db->join('lead_category as lc','`lc`.`category_id`=`al`.`lead_status`','left');
        //set start and limit

        if (!empty($params)) {
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
        }
        
        $this->db->where('`al`.`reference_id',$userId);
        $this->db->order_by('al.id','DESC');

        /* Created by Siva Shankar - Filter for leaded */
        /* Filter By Name */
        if (!empty($filter['leadname'])) {
            $this->db->like('l.first_name', $filter['leadname']);
            $this->db->or_like('l.last_name', $filter['leadname']);
        }

        /* Category filter */
        if (!empty($filter['category'])) {
            $this->db->where_in('al.lead_cat', $filter['category']);
        }

        /* Category filter */
        if (!empty($filter['lead_status'])) {
            $this->db->where_in('al.lead_status', $filter['lead_status']);
        }

        /* Filter Lead Type */
        if (!empty($filter['lead_type'])) {
            $this->db->where('al.lead_type', $filter['lead_type']);
        }
        /* Filter End */

        //get records
        $query = $this->db->get();
        //echo $this->db->last_query(); die();
        //return fetched data
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }  
         //Karthik get all lead api
    function get_all_leads($userId){
        $today = date('Y-m-d');
        $this->db->select('al.id,al.lead_status,al.lead_type,al.created_at,al.lead_cat,al.policy_type,al.lead_id,ls.status_name,lt.leadname as leadtype,lc.category_name,pt.policy_name,l.company,al.appointment_date,al.renewal_date,l.lead_id,l.first_name,al.reference_id');
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','`l`.`lead_id`=`al`.`lead_id`','left');
        $this->db->join('lead_status as ls','`ls`.`status_id`=`al`.`lead_status`','left');
        $this->db->join('lead_type as lt','`lt`.`lead_id`=`al`.`lead_type`','left');
        $this->db->join('lead_category as lc','`lc`.`category_id`=`al`.`lead_cat`','left');
        $this->db->join('policy_type as pt','`pt`.`policy_id`=`al`.`policy_type`','left');
        $this->db->where('`al`.`reference_id',$userId);
        //get records
        $query = $this->db->get();
        //return fetched data
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    // get all the country
    function get_country(){
        $this->db->select('country.country_id,country.country_name,country.phone_code');
        $this->db->from('country');
        $this->db->order_by('country.country_id','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }  
    // get all the state
    function get_state($country_id=""){
        $this->db->select('state.state_id,state.name,state.country_id');
        $this->db->from('state');
        if ($country_id !="") {
                $this->db->where('country_id', $country_id);
        }
        $this->db->order_by('state.state_id','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    // get all the city
    function get_city($state_id=""){
        $this->db->select('city.city_id,city.city_name,city.state_id');
        $this->db->from('city');
        if ($state_id !="") {
                $this->db->where('state_id', $state_id);
        }
        $this->db->order_by('city.city_id','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    // get all the policy type
    function lead_type(){
        $this->db->select('lead_type.lead_id,lead_type.leadname');
        $this->db->from('lead_type');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    } 
    // get all the lead_category
    function lead_category(){
        $this->db->select('lead_category.category_id,lead_category.category_name');
        $this->db->from('lead_category');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    // get all the lead_status
    function lead_status(){
        $this->db->select('lead_status.status_id,lead_status.status_name');
        $this->db->from('lead_status');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    } 
    // get all the lead_status
    function policy_type(){
        $this->db->select('policy_type.policy_id,policy_type.policy_name');
        $this->db->from('policy_type');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    // get all the campaign data
    function campaign(){
        $this->db->select('campaign_type.campaign_id,campaign_type.campaign_name');
        $this->db->from('campaign_type');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }


    //Get all leads in front view web filter
    function get_all_leads_filter(){
        $today = date('Y-m-d');
        $user = $this->session->userdata('user');
        $user_id = $user['user_id'];
        $role_id = $user['role_id'];
       // echo "<pre>";echo $role_id;exit();

        $this->db->select('al.id,ls.status_name,al.branch_manager,lt.leadname as leadtype,lc.category_name,al.policy_type,al.lead_id,al.appointment_date,al.renewal_date,l.lead_title,l.first_name,l.last_name,al.reference_id');
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','`l`.`lead_id`=`al`.`lead_id`','left');
        $this->db->join('lead_status as ls','`ls`.`status_id`=`al`.`lead_status`','left');
        $this->db->join('lead_type as lt','`lt`.`lead_id`=`al`.`lead_type`','left');
        $this->db->join('lead_category as lc','`lc`.`category_id`=`al`.`lead_cat`','left');
        if(!empty($this->_lead_status)){
            $this->db->group_start();
            $this->db->like('ls.status_id', $this->_lead_status,'both');
            $this->db->group_end();
        } 
        if(!empty($this->_lead_category)){
            $this->db->group_start();
            $this->db->like('lc.category_id', $this->_lead_category,'both');
            $this->db->group_end();
        }
        if(!empty($this->_branch_manager)){
        
            $this->db->group_start();
            $this->db->where('al.branch_manager',$this->_branch_manager);
            $this->db->group_end();
        } 
        if(!empty($this->_branch_manger_fso_filter)){
            $this->db->group_start();
            //$this->db->where('al.branch_manager',$this->_branch_manager);
           // $this->db->where('al.reference_id !=','0');
            // $this->db->where('al.id',$this->_branch_manger_fso_filter);
            $this->db->where('al.reference_id',$this->_branch_manger_fso_filter);
            $this->db->group_end();
        }
        if(!empty($role_id == 1)){
                $this->db->where('al.branch_head',$user_id);
        }
        if(!empty($role_id == 2)){
                $this->db->where('al.branch_manager',$user_id);
        }
        //get records
        $query = $this->db->get();
        //echo $this->db->last_query();exit();
        //return fetched data
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    //Get all leads filter for admin
    function get_all_leads_filter_admin(){
        $today = date('Y-m-d');
       // echo "<pre>";echo $role_id;exit();

        $this->db->select('al.id,ls.status_name,al.branch_manager,lt.leadname as leadtype,lc.category_name,al.policy_type,al.lead_id,al.appointment_date,al.renewal_date,l.lead_title,l.first_name,l.last_name,al.reference_id');
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','`l`.`lead_id`=`al`.`lead_id`','left');
        $this->db->join('lead_status as ls','`ls`.`status_id`=`al`.`lead_status`','left');
        $this->db->join('lead_type as lt','`lt`.`lead_id`=`al`.`lead_type`','left');
        $this->db->join('lead_category as lc','`lc`.`category_id`=`al`.`lead_cat`','left');
        if(!empty($this->_lead_status)){
            $this->db->group_start();
            $this->db->like('ls.status_id', $this->_lead_status,'both');
            $this->db->group_end();
        } 
        if(!empty($this->_lead_category)){
            $this->db->group_start();
            $this->db->like('lc.category_id', $this->_lead_category,'both');
            $this->db->group_end();
        }
        if(!empty($this->_branch_manager)){
        
            $this->db->group_start();
            $this->db->where('al.branch_manager',$this->_branch_manager);
            $this->db->group_end();
        } 
        if(!empty($this->_branch_manger_fso_filter)){
            $this->db->group_start();
            //$this->db->where('al.branch_manager',$this->_branch_manager);
           // $this->db->where('al.reference_id !=','0');
            // $this->db->where('al.id',$this->_branch_manger_fso_filter);
            $this->db->where('al.reference_id',$this->_branch_manger_fso_filter);
            $this->db->group_end();
        }
        // if(!empty($role_id == 1)){
        //         $this->db->where('al.branch_head',$user_id);
        // }
        // if(!empty($role_id == 2)){
        //         $this->db->where('al.branch_manager',$user_id);
        // }
        //get records
        $query = $this->db->get();
        //echo $this->db->last_query();exit();
        //return fetched data
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    //Lead model ASSIGN LEADS DERAILS
    public function assign_lead_profile($id){
        $this->db->select("al.id,ls.status_name,lt.leadname as leadtype,lc.category_name,al.policy_type,pt.policy_name,al.lead_id,al.appointment_date,al.campaign_owner,al.campaign_location,al.created_at,al.updated_at,l.lead_title,l.first_name,l.last_name,l.address_line1,l.address_line2,city.city_name,state.name,country.country_name,l.pincode,l.email,l.phone,l.gender,l.dob,l.company,l.aadhar_id,l.marital_status,l.spouse_name,l.anniversary,CONCAT(u.first_name, ' ', u.last_name) AS lead_owner,al.reference_id,ct.campaign_id,ct.campaign_name");
        $this->db->from('assign_lead as al');
        $this->db->join('lead as l','`l`.`lead_id`=`al`.`lead_id`','left');
        $this->db->join('lead_status as ls','`ls`.`status_id`=`al`.`lead_status`','left');
        $this->db->join('lead_type as lt','`lt`.`lead_id`=`al`.`lead_type`','left');
        $this->db->join('campaign_type as ct','`al`.`campaign_name`=`ct`.`campaign_id`','left');
        $this->db->join('lead_category as lc','`lc`.`category_id`=`al`.`lead_cat`','left');
        $this->db->join('policy_type as pt','`pt`.`policy_id`=`al`.`policy_type`','left');
        $this->db->join('city as city','`city`.`city_id`=`l`.`city_id`','left');
        $this->db->join('state as state','`state`.`state_id`=`l`.`state_id`','left');
        $this->db->join('country as country','`country`.`country_id`=`l`.`country_id`','left');
        $this->db->join('users as u','`u`.`user_id`=`l`.`created_by`','left');
        $this->db->where('al.id',$id);
        $query = $this->db->get();
        return $query->row_array();
    }
    /**
     * Gets the lead information
     * Created by siva shankar
     * @param lead ID
     */
    function get_lead_display_details($lead_id, $params=array()) {
        $rtn_data = array();
        if (!empty($lead_id)) {
            $this->db->where(array(
                'L.lead_id' => $lead_id
            ));

            if (!empty($params['select'])) {
                $this->db->select($params['select']);
            }

            $this->db->join('assign_lead AL', 'AL.lead_id = L.lead_id');
            $this->db->join('lead_type LT', 'LT.lead_id = AL.lead_type');
            return $this->db->get('lead L');
            //echo $this->db->last_query(); die();
        }
        return $rtn_data;
    }

    /**
     * Gets the lead status activity
     * Created by siva shankar
     * @param lead ID
     */
    function get_lead_display_activity($lead_id, $params=array()) {
        $rtn_data = array();
        if (!empty($lead_id)) {
            $this->db->where(array(
                'L.lead_id' => $lead_id
            ));

            if (!empty($params['select'])) {
                $this->db->select($params['select']);
            }

            $this->db->order_by('LA.lead_activity_id ASC');

            $this->db->join('lead L', 'L.lead_id = LA.act_lead_id');
            $this->db->join('lead_status LS', 'LA.act_lead_status_id = LS.status_id');
            return $this->db->get('lead_activity LA');
        }
        return $rtn_data;
    }

    /**
     * Save lead activity log
     * Created by siva shankar
     * @param status data
     */
    function lead_activity_status($status) {
        if (!empty($status)) {
            $result = $this->db->insert('lead_activity', $status);
            $last_insert = $this->db->insert_id();
            if (!empty($result)) {
                $assign_lead = array(
                    'lead_status' => $status['act_lead_status_id'],
                    'appointment_date' => $status['act_status_date'],
                );  
                $this->db->where(array(
                    'lead_id' => $status['act_lead_id'],
                ));
                $this->db->update('assign_lead', $assign_lead);
            }
            return $last_insert;
        }
        return false;
    }

    /**
     * Get lead source
     * Created By Siava Shankar
     * @param null
     */
    function lead_source() {
        return $this->db->get('lead_type');
    }

    /**
     * Save upload file and save it
     * Created by Siva Shankar
     * @param Files
     */
    function recorded_history($data) {
        $this->db->insert('recorded_history', $data);
    }

    /**
     * Get
     * Created by Siva Shankar
     * @param Files
     */
    // function get_recorded_history() {
    //     return $this->db->get('recorded_history');   
    // }
    function get_recorded_audio_history() {
        $this->db->select('*');
        $this->db->from('recorded_history');
        $this->db->where('file_type','audio');
        return $this->db->get()->result_array();   
    }
     function get_recorded_vedio_history() {
        $this->db->select('*');
        $this->db->from('recorded_history');
        $this->db->where('file_type','vedio');
        return $this->db->get()->result_array();    
    }

    public function get_all_assign_lead_data(){
       $result  = $this->db->query("SELECT al.lead_id,l.pincode,al.created_at,al.reference_id FROM assign_lead as al LEFT JOIN lead as l ON l.lead_id=al.lead_id where al.reference_id=0 AND date(al.created_at)=CURDATE()")->result_array();
       return $result;
    }

    public function assign_lead_data($reference_id){
        $today = date("Y-m-d");
        $result  = $this->db->query("SELECT lead_id,reference_id,date_format(created_at, '%Y-%m-%d' ) as created_at FROM assign_lead where reference_id=$reference_id AND date(created_at)=CURDATE()")->result_array();
       return $result;
   }
    /**
     * Get
     * Created by Siva Shankar
     * @param user ID
     */
    function get_leads_dropdown($user_id, $drop_down=false) {
        if (!empty($user_id)) {
            $this->db->where(array(
                'AL.reference_id' => $user_id
            ));
            $this->db->join("assign_lead AL", "AL.lead_id = L.lead_id");
            $rtn_data = $this->db->get('lead L');

            if (!empty($rtn_data) && $rtn_data->num_rows() > 0) {
                if ($drop_down) {
                    $corban = array(
                        '' => 'Select'
                    );
                    foreach ($rtn_data->result() as $key => $assign) {
                        $corban[$assign->lead_id] = $assign->first_name . ' ' . $assign->last_name;
                    }   
                    return $corban;
                } else {
                    return $rtn_data;    
                }
            }
        }
        return array();
    }
}

?>  