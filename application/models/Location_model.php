<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Location_model extends CI_Model {
    public function __construct() {
            parent::__construct();
        }

    public function lead_dist(){
        $result  = $this->db->query("SELECT kilometer,item_number FROM settings")->row_array();
       return $result;
    }
/**/
public function location_list(){
         $result  = $this->db->query("SELECT u.*,r.role_name FROM users u LEFT JOIN roles r ON r.role_id = u.role_id WHERE r.role_name='FSO'")->result_array();
        // echo '<pre>';
        // print_R($result);
        // echo '</pre>';
        return $result;

    } 

/**/

    //Getting All FSO's List

  public function fso_list(){
         $result  = $this->db->query("SELECT u.*,r.role_name FROM users u LEFT JOIN roles r ON r.role_id = u.role_id WHERE r.role_name='FSO'")->result_array();
        return $result;

    } 

    //Getting All Branch Managers List
    
    public function branchm_list(){
        $result  = $this->db->query("SELECT u.*,r.role_name FROM users u LEFT JOIN roles r ON r.role_id = u.role_id WHERE r.role_name='Branch manager'")->result_array();
       return $result;

   } 
    
    public function lead_list($id,$kms,$lmt){
    $query  = $this->db->query("SELECT u.zip_code,z.latitude,z.longitude FROM users u LEFT JOIN zipcode z ON z.zip_code=u.zip_code WHERE user_id='$id'")->row_array();
    //echo $this->db->last_query();exit();
 //echo '<pre>';
//print_r($query['longitude']);
///exit;
    $lat = $query['latitude'];
    $lon = $query['longitude'];
    $result ='';
    if(!empty($lat) && !empty($lon)){

       $result  = $this->db->query("SELECT  DISTINCT(z.zip_code),l.*,a.*,
                                (
                                   6371 *
                                   acos(cos(radians($lat)) * 
                                   cos(radians(z.latitude)) * 
                                   cos(radians(z.longitude) - 
                                   radians($lon)) + 
                                   sin(radians($lat)) * 
                                   sin(radians(z.latitude )))
                                ) AS distance 
                                FROM zipcode z LEFT JOIN lead l ON l.pincode = z.zip_code LEFT JOIN assign_lead a ON a.lead_id = l.lead_id WHERE  a.lead_id = l.lead_id AND l.pincode = z.zip_code AND a.lead_status='1' AND a.reference_id=0 AND l.status !='2' HAVING distance < $kms 
                                ORDER BY a.lead_cat ASC LIMIT $lmt")->result_array();

        //echo $this->db->last_query();exit();
      //echo '<pre>';print_r($result);echo '</pre>';
      }
        return $result;
    }
   //policy sold
    public function policy_sold($userId){
    $result  = $this->db->query("SELECT ps.`policy_sold_id`,ps.`policy_no`, ps.`sold_date`, pt.`policy_name`, concat(u.first_name, ' ', u.last_name) as user_name, COALESCE(l.first_name, '') as customer_name FROM `policy_sold` as ps LEFT JOIN `users` as u ON u.user_id = ps.user_id LEFT JOIN `lead` as l ON l.lead_id = ps.lead_id LEFT JOIN `policy_type` as pt ON pt.`policy_id` = ps.`policy_type_id` WHERE ps.user_id = $userId ");
    
    return $result->result_array();
    }
    public function policy_sold_view($policy_sold_id){
      $result  = $this->db->query("SELECT ps.`policy_sold_id`,ps.`policy_no`, ps.`sold_date`, pt.`policy_name`, concat(u.first_name, ' ', u.last_name) as user_name, COALESCE(l.first_name, '') as customer_name, l.phone, l.dob, l.gender, l.address_line1, l.address_line2, ps.sold_date, ps.renewal_date, ps.contact_no, ps.payment_method FROM `policy_sold` as ps LEFT JOIN `users` as u ON u.user_id = ps.user_id LEFT JOIN `lead` as l ON l.lead_id = ps.lead_id LEFT JOIN `policy_type` as pt ON pt.`policy_id` = ps.`policy_type_id` WHERE ps.policy_sold_id = $policy_sold_id");
       return $result->row_array();
      // echo $this->db->last_query();exit();
    }

   //Getting Branch Manager List based on zip_code
    public function branchManagerlist($kms,$user_id,$role_id){
        $query  = $this->db->query("SELECT u.zip_code,z.latitude,z.longitude FROM users u LEFT JOIN zipcode z ON z.zip_code=u.zip_code WHERE user_id='$user_id'")->row_array();
        //echo $this->db->last_query();exit();
        $lat = $query['latitude'];
        $lon = $query['longitude'];
        $zipcode = $query['zip_code'];
        $result ='';
        if(!empty($lat) && !empty($lon)){
           $result  = $this->db->query("SELECT  DISTINCT(z.zip_code),u.*,
                                    (
                                       6371 *
                                       acos(cos(radians($lat)) * 
                                       cos(radians(z.latitude)) * 
                                       cos(radians(z.longitude) - 
                                       radians($lon)) + 
                                       sin(radians($lat)) * 
                                       sin(radians(z.latitude )))
                                    ) AS distance 
                                    FROM zipcode z LEFT JOIN users u ON u.zip_code = z.zip_code WHERE  u.user_id!=$user_id AND u.role_id!=$role_id AND u.role_id!=3 HAVING distance < $kms")->result_array();
    
           //echo $this->db->last_query();exit();
          //echo '<pre>';print_r($result);echo '</pre>';
          }
            return $result;
    }
    //Getting Branch FSO List based on zip_code
    public function assign_fso_list($kms,$user_id,$role_id){
    $query  = $this->db->query("SELECT u.zip_code,z.latitude,z.longitude FROM users u LEFT JOIN zipcode z ON z.zip_code=u.zip_code WHERE user_id='$user_id'")->row_array();
    //echo $this->db->last_query();exit();
    $lat = $query['latitude'];
    $lon = $query['longitude'];
    $zipcode = $query['zip_code'];
    $result ='';
    if(!empty($lat) && !empty($lon)){

       $result  = $this->db->query("SELECT  DISTINCT(z.zip_code),u.*,al.branch_manager,
                                (
                                   6371 *
                                   acos(cos(radians($lat)) * 
                                   cos(radians(z.latitude)) * 
                                   cos(radians(z.longitude) - 
                                   radians($lon)) + 
                                   sin(radians($lat)) * 
                                   sin(radians(z.latitude )))
                                ) AS distance 
                                FROM assign_lead al,zipcode z LEFT JOIN users u ON u.zip_code = z.zip_code WHERE  u.user_id!=$user_id AND u.role_id!=$role_id  AND u.role_id!=1 AND al.branch_manager=$user_id HAVING distance < $kms")->result_array();
      }
        return $result;
    }
    //Getting Branch FSO List based on zip_code
    public function branch_manager_fso_filter($kms,$user_id,$role_id){
    $query  = $this->db->query("SELECT u.zip_code,z.latitude,z.longitude FROM users u LEFT JOIN zipcode z ON z.zip_code=u.zip_code WHERE user_id='$user_id'")->row_array();
    //echo $this->db->last_query();exit();
    $lat = $query['latitude'];
    $lon = $query['longitude'];
    $zipcode = $query['zip_code'];
    $result ='';
    if(!empty($lat) && !empty($lon)){

       $result  = $this->db->query("SELECT  DISTINCT(z.zip_code),l.first_name,l.last_name,al.branch_manager,al.id,
                                (
                                   6371 *
                                   acos(cos(radians($lat)) * 
                                   cos(radians(z.latitude)) * 
                                   cos(radians(z.longitude) - 
                                   radians($lon)) + 
                                   sin(radians($lat)) * 
                                   sin(radians(z.latitude )))
                                ) AS distance 
                                FROM zipcode z LEFT JOIN lead l ON l.pincode = z.zip_code LEFT JOIN assign_lead al ON al.lead_id = l.lead_id WHERE  al.branch_manager=$user_id HAVING distance < $kms")->result_array();
                                
      }
        return $result;
    }
    //Getting Branch Manager List based on zip_code
    public function branch_manager_fso_list($kms,$user_id,$role_id){
        $query  = $this->db->query("SELECT u.zip_code,z.latitude,z.longitude FROM users u LEFT JOIN zipcode z ON z.zip_code=u.zip_code WHERE user_id='$user_id'")->row_array();
        //echo $this->db->last_query();exit();
        $lat = $query['latitude'];
        $lon = $query['longitude'];
        $zipcode = $query['zip_code'];
        $result ='';
        if(!empty($lat) && !empty($lon)){
           $result  = $this->db->query("SELECT  DISTINCT(z.zip_code),u.*,
                                    (
                                       6371 *
                                       acos(cos(radians($lat)) * 
                                       cos(radians(z.latitude)) * 
                                       cos(radians(z.longitude) - 
                                       radians($lon)) + 
                                       sin(radians($lat)) * 
                                       sin(radians(z.latitude )))
                                    ) AS distance 
                                    FROM zipcode z LEFT JOIN users u ON u.zip_code = z.zip_code WHERE  u.user_id!=$user_id AND u.role_id!=$role_id AND u.role_id!=1 HAVING distance < $kms")->result_array();
                                    //echo $this->db->last_query();exit();
          }
         
            return $result;
    }

    //Getting Branch Manager List based on zip_code
    public function branch_fso_list_excel($kms,$role_id,$pincode){
      // to find zipcode table match to the users table
         $query  = $this->db->query("SELECT DISTINCT(z.zip_code),z.latitude,z.longitude FROM users u LEFT JOIN zipcode z ON z.zip_code=u.zip_code WHERE z.zip_code=$pincode")->row_array();
     // print_r($query);exit();
        $lat = $query['latitude'];
        $lon = $query['longitude'];
        $zipcode = $query['zip_code'];
        $result ='';
        // to find nearest location to write query
        if(!empty($lat) && !empty($lon)){
           $result  = $this->db->query("SELECT DISTINCT(z.zip_code),u.*,
                                    (
                                       6371 *
                                       acos(cos(radians($lat)) * 
                                      cos(radians(z.latitude)) * 
                                       cos(radians(z.longitude) - 
                                       radians($lon)) + 
                                       sin(radians($lat)) * 
                                       sin(radians(z.latitude )))
                                    ) AS distance 
                                    FROM zipcode z LEFT JOIN users u ON u.zip_code = z.zip_code WHERE u.role_id=3 HAVING distance < $kms" )->result_array();
                                    // echo $this->db->last_query();exit();
          }
        return $result;
    }
    //Getting Branch Head List
    public function branchHead_list(){
        $result  = $this->db->query("SELECT u.*,r.role_name FROM users u LEFT JOIN roles r ON r.role_id = u.role_id WHERE r.role_name='Branch Head'")->result_array();
       return $result;

   } 
}


?>