<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_login_model extends CI_Model {

    // Karthik Login check
    public function login($email, $password) {
        $this->db->select('*')->from('users');
        $this->db->where('password', sha1($password));
        $this->db->group_start();
        $this->db->where('email', $email);
        $this->db->or_where('phone', $email);
        $this->db->group_end();
        $query = $this->db->get();
        return $query->row_array();
    }

    // Karthik getting all the leads
    public function get_all_leads() {
        $this->db->select('lead.* ,city.*,country.*,state.*');
        $this->db->from('lead');
        $this->db->join('city', 'lead.city_id = city.city_id', 'left');
        $this->db->join('country', 'lead.country_id = country.country_id', 'left');
        $this->db->join('state', 'lead.state_id = state.state_id', 'left');
        $get_all_leads = $this->db->get();
        return $get_all_leads->result_array();
    }

    // Karthik getting today leads
    public function get_today_leads() {
        $today = date("Y-m-d");
        $this->db->select('lead.* ,city.*,country.*,state.*');
        $this->db->from('lead');
        $this->db->join('city', 'lead.city_id = city.city_id', 'left');
        $this->db->join('country', 'lead.country_id = country.country_id', 'left');
        $this->db->join('state', 'lead.state_id = state.state_id', 'left');

        $this->db->where('lead.created_at', $today);
        $get_all_leads = $this->db->get();
        return $get_all_leads->result_array();
    }

    // SENTHAMIZH SELVI -- Checking phone number query 14-12-2018 
    public function get_phone($mobile) {
        $this->db->select('phone', 'email');
        $this->db->from('users');
        $this->db->where('phone', $mobile);
        $result = $this->db->get();
        return $result->row_array();
    }

    // SENTHAMIZH SELVI -- Common UPDATE query 14-12-2018 
    public function update($data = '', $field = '', $val = '', $table = '') {

        $this->db->where($field, $val);
        $result = $this->db->update($table, $data);
        return $result;
    }

    // check email otp
    public function verification_otp($user_mobile) {
        $this->db->select('*')->from('users');
        $this->db->where('phone', $user_mobile);
        $query = $this->db->get();
        return $query->row_array();
    }

    //
    public function check_verification_email_otp($check_number) {
        $this->db->select('*')->from('users');
        $this->db->where('otp_number', $check_number);
        $query = $this->db->get();
        return $query->row_array();
    }

    // SENTHAMIZH SELVI -- Otp and Mobile number validation query 14-12-2018 
    public function otp_validation($data) {

        $this->db->select('*');
        $this->db->where('phone', $data['mobile']);
        $this->db->where('otp_number', $data['otp']);
        $result = $this->db->get('users');
        return $result->row_array();
    }
    //update password
    public function update_password(){
        $mobile_number = $this->input->post('mobile_number');
        $data = array(
            'real_password' => $this->input->post('password_1'),
            'password' => sha1($this->input->post('password_2')),
            'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('phone',$mobile_number);
        $this->db->update('users', $data);
        return true;
    }
}
?>