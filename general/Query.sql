####################################
# Lead Activity table by Siva Shankar #
DROP TABLE IF EXISTS `lead_activity`;
CREATE TABLE `lead_activity` (
 `lead_activity_id` int(11) NOT NULL AUTO_INCREMENT,
 `act_lead_id` bigint(11) NOT NULL COMMENT 'Primary Index of lead table',
 `act_lead_status_id` int(11) NOT NULL COMMENT 'Primary Index of lead status table',
 `act_status_date` datetime NOT NULL COMMENT 'Manual datetime given by reporting person',
 `act_comment` text COMMENT 'Manual text status for managing leads',
 `act_doc_path` varchar(255) DEFAULT NULL COMMENT 'document reference for leads',
 `act_created_by` bigint(20) NOT NULL COMMENT 'The ID of logged in user',
 `act_created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `act_updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`lead_activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
###################################

##############################################
DROP TABLE IF EXISTS `recorded_history`;
CREATE TABLE `recorded_history` ( `rec_history_id` INT NOT NULL AUTO_INCREMENT COMMENT 'ID of files' , `file_type` ENUM('audio','vedio') NOT NULL COMMENT 'To detect the mime type' , `file_path` VARCHAR(255) NOT NULL , `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `created_by` BIGINT(20) NOT NULL , `updated_on` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL , `mime_type` VARCHAR(20) NULL , PRIMARY KEY (`rec_history_id`)) ENGINE = InnoDB;
ALTER TABLE `recorded_history` ADD `lead_id` BIGINT(11) NOT NULL AFTER `rec_history_id`;
################################################


################################################
ALTER TABLE `policy_sold` ADD `payment_method` ENUM('monthly','quarterly') NOT NULL AFTER `sold_date`;
ALTER TABLE `policy_sold` ADD `contact_no` VARCHAR(25) NOT NULL AFTER `sold_date`;
ALTER TABLE `policy_sold` ADD `renewal_date` DATETIME NOT NULL AFTER `sold_date`;
################################################